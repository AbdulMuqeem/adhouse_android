package com.wamztech.adhouse.activities;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.libraries.places.api.Places;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.activities.abstracts.BaseActivity;
import com.wamztech.adhouse.fragments.HomeFragment;
import com.wamztech.adhouse.fragments.SideMenuFragment;
import com.wamztech.adhouse.fragments.user.LoginFragment;
import com.wamztech.adhouse.helpers.BasePreferenceHelper;
import com.wamztech.adhouse.helpers.RunTimePermissions;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.ui.views.TitleBar;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity  {



    boolean isGuestUser = false;
    @BindView(R.id.contentFrame)
    RelativeLayout contentFrame;
    private TitleBar mainTitleBar;
    private DrawerLayout drawerLayout;
    private BasePreferenceHelper basePreferenceHelper;
    private OnActivityResult onActivityResult;
    private SideMenuFragment sideMenuFragment;
    private String searchKeyWord;


    public OnActivityResult getOnActivityResult() {
        return onActivityResult;
    }

    public void setOnActivityResult(OnActivityResult onActivityResult) {
        this.onActivityResult = onActivityResult;
    }

//    private void setUpGoogleClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, 0 /* clientId */, this)
//                .addApi(Places.GEO_DATA_API)
//                .build();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FragmentManager.enableDebugLogging(true);
        basePreferenceHelper = new BasePreferenceHelper(this);
//        Places.initialize(getApplicationContext(), YOUR_API_KEY);

        RunTimePermissions.verifyStoragePermissions(this);
        initViews();
        initFragment();
//        setUpGoogleClient();

        mainTitleBar.setLeftButton(R.drawable.dropdown, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerOpen();
            }
        });
        validateDrawerLock();
    }

    @Override
    protected void onResume() {
        super.onResume();
        validateDrawerLock();
    }

    private void validateDrawerLock() {
        if (detectCurrentFragment("LoginFragment")
                || detectCurrentFragment("SignupFragment")
                || detectCurrentFragment("ForgotPasswordFragment")
        ) {
            drawerLock();
        } else {
            drawerLock();

        }
    }

    private void initViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mainTitleBar = (TitleBar) findViewById(R.id.mainTitleBar);

    }

    public TitleBar getMainTitleBar() {
        return mainTitleBar;
    }

    public void setHeading(String heading) {
        mainTitleBar.setHeading(heading);
    }

    public void setLeftButton(int drawable, View.OnClickListener listener) {

        mainTitleBar.setLeftButton(drawable, listener);

    }

    public void setRightButton(int drawable, View.OnClickListener listener) {
        mainTitleBar.setRightButton(drawable, listener);
    }

    public void drawerclose() {
        if (drawerLayout == null)
            return;
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void drawerOpen() {
        if (drawerLayout == null)
            return;
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void initDrawer() {
        setSideMenuFragment(SideMenuFragment.newInstance());
        addSideMenuFragment(getSideMenuFragment());


    }

    public void initFragment() {
        if (basePreferenceHelper.getUser() == null) {
            addDockableFragment(LoginFragment.newInstance());
        } else {
            addDockableFragment(HomeFragment.newInstance());
            initDrawer();
        }

    }

    public void drawerLock() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    public void drawerUnLock() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 2) {
            super.onBackPressed();
        } else {
            showExitDialog();
        }
    }

    private void showExitDialog() {

        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage(getResources().getString(R.string.are_you_sure_exit));
        alertDlg.setCancelable(false);
        alertDlg.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                }
        );
        alertDlg.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // We do nothing
            }
        });

        alertDlg.create().show();
    }

    public boolean isGuestUser() {
        return isGuestUser;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2222) {
            onActivityResult.onResult(requestCode, resultCode, data);
        } else if (requestCode == 5656) {
            onActivityResult.onResult(requestCode, resultCode, data);
        }
    }

    public String getSearchKeyWord() {
        return searchKeyWord;
    }

    public void setSearchKeyWord(String searchKeyWord) {
        this.searchKeyWord = searchKeyWord;
    }


    public SideMenuFragment getSideMenuFragment() {
        return sideMenuFragment;
    }

    public void setSideMenuFragment(SideMenuFragment sideMenuFragment) {
        this.sideMenuFragment = sideMenuFragment;
    }

    @OnClick(R.id.contentFrame)
    public void onViewClicked() {
        drawerclose();
    }

}
