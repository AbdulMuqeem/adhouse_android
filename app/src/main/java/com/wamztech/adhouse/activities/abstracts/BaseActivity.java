package com.wamztech.adhouse.activities.abstracts;


import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.HomeFragment;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public abstract class BaseActivity extends AppCompatActivity {

    public static final String KEY_FRAG_FIRST = "firstFrag";

    public void addSideMenuFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.sideMneuFragmentContainer, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();

    }

    public void addDockableFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();

    }

    public void replaceDockableSupportFragment(Fragment frag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, frag, frag.getClass().getSimpleName()).commit();

    }

    public boolean isHomeOnTop() {

        BaseFragment myFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
        if (myFragment != null && myFragment.isVisible()) {
            return true;
        }
        return false;

    }


    public void emptyBackStack() {

        FragmentManager manager = getSupportFragmentManager();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            if (manager.getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

    }


    public void popBackstackTillEntry(int entryIndex) {


        if (getSupportFragmentManager() == null) {
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() <= entryIndex) {
            return;
        }
        FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                entryIndex);
        if (entry != null) {
            getSupportFragmentManager().popBackStackImmediate(entry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * Checks current visible Fragment and then removes respective Option from PopupWindow
     *
     * @param fragment Fragment Simple Name to be checked from stack
     ***/
    public boolean detectCurrentFragment(String fragment) {

        Fragment myFragment = getSupportFragmentManager().findFragmentByTag(fragment);
        if (myFragment != null && myFragment.isVisible()) {
            // add your code here
            return true;
        } else {
            return false;

        }


    }

    public void popFragment() {
        if (getSupportFragmentManager() == null)
            return;
        getSupportFragmentManager().popBackStack();
    }


}
