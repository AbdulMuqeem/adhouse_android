package com.wamztech.adhouse;

import android.content.Context;

import com.wamztech.adhouse.models.DetailHorizontal;

import java.util.ArrayList;


public class Constants {

    public static final String RESULT_DATA_KEY = "result_data_key";
    public static final int SUCCESS_RESULT = 100;
    public static final String LOCATION_DATA_EXTRA = "location_data_extra";
    public static final String RECEIVER = "receiver";
    public static final int FAILURE_RESULT = 101;
    public static final int LOCATION_PERMISSION = 914;
    private static Context context;

    public static ArrayList<String> getCities() {

        ArrayList<String> cities = new ArrayList<>();
        cities.add("Abu Dhabi");
        cities.add("Al Ain");
        cities.add("Ajman");
        cities.add("Dubai");
        cities.add("Fujairah");
        cities.add("Ras Al Khaimah");
        cities.add("Sharjah");
        cities.add("Umm al-Quwain");

        return cities;
    }

    public static ArrayList<String> getGender() {


//      gender.add("Gender");
        ArrayList<String> gender = new ArrayList<>();
        gender.add("Female");
        gender.add("Male");


        return gender;
    }

    public static ArrayList<String> getCategory() {

        ArrayList<String> category = new ArrayList<>();

        category.add("Vehicle");
        category.add("Properties");
        category.add("Classified");

        return category;
    }

    public static ArrayList<String> getFeatured() {

        ArrayList<String> featured = new ArrayList<>();

        featured.add("Yes");
        featured.add("No");

        return featured;
    }

    public static ArrayList<String> getFuelType() {

        ArrayList<String> fuelType = new ArrayList<>();

        fuelType.add("Hybrid");
        fuelType.add("CNG");
        fuelType.add("Petrol");

        return fuelType;
    }


    public static ArrayList<String> getCarType() {

        ArrayList<String> carType = new ArrayList<>();

        carType.add("New");
        carType.add("Old");

        return carType;
    }

    public static ArrayList<String> getCarClass() {

        ArrayList<String> carClass = new ArrayList<>();

        carClass.add("Luxury");
        carClass.add("Sports");
        carClass.add("Minivans");
        carClass.add("Station Wagons");

        return carClass;
    }

    public static ArrayList<String> getKilometer() {

        ArrayList<String> kilometer = new ArrayList<>();

        kilometer.add("0 km - 1000 km");
        kilometer.add("1000 km - 5000 km");
        kilometer.add("5000 km - 10,000km");
        kilometer.add("10,000 km - above");

        return kilometer;
    }

    public static ArrayList<String> getCarColor() {

        ArrayList<String> color = new ArrayList<>();

        color.add("Jack Black");
        color.add("white");
        color.add("gray ");
        color.add("blue");

        return color;
    }

    public static ArrayList<String> getCylinder() {

        ArrayList<String> cylinder = new ArrayList<>();

        cylinder.add("2 ");
        cylinder.add("4 ");
        cylinder.add("5");
        cylinder.add("6");

        return cylinder;
    }

    public static ArrayList<String> getGear() {

        ArrayList<String> gear = new ArrayList<>();

        gear.add("4 speed automatic ");
        gear.add("5 speed automatic");
        gear.add("7 speed automatic");

        return gear;
    }

    public static ArrayList<String> getHorsePower() {

        ArrayList<String> horsePower = new ArrayList<>();

        horsePower.add("530 HP");
        horsePower.add("650 hp");
        horsePower.add("1500 hp");

        return horsePower;
    }

    public static ArrayList<String> getSportsKit() {

        ArrayList<String> sportsKit = new ArrayList<>();

        sportsKit.add("Yes");
        sportsKit.add("No");

        return sportsKit;
    }

    public static ArrayList<String> getDriveTrain() {

        ArrayList<String> driveTrain = new ArrayList<>();

        driveTrain.add("Front-wheel drive car");
        driveTrain.add("Four-wheel drive off-road vehicle");
        driveTrain.add("Manual transmission car");
        driveTrain.add("Automatic transmission car");

        return driveTrain;
    }

    public static ArrayList<String> getBodyType() {

        ArrayList<String> bodyType = new ArrayList<>();

        bodyType.add("Compact Car");
        bodyType.add("City cars");
        bodyType.add("Super Minis");
        bodyType.add("Hatchbacks");
        bodyType.add("Mini-MPVs");
        bodyType.add("Estates");
        bodyType.add("Four-door Coupes");

        return bodyType;
    }

    public static ArrayList<String> getInteriorColor() {

        ArrayList<String> interiorColor = new ArrayList<>();

        interiorColor.add("Wicker");

        return interiorColor;
    }

    public static ArrayList<String> getSeatType() {

        ArrayList<String> seatType = new ArrayList<>();

        seatType.add("Rear facing");
        seatType.add("Convertible");
        seatType.add("Belt-positioning booster seats");

        return seatType;
    }

    public static ArrayList<String> getSlideRoof() {

        ArrayList<String> slideRoof = new ArrayList<>();

        slideRoof.add("Yes");
        slideRoof.add("No");

        return slideRoof;
    }

    public static ArrayList<String> getParkingCamera() {

        ArrayList<String> parkingCamera = new ArrayList<>();

        parkingCamera.add("Yes");
        parkingCamera.add("No");

        return parkingCamera;
    }

    public static ArrayList<String> getCD() {

        ArrayList<String> CD = new ArrayList<>();

        CD.add("Yes");
        CD.add("No");

        return CD;
    }

    public static ArrayList<String> getDVD() {

        ArrayList<String> DVD = new ArrayList<>();

        DVD.add("Yes");
        DVD.add("No");

        return DVD;
    }

    public static ArrayList<String> getBluetooth() {

        ArrayList<String> bluetooth = new ArrayList<>();

        bluetooth.add("Yes");
        bluetooth.add("No");

        return bluetooth;
    }

    public static ArrayList<String> getGPS() {

        ArrayList<String> GPS = new ArrayList<>();

        GPS.add("Yes");
        GPS.add("No");

        return GPS;
    }

    //property


    public static ArrayList<String> getPremisesType() {

        ArrayList<String> premisesType = new ArrayList<>();

        premisesType.add("Residential");
        premisesType.add("Commercial");

        return premisesType;
    }

    public static ArrayList<String> getOwnership() {

        ArrayList<String> ownership = new ArrayList<>();

        ownership.add("Sale");
        ownership.add("Individual ownership");
        ownership.add("Joint ownership");
        ownership.add("Title by contract (POD),(TOD)");

        return ownership;
    }

    public static ArrayList<String> getPaymentPlan() {

        ArrayList<String> paymentPlan = new ArrayList<>();

        paymentPlan.add("Monthly");
        paymentPlan.add("Quarterly");

        return paymentPlan;
    }


    public static ArrayList<String> getUnderWarranty() {

        ArrayList<String> underWarranty = new ArrayList<>();

        underWarranty.add("Yes");
        underWarranty.add("No");

        return underWarranty;
    }

    public static ArrayList<String> getSellerType() {

        ArrayList<String> sellerType = new ArrayList<>();

        sellerType.add("Car Dealer");

        return sellerType;
    }

    public static ArrayList<String> getYear() {

        ArrayList<String> year = new ArrayList<>();

        year.add("2000");
        year.add("2001");
        year.add("2002");
        year.add("2003");
        year.add("2004");
        year.add("2005");

        return year;
    }

    public static ArrayList<String> getArea() {

        ArrayList<String> area = new ArrayList<>();

        area.add("Sq Yds");
        area.add("Sq Feet");
        area.add("Sq Meter");


        return area;
    }

    public static ArrayList<String> getSubCategory() {

        ArrayList<String> subCategory = new ArrayList<>();
        subCategory.add("Camera");
        subCategory.add("Electronics / computer & Tablets");
        subCategory.add("Antiques");
        subCategory.add("Furniture");


        return subCategory;
    }


    public static ArrayList<String> getVehicleImages() {
        ArrayList<String> arrImagesVehicle = new ArrayList<>();
        arrImagesVehicle.add("http://www.hdnicewallpapers.com/Walls/Big/Lamborghini/New_Lamborghini_Centenario_2016_Car_HD_Wallpapers.jpg");
        arrImagesVehicle.add("http://hdqwalls.com/wallpapers/lamborghini-centenario-new.jpg");
        arrImagesVehicle.add("http://hdqwalls.com/wallpapers/lamborghini-centenario-new.jpg");
        arrImagesVehicle.add("http://www.hdnicewallpapers.com/Walls/Big/Lamborghini/New_Lamborghini_Centenario_2016_Car_HD_Wallpapers.jpg");

        return arrImagesVehicle;
    }


    private static ArrayList<String> getClassifiedImages() {
        ArrayList<String> arrImages = new ArrayList<>();
        arrImages.add("https://img2.10bestmedia.com/Images/Photos/229985/p-Antique-Alley_54_990x660_201404241128.jpg");
        arrImages.add("https://cnet4.cbsistatic.com/img/0o4PYtmApp34kBxO22b4JTcQfpM=/1070x602/2016/12/16/a3b7f4d5-f15d-41e0-9686-684ecf15e795/dell-xps-13-2-in-1-014.jpg");
        arrImages.add("http://drop.ndtv.com/albums/GADGETS/mobiles_launched_in_january_2016/alcatel_onetouch_fierce_xl.jpg?output-quality=70&output-format=jpg");
        arrImages.add("https://img2.10bestmedia.com/Images/Photos/229985/p-Antique-Alley_54_990x660_201404241128.jpg");

        return arrImages;
    }


    public static ArrayList<String> getPropertiesImages() {
        ArrayList<String> arrImagesProperties = new ArrayList<>();

        arrImagesProperties.add("http://cmcf.org.my/wp-content/uploads/2016/10/about_us_001.jpg");
//        arrImagesProperties.add("http://aboutislam.net/wp-content/uploads/2016/09/A-Father-Giving-His-Son%E2%80%99s-Properties-to-Another.jpg");
        arrImagesProperties.add("http://cmcf.org.my/wp-content/uploads/2016/10/about_us_001.jpg");
        arrImagesProperties.add("http://www.propertygama.com/user/imagesell/5515_Properties-in-Bangalore1.jpg");
        arrImagesProperties.add("http://cmcf.org.my/wp-content/uploads/2016/10/about_us_001.jpg");


        return arrImagesProperties;
    }


    public static ArrayList<DetailHorizontal> getFeaaturedAdsClassified() {


        ArrayList<DetailHorizontal> arrFeatuedAdsProperties = new ArrayList<>();

        arrFeatuedAdsProperties = new ArrayList<>();

        arrFeatuedAdsProperties.add(new DetailHorizontal("https://www.bhphotovideo.com/images/categoryImages/desktop/325x325/21008-DSLR-Cameras.jpg", "Electronics/ Computer and Tablet", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("http://g-ecx.images-amazon.com/images/G/31//img16/PC/sep/LaptopsRevamp/budget._V279288877_.jpg", "Electronics/ Computer and Tablet", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("http://drop.ndtv.com/albums/GADGETS/mobiles_launched_in_january_2016/alcatel_onetouch_fierce_xl.jpg?output-quality=70&output-format=jpg", "Mobile Phone & Accessories", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("http://www.images.searchpointer.com/furnitures/9968/farhan-furnitures-4.jpg", "Furniture", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("https://peloponnese.events/wp-content/uploads/2016/02/oldaker-antiques-collectables-handmade-creations-centre-image.jpg", "Antiques ", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("https://www.bhphotovideo.com/images/categoryImages/desktop/325x325/21008-DSLR-Cameras.jpg", "Electronics/ Computer and Tablet", "Featured"));
        arrFeatuedAdsProperties.add(new DetailHorizontal("http://g-ecx.images-amazon.com/images/G/31//img16/PC/sep/LaptopsRevamp/budget._V279288877_.jpg", "Electronics/ Computer and Tablet", "Featured"));

        return arrFeatuedAdsProperties;
    }


}
