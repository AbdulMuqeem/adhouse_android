package com.wamztech.adhouse.retrofit;

import com.wamztech.adhouse.constants.APIConstants;
import com.wamztech.adhouse.retrofit.entities.GsonFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebServiceFactory {

    private static WebService instance;

    public static WebService getInstance() {

        if (instance == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            Interceptor header = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            };

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(121, TimeUnit.SECONDS)
                    .addInterceptor(header)
                    .addInterceptor(interceptor)
                    .build();



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIConstants.SERVER_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(GsonFactory.getSimpleGson()))
                    .build();

            instance = retrofit.create(WebService.class);
        }


        return instance;
    }

}