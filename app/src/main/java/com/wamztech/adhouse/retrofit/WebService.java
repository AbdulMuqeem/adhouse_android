package com.wamztech.adhouse.retrofit;


 ;

import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.Ads;
import com.wamztech.adhouse.models.Attributes;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.models.City;
import com.wamztech.adhouse.models.Cms;
import com.wamztech.adhouse.models.Contact;
import com.wamztech.adhouse.models.ContactUsModal;
import com.wamztech.adhouse.models.Currency;
import com.wamztech.adhouse.models.GetGoogleAPIKey;
import com.wamztech.adhouse.models.Notification;
import com.wamztech.adhouse.models.Search;
import com.wamztech.adhouse.models.SubscriptionResult;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.models.UserSubscription;

import java.util.ArrayList;
import java.util.List;

 import androidx.annotation.Nullable;
 import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface WebService {

    @FormUrlEncoded
    @POST("login")
    Call<WebResponse<User>> login(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("app-config")
    Call<WebResponse<GetGoogleAPIKey>> getGoogleApiKey(
            @Query("key") String key
    );


    @FormUrlEncoded
    @POST("contact")
    Call<WebResponse<Contact>> contatct(
            @Field("user_name") String userName,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("description") String description
    );

    @Multipart
    @POST("register")
    Call<WebResponse<User>> userRegistration(
            @Part("full_name") RequestBody fullName,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("password") RequestBody password,
            @Part("password_confirmation") RequestBody password_confirmation,
            @Part("address") RequestBody address,
            @Part("currency_id") RequestBody currency_id,
            @Part("country_id") RequestBody country_id,
            @Part("city") RequestBody city,
            @Part("role_id") RequestBody roleId,
            @Part MultipartBody.Part profile_picture
    );

    @GET("city")
    Call<WebResponse<ArrayList<City>>> getCity(
            @Query("lang") String lang

    );

    @GET("countries")
    Call<WebResponse<ArrayList<City>>> getCountry(
            @Query("lang") String lang

    );


    @GET("currencies")
    Call<WebResponse<ArrayList<Currency>>> getCurrency(
            @Query("user_id") Integer user_id

    );

    @GET("currency")
    Call<WebResponse<Currency>> getUserCurrency(
            @Query("user_id") Integer user_id
    );

    @GET("contact/web")
    Call<WebResponse<ContactUsModal>> getInTouchDetail(
            @Query("lang") String lang

    );


    @GET("user/profile")
    Call<WebResponse<User>> getProfile(
            @Query("user_id") int userId
    );

    @Multipart
    @POST("user/update")
    Call<WebResponse<User>> updateProfile(
            @Part("user_id") RequestBody userId,
            @Part("full_name") RequestBody fullName,
            @Part("phone") RequestBody phone,
            @Part("address") RequestBody address,
            @Part("currency_id") RequestBody currency_id,
            @Part("country_id") RequestBody country_id,
            @Part("city") RequestBody city,
            @Part("gender") RequestBody gender,
            @Part MultipartBody.Part profile_picture

    );

    @FormUrlEncoded
    @POST("forgotpassword")
    Call<WebResponse<User>> forgotPassword(
            @Field("email") String email

    );


    @FormUrlEncoded
    @POST("changepassword")
    Call<WebResponse<User>> changePassword(
            @Field("user_id") int userId,
            @Field("old_password") String oldPassword,
            @Field("password") String password,
            @Field("password_confirmation") String passwordConfirmation
    );


    @GET("get/vehicle/ads")
    Call<WebResponse<ArrayList<Ad>>> getVehicleAds(
            @Query("user_id") int userId,
            @Query("lang") String lang
    );

    @GET("get/property/ads")
    Call<WebResponse<ArrayList<Ad>>> getPropertiesAds(
            @Query("user_id") int userId,
            @Query("lang") String lang
    );

    @GET("get/classified/ads")
    Call<WebResponse<ArrayList<Ad>>> getClassifiedAds(
            @Query("user_id") int userId,
            @Query("lang") String lang
    );

    @GET("get/sub/classified/ads")
    Call<WebResponse<ArrayList<Ad>>> getClassifiedSubAds(
            @Query("user_id") int userId,
            @Query("category_id") int categoryId,
            @Query("lang") String lang
    );


    @GET("get/ads")
    Call<WebResponse<Ads>> getAds(
            @Query("user_id") int userId,
            @Query("lang") String lang
    );


    @GET("get/category/featured")
    Call<WebResponse<ArrayList<Ad>>> getCategoryFeatured(
            @Query("category_id") int categoryId,
            @Query("user_id") int userId
    );

    @GET("get/owner/ads")
    Call<WebResponse<ArrayList<Ad>>> getOwnerads(
            @Query("owner_id") int owner_id,
            @Query("user_id") int user_id
    );

    @GET("ad/description")
    Call<WebResponse<ArrayList<Ad>>> getAd(
            @Query("ad_id") int ad_id,
            @Query("user_id") int user_id
    );

    @GET("description")
    Call<WebResponse<Ad>> getAdDetails(
            @Query("ad_id") int ad_id,
            @Query("user_id") int user_id,
            @Query("lang") String language
    );

    @FormUrlEncoded
    @POST("addfavourite")
    Call<WebResponse<Ad>> addFavourite(
            @Field("user_id") int userId,
            @Field("ad_id") int adId,
            @Field("state") int state
    );


    @GET("getfavourites")
    Call<WebResponse<ArrayList<Ad>>> getFavourite(
            @Query("user_id") int userId
    );


    @GET("cms")
    Call<WebResponse<Cms>> getCms(
            @Query("keyword") String keyword,
            @Query("lang") String lang
    );


    @GET("search")
    Call<WebResponse<Search>> getSearchResult(
            @Query("user_id") int userId,
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("keyword") String keyword,
            @Query("category_id") int category_id,
            @Query("lang") String language
    );

    @GET("get/notifications")
    Call<WebResponse<ArrayList<Notification>>> getNotification(
            @Query("user_id") int userId
    );


    @GET("subscription/pakages")
    Call<WebResponse<SubscriptionResult>> getSubscriptionPackages(
            @Query("user_id") int userId
    );

    @GET("user/subscription")
    Call<WebResponse<UserSubscription>> getUserSubscription(
            @Query("user_id") int userId
    );

    @FormUrlEncoded
    @POST("update/devicetoken")
    Call<WebResponse> updateToken(
            @Field("user_id") int userId,
            @Field("device_token") int adId,
            @Field("device_type") int state
    );


    @FormUrlEncoded
    @POST("subscribe/package")
    Call<WebResponse<UserSubscription>> subscribePackage(
            @Field("user_id") int userId,
            @Field("package_id") int packageId,
            @Field("payment_response") String paymentResponse,
            @Field("nonce") String nonce
    );

    /*
   Submit Ads Module
    */
    @GET("categories")
    Call<WebResponse<ArrayList<Category>>> getCategory(
            @Query("lang") String lang

    );

    @GET("search/categories")
    Call<WebResponse<ArrayList<Category>>> getSearchcategories(
            @Query("lang") String lang

    );

    @GET("sub/categories")
    Call<WebResponse<ArrayList<Category>>> getSubCategory(
            @Query("category_id") int category_id,
            @Query("lang") String lang


    );

    @GET("attributes")
    Call<WebResponse<ArrayList<Attributes>>> getAttributes(
            @Query("category_id") int category_id,
            @Query("sub_category_id") int sub_category_id,
            @Query("lang") String lang


    );


    @Multipart
    @POST("submit/ad")
    Call<WebResponse<Ad>> postAds(
            @Part("user_id") RequestBody user_id,
            @Part("title") RequestBody title,
            @Part("price") RequestBody price,
            @Part("description") RequestBody description,
            @Part("featured") RequestBody featured,
            @Part("phone") RequestBody phone,
            @Part("area") RequestBody area,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("category_id") RequestBody category_id,
            @Part("sub_category_id") RequestBody sub_category_id,

            @Part List<MultipartBody.Part> ad_images,
//            @Part List<MultipartBody.Part> attributes,
            @Part("attributes") RequestBody attributes,
            @Part("lang") RequestBody lang
//            @Part JSONArray attributes
    );


    @DELETE("ad/Delete/{id}")
    Call<WebResponse<Ad>> deleteAds(@Path("id") int adsId);

    /*
    Filter
     */
    @GET("filter")
    Call<WebResponse<ArrayList<Ad>>> getFiltered(
            @Query("user_id") int user_id,
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("title") String title,
            @Nullable @Query("featured") int featured,
            @Query("price_min") String priceMin,
            @Query("price") String price,
            @Query("area") String area,
            @Query("latitude") String latitude,
            @Query("longitude") String longitude,
            @Nullable @Query("category_id") int category_id,
            @Nullable @Query("sub_category_id") int sub_category_id,
            @Query("attributes") String attributes,
            @Query("lang") String language

    );

    @GET("filter")
    Call<WebResponse<ArrayList<Ad>>> getFilteredv1(
            @Query("user_id") int user_id,
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("title") String title,
            @Query("price_min") String priceMin,
            @Query("price") String price,
            @Query("area") String area,
            @Query("latitude") String latitude,
            @Query("longitude") String longitude,
            @Nullable @Query("country_id") int country_id,
            @Nullable @Query("category_id") int category_id,
            @Nullable @Query("sub_category_id") int sub_category_id,
            @Query("attributes") String attributes,
            @Query("lang") String language

    );

    @Multipart
    @POST("update")
    Call<WebResponse<Ad>> postAdsUpdate(
            @Part("user_id") RequestBody user_id,
            @Part("title") RequestBody title,
            @Part("price") RequestBody price,
            @Part("description") RequestBody description,
            @Part("featured") RequestBody featured,
            @Part("phone") RequestBody phone,
            @Part("area") RequestBody area,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("category_id") RequestBody category_id,
            @Part("sub_category_id") RequestBody sub_category_id,
            @Part("ad_id") RequestBody ad_id,
            @Part("ad_images_ids") RequestBody ad_images_ids,
            @Part("ad_attributes_ids") RequestBody ad_attributes_ids,
            @Part List<MultipartBody.Part> ad_images,
            @Part("attributes") RequestBody attributes,
            @Part("lang") RequestBody lang
    );

    @GET("get/myads")
    Call<WebResponse<ArrayList<Ad>>> myAds(
            @Query("user_id") int user_id
    );

    @FormUrlEncoded
    @POST("update/devicetoken")
    Call<WebResponse> updateDeviceToken(
            @Field("user_id") int user_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );

    @FormUrlEncoded
    @POST("check/block/user")
    Call<WebResponse<User>> checkBlockStatus(
            @Field("user_id") int user_id
    );

}
