package com.wamztech.adhouse.retrofit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebResponse<T> {

    @SerializedName("Message")
    @Expose
    private String Message;

    @SerializedName("Result")
    @Expose
    private T Result;

    @SerializedName("Response")
    @Expose
    private String Response;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public T getResult() {
        return Result;
    }

    public void setResult(T result) {
        Result = result;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public boolean isSuccess() {
        if (Response.equalsIgnoreCase("2000")) {
            return true;
        } else {
            return false;
        }
    }
}

