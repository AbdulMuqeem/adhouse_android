package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.DialogListener;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.MyAdsItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAdsFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    GenericArrayAdapter adapterMyAds;
    String Ads;
    @BindView(R.id.lvAds)
    ListView lvAds;
    Unbinder unbinder;
    @BindView(R.id.emptyViewNoRecord)
    TextView emptyViewNoRecord;
    private ArrayList<Ad> arrMyAds;
    private int adsId;

    public static MyAdsFragment newInstance() {
        Bundle args = new Bundle();
        MyAdsFragment fragment = new MyAdsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrMyAds = new ArrayList<>();
        dataSet();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myads, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvAds.setEmptyView(emptyViewNoRecord);
        lvAds.setAdapter(adapterMyAds);
        lvAds.setOnItemClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        adapterMyAds.getList().clear();
        adapterMyAds.notifyDataSetChanged();
        serviceCallMyAds();
    }

    private void dataSet() {
        adapterMyAds = new GenericArrayAdapter<>(getActivity(), arrMyAds, new MyAdsItemBinder(this));
    }


    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btnDelete:

            {
                final int position = (int) v.getTag();
                final Ad entityAds = (Ad) adapterMyAds.getList().get((int) v.getTag());
                GenericDialog dFragment = GenericDialog.newInstance();
                dFragment.setListener(new DialogListener() {
                    @Override
                    public void onOkClicked() {
                        adapterMyAds.getList().remove(position);
                        removeServiceCall(entityAds.getId());
                        adapterMyAds.notifyDataSetChanged();
                    }
                    @Override
                    public void onCancelClicked() {
                    }
                });
                dFragment.setTitleContent(getString(R.string.delete), getString(R.string.are_you_sure_to_delete_this_item));
                dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");
            }
            break;

            case R.id.btnEdit: {
                Ad entityAds = (Ad) adapterMyAds.getList().get((int) v.getTag());
                addDockableFragment(SubmitAdFragment.newInstance(entityAds, true));
            }
            break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.My_Ads));
        titleBar.showBackButton(getMainActivity());
    }

    private Call<WebResponse<ArrayList<Ad>>> serviceMyAds;

    public void serviceCallMyAds() {
        serviceMyAds = WebServiceFactory.getInstance().myAds(prefHelper.getUser().getId());
        serviceMyAds.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapterMyAds.getList().clear();
                    adapterMyAds.getList().addAll(response.body().getResult());
                    adapterMyAds.notifyDataSetChanged();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private Call<WebResponse<Ad>> serviceDeleteAds;

    private void removeServiceCall(int adsId) {

        serviceDeleteAds = WebServiceFactory.getInstance().deleteAds(adsId);
        serviceDeleteAds.enqueue(new Callback<WebResponse<Ad>>() {

            @Override
            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    UtilHelper.showToast(getActivity(), response.body().getMessage());
                } else {
                    UtilHelper.showToast(getActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    @Override
    public void onPause() {
        if (serviceDeleteAds != null) {
            serviceDeleteAds.cancel();
        }
        if (serviceMyAds != null) {
            serviceMyAds.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceDeleteAds != null) {
            serviceDeleteAds.cancel();
        }
        if (serviceMyAds != null) {
            serviceMyAds.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Ad item = (Ad) adapterMyAds.getList().get(position);
        addDockableFragment(DetailFragment.newInstance(item));
    }
}
