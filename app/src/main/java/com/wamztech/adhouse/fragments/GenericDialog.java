package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseDialogFragment;
import com.wamztech.adhouse.interfaces.DialogListener;
import com.wamztech.adhouse.ui.views.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;



public class GenericDialog extends BaseDialogFragment implements View.OnClickListener {


    Button btnCancle;

    String title;
    String content;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.txtContent)
    AnyTextView txtContent;
    @BindView(R.id.btnNo)
    Button btnNo;
    @BindView(R.id.btnOK)
    Button btnOK;
    Unbinder unbinder;
    private DialogListener listener;

    public static GenericDialog newInstance() {
        Bundle args = new Bundle();
        GenericDialog fragment = new GenericDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_dialogue, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        txtTitle.setText(title);
        txtContent.setText(content);


        btnOK.setOnClickListener(this);
        btnCancle.setOnClickListener(this);

    }

    public void setTitleContent(String title, String content) {
        this.title = title;
        this.content = content;
    }

    private void initView(View view) {

        txtTitle = (AnyTextView) view.findViewById(R.id.txtTitle);
        txtContent = (AnyTextView) view.findViewById(R.id.txtContent);

        btnOK = (Button) view.findViewById(R.id.btnOK);
        btnCancle = (Button) view.findViewById(R.id.btnNo);
    }


    public DialogListener getListener() {
        return listener;
    }

    public void setListener(DialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNo:
                this.listener.onCancelClicked();
                dismiss();
                break;
            case R.id.btnOK:
                dismiss();
                this.listener.onOkClicked();

                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
