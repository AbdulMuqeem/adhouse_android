package com.wamztech.adhouse.fragments;


import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.fragments.user.LoginFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.DialogListener;
import com.wamztech.adhouse.models.SideMenu;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.SideMenuItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SideMenuFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.imgUser)
    ImageView imgUser;
    @BindView(R.id.txtUserName)
    AnyTextView txtUserName;
    @BindView(R.id.txtUserEmail)
    AnyTextView txtUserEmail;
    @BindView(R.id.lvSidemenu)
    ListView lvSidemenu;
    Unbinder unbinder;
    SideMenuItemBinder sideMenuItemBinder = new SideMenuItemBinder();
    private GenericArrayAdapter<SideMenu> adapter;
    private ArrayList<SideMenu> arrSideMenu;

    public static SideMenuFragment newInstance() {
        SideMenuFragment fragment = new SideMenuFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrSideMenu = new ArrayList<>();
        arrSideMenu.add(new SideMenu(0, R.drawable.sm_icon_home, getString(R.string.Home)));
        arrSideMenu.add(new SideMenu(1, R.drawable.sm_icon_notification, getString(R.string.Notifications)));
        arrSideMenu.add(new SideMenu(2, R.drawable.sm_icon_fav, getString(R.string.My_Favorites)));
        arrSideMenu.add(new SideMenu(3, R.drawable.sm_icon_ads, getString(R.string.My_Ads)));
        arrSideMenu.add(new SideMenu(4, R.drawable.sm_icon_profile, getString(R.string.My_Profile)));
        arrSideMenu.add(new SideMenu(5, R.drawable.help, getString(R.string.settings)));
        arrSideMenu.add(new SideMenu(6, R.drawable.ssm_icon_sub_com, getString(R.string.Subscription_Commercial)));

        if(prefHelper.getUser() != null) {
            if (prefHelper.getUser().isGuestUser()) {
                arrSideMenu.add(new SideMenu(7, R.drawable.sm_icon_logout, getString(R.string.Login)));
            } else {
                arrSideMenu.add(new SideMenu(7, R.drawable.sm_icon_logout, getString(R.string.Logout)));
            }
        }

        adapter = new GenericArrayAdapter<>(getActivity(), arrSideMenu, sideMenuItemBinder);
        adapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu, container, false);

        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindData();
        setListeners();

        lvSidemenu.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        //serviceCallUserSubscription();
    }

    private void setListeners() {
        lvSidemenu.setOnItemClickListener(this);
    }

    public void bindData() {
        String url;
        url = "http://www.stottpilates.com/studio/images/instructors/person-placeholder.png";
        if (prefHelper.getUser().isGuestUser()) {
            txtUserName.setText(getResources().getString(R.string.guest_user));
            txtUserEmail.setText("");
            ImageLoader.getInstance().displayImage(url, imgUser);
        } else {

            txtUserName.setText(prefHelper.getUser().getFullName());
            txtUserEmail.setText(prefHelper.getUser().getEmail());
            ImageLoader.getInstance().displayImage(prefHelper.getUser().getProfileImage(), imgUser);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        switch (arrSideMenu.get(position).getLabel_id()) {
            case SideMenu.SIDE_MENU_HOME: {
               if(!getMainActivity().isHomeOnTop()) {
                emptyBackStack();
                getMainActivity().initFragment();
               }
            }
            break;
            case SideMenu.SIDE_MENU_NOTIFICATION: {
                if (gestUser()) return;

                addDockableFragment(NotificationFragment.newInstance());
            }
            break;
            case SideMenu.SIDE_MENU_MY_FAVORITES: {
                if (gestUser()) return;
                addDockableFragment(MyFavFragment.newInstance());
            }
            break;
            case SideMenu.SIDE_MENU_MY_ADS: {
                if (gestUser()) return;
                addDockableFragment(MyAdsFragment.newInstance());
            }
            break;
            case SideMenu.SIDE_MENU_MY_PROFILE: {
                if (gestUser()) return;
                addDockableFragment(MyProfileFragment.newInstance(prefHelper.getUser().getId()));

            }
            break;
            case SideMenu.SIDE_MENU_SETTINGS: {
                addDockableFragment(SettingsFragment.newInstance());
            }
            break;

            case SideMenu.SIDE_MENU_SUBSCRIPTION: {

                if (gestUser()) return;

                addDockableFragment(SubscriptionFragment.newInstance());
            }
            break;
            case SideMenu.SIDE_MENU_LOGOUT: {

                    if (prefHelper.getUser().isGuestUser()) {
                        // Todo
                        getMainActivity().emptyBackStack();
                        addDockableFragment(LoginFragment.newInstance());

                        return;
                    }

                GenericDialog dFragment = GenericDialog.newInstance();
                dFragment.setListener(new DialogListener() {
                    @Override
                    public void onOkClicked() {
                        emptyBackStack();
                        addDockableFragment(LoginFragment.newInstance());
                        prefHelper.putUser(null);

                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
                dFragment.setTitleContent(getString(R.string.Logout), getString(R.string.want_to_logout) + "?");
                dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");
            }
            break;
        }
        drawerclose();

    }

    private boolean gestUser() {
        if (prefHelper.getUser().isGuestUser()) {
            UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
            drawerclose();
            return true;
        }
        return false;
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
