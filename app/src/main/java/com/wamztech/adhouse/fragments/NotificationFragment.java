package com.wamztech.adhouse.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.Notification;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.NotificationItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationFragment extends BaseFragment {


    @BindView(R.id.lvNotification)
    ListView lvNotification;
    Unbinder unbinder;
//    private ListView lvNotification;
    private GenericArrayAdapter<Ad> adapter;
    //    private ArrayList<Notification> arrNotification;
    private ArrayList<Ad> arrNotification;

    public static NotificationFragment newInstance() {

        Bundle args = new Bundle();

        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrNotification = new ArrayList<>();

        dataSet();

    }

    private void dataSet() {
        adapter = new GenericArrayAdapter<>(getActivity(), arrNotification, new NotificationItemBinder());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        initViews(view);
        lvNotification.setAdapter(adapter);
        serviceCallGetNotification();

    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.Notifications));
        titleBar.showBackButton(getMainActivity());


    }

    private Call<WebResponse<ArrayList<Notification>>> serviceGetNotification;
    public void serviceCallGetNotification() {
        serviceGetNotification = WebServiceFactory.getInstance().getNotification(prefHelper.getUser().getId());
        serviceGetNotification.enqueue(new Callback<WebResponse<ArrayList<Notification>>>() {
                    @Override
                    public void onResponse(Call<WebResponse<ArrayList<Notification>>> call, Response<WebResponse<ArrayList<Notification>>> response) {
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        try {
                            if (response != null && response.body() != null && response.body().isSuccess()) {
                                adapter.clear();
                                adapter.addAll(response.body().getResult());
                                adapter.notifyDataSetChanged();

                            } else {
//                                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<WebResponse<ArrayList<Notification>>> call, Throwable t) {
                        t.printStackTrace();
                    }


                });

    }

    @Override
    public void onPause() {
        if (serviceGetNotification != null) {
            serviceGetNotification.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceGetNotification != null) {
            serviceGetNotification.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

}
