package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.interfaces.DialogListener;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.MyFavouriteItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFavFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.lvFav)
    ListView lvFav;
    Unbinder unbinder;
    int isLike;
    @BindView(R.id.emptyViewNoRecord)
    TextView emptyViewNoRecord;
    private GenericArrayAdapter<Ad> adapter;
    private ArrayList<Ad> arrMyFavourite;

    public static MyFavFragment newInstance() {

        Bundle args = new Bundle();

        MyFavFragment fragment = new MyFavFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrMyFavourite = new ArrayList<>();

        dataSet();

    }

    private void dataSet() {
        adapter = new GenericArrayAdapter<>(getActivity(), arrMyFavourite, new MyFavouriteItemBinder(this));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_fav, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvFav.setEmptyView(emptyViewNoRecord);
        lvFav.setAdapter(adapter);
        serviceCallGetFavorite();

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.My_Favorites));
        titleBar.showBackButton(getMainActivity());
    }

    @Override
    public void onPause() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetFavourite != null) {
            serviceGetFavourite.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetFavourite != null) {
            serviceGetFavourite.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }
    private Call<WebResponse<Ad>> serviceAddFavourite;

    @Override
    public void onClick(final View v) {

        switch (v.getId()) {

            case R.id.imgFavourit: {
                final ImageView imgFavourite = (ImageView) v;
                final int position = (int) v.getTag();
                final Ad item = (Ad) adapter.getItem(position);

                imgFavourite.setEnabled(false);
                serviceAddFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), item.getId(), isLike);
                serviceAddFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                        imgFavourite.setEnabled(true);
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {

                            if (response.body().getResult().isFavourite()) {

                                adapter.clear();
                                item.setIsLike(0);
//                                adapter.remove(position);
//                                adapter.notifyDataSetChanged();


                            }
                        }
//                        adapter.remove(position);
                        adapter.notifyDataSetChanged();


                        GenericDialog dFragment = GenericDialog.newInstance();
                        dFragment.setListener(new DialogListener() {

                            @Override
                            public void onOkClicked() {
                                item.setIsLike(0);
                                adapter.remove(position);
                                adapter.notifyDataSetChanged();

                            }

                            @Override
                            public void onCancelClicked() {

                            }
                        });
                        dFragment.setTitleContent(getResources().getString(R.string._delete), getResources().getString(R.string.update_ads));
                        dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");
//                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());


                    }

                    @Override
                    public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                        if(imgFavourite != null) {
                            imgFavourite.setEnabled(true);
                        }
                        t.printStackTrace();
//                        UtilHelper.showToast(getMainActivity(), "No internet connection");
                    }
                });
            }
            break;


            case R.id.btnViewDetails:

            {
                int position = (int) v.getTag();
                Ad item = (Ad) adapter.getItem(position);
                addDockableFragment(DetailFragment.newInstance(item));


//                item.setIsLike(isLike);
//                if (!item.isFavourite()){
//                    adapter.remove(position);
//            }
//                adapter.notifyDataSetChanged();
            }
            break;
        }

    }

    private Call<WebResponse<ArrayList<Ad>>> serviceGetFavourite;
    public void serviceCallGetFavorite() {
        serviceGetFavourite = WebServiceFactory.getInstance().getFavourite(prefHelper.getUser().getId());
        serviceGetFavourite.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
                    @Override
                    public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {
                            adapter.clear();
                            adapter.addAll(response.body().getResult());
                            adapter.notifyDataSetChanged();

                        } else {
                            adapter.clear();
//                            adapter.addAll(response.body().getResult());
                            adapter.notifyDataSetChanged();
                            //UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                        t.printStackTrace();
//                        UtilHelper.showToast(getMainActivity(), "No internet connection");
                    }


                });

    }
}
