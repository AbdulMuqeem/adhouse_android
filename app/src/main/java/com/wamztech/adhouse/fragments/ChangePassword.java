package com.wamztech.adhouse.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePassword extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.edOldPassword)
    AnyEditTextView edOldPassword;
    @BindView(R.id.edNewPassword)
    AnyEditTextView edNewPassword;
    @BindView(R.id.edConfirmPassword)
    AnyEditTextView edConfirmPassword;
    @BindView(R.id.btnsave)
    AnyButton btnsave;
    Unbinder unbinder;

    public static ChangePassword newInstance() {
        ChangePassword fragment = new ChangePassword();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnsave.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnsave:{
                String oldPass = edOldPassword.getText().toString();
                String newPass = edNewPassword.getText().toString();
                String comPass = edConfirmPassword.getText().toString();

                if (oldPass.equals("") && newPass.equals("") && comPass.equals("") ) {
                    edOldPassword.setError("Enter Old Password");
                    edNewPassword.setError("Enter New Password");
                    edConfirmPassword.setError("Enter Confirm Password");

                }
                else if (!isValidPassword(oldPass) || !isValidPassword(newPass)) {
                    edOldPassword.setError("Invalid Password");
                    edNewPassword.setError("Invalid Password");
                } else if (!comPass.equals(newPass)) {
                    edConfirmPassword.setError("Password not match ");
                }
                else {
//                        addDockableFragment(MyProfileFragment.newInstance(prefHelper.getUser().getId()));
                    UpdatePassword();
                    }
            }break;


        }
    }

    private Call<WebResponse<User>> serviceChangePassword;
    public void UpdatePassword(){

        String oldPass = edOldPassword.getText().toString();
        String newPass = edNewPassword.getText().toString();
        String comPass = edConfirmPassword.getText().toString();
        serviceChangePassword = WebServiceFactory.getInstance().changePassword(prefHelper.getUser().getId(),oldPass,newPass,comPass);
        serviceChangePassword.enqueue(new Callback<WebResponse<User>>() {
                    @Override
                    public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {
//                            prefHelper.getUserId();
                          //  addDockableFragment(MyProfileFragment.newInstance(prefHelper.getUser().getId()));
                            getMainActivity().popFragment();
                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
//                            showProfile();

                        } else {
                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                        t.printStackTrace();
                        UtilHelper.showToast(getMainActivity(), t.getMessage());
                    }


                });


        }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }

        return false;
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {

        titleBar.resetTitleBar();

        titleBar.showLeftButton();
        titleBar.setHeading(getString(R.string.change_password));
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popBackStack();

            }
        });


    }

    @Override
    public void onPause() {
        if (serviceChangePassword != null) {
            serviceChangePassword.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceChangePassword != null) {
            serviceChangePassword.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }
}
