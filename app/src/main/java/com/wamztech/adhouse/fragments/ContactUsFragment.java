package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ctrlplusz.anytextview.AnyButton;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.Contact;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.edUserName)
    AnyEditTextView edUserName;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.edPhone)
    AnyEditTextView edPhone;
    @BindView(R.id.edText)
    AnyEditTextView edText;
    @BindView(R.id.btnSend)
    AnyButton btnSend;
    Unbinder unbinder;
    private Call<WebResponse<Contact>> serviceCallContact;

    public static ContactUsFragment newInstance() {
        Bundle args = new Bundle();
        ContactUsFragment fragment = new ContactUsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSend.setOnClickListener(this);
        showUserData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSend:
                String name = edUserName.getText().toString();
                String email = edEmail.getText().toString();
                String phone = edPhone.getText().toString();
                String description = edText.getText().toString();

                if (name.equals("") && email.equals("") && phone.equals("") && description.equals("")) {
                    edUserName.setError(getResources().getString(R.string.enter_name));
                    edEmail.setError(getResources().getString(R.string.enter_email));
                    edPhone.setError(getResources().getString(R.string.enter_phone));
                    edText.setError(getString(R.string.enter_your_message));

                } else if (!isValidEmail(email)) {
                    edEmail.setError(getResources().getString(R.string.invalid_email));
                } else if (!isValidatePhone(phone)) {
                    edPhone.setError(getResources().getString(R.string.invalid_phone_no));
                } else if (description.equals("")) {
                    edText.setError(getString(R.string.enter_your_message));
                } else if (name.equals("")) {
                    edUserName.setError(getResources().getString(R.string.enter_name));
                } else if (email.equals("")) {
                    edEmail.setError(getResources().getString(R.string.enter_email));
                } else if (phone.equals("")) {
                    edPhone.setError(getResources().getString(R.string.enter_phone));
                } else {
                    serviceCallContact();
                }
                break;
        }

    }

    private boolean isValidatePhone(String phone) {
        if (phone.length() >= 8 && phone.length() <= 50) {
            return true;
        }
        return false;
    }

    public void showUserData() {
        edUserName.setText(prefHelper.getUser().getFullName());
        edEmail.setText(prefHelper.getUser().getEmail());
        edPhone.setText(prefHelper.getUser().getPhone());
    }

    public void serviceCallContact() {
        String name = edUserName.getText().toString();
        String email = edEmail.getText().toString();
        String phone = edPhone.getText().toString();
        String description = edText.getText().toString();
        btnSend.setEnabled(false);

        serviceCallContact = WebServiceFactory.getInstance().contatct(name, email, phone, description);
        serviceCallContact.enqueue(new Callback<WebResponse<Contact>>() {
            @Override
            public void onResponse(Call<WebResponse<Contact>> call, Response<WebResponse<Contact>> response) {
                btnSend.setEnabled(true);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    popBackStack();
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                } else {
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Contact>> call, Throwable t) {
                if (btnSend != null) {
                    btnSend.setEnabled(true);
                }
                t.printStackTrace();
            }
        });
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.contact_form));
        titleBar.showBackButton(getMainActivity());
    }

    @Override
    public void onPause() {
        if (serviceCallContact != null) {
            serviceCallContact.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceCallContact != null) {
            serviceCallContact.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }
}
