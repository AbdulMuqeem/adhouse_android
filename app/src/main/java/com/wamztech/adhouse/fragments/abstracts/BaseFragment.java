package com.wamztech.adhouse.fragments.abstracts;


import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.wamztech.adhouse.activities.MainActivity;
import com.wamztech.adhouse.helpers.BasePreferenceHelper;
import com.wamztech.adhouse.helpers.FileUtils;
import com.wamztech.adhouse.ui.views.TitleBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public abstract class BaseFragment extends Fragment {

    protected BasePreferenceHelper prefHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefHelper = new BasePreferenceHelper(getMainActivity());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) context).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void drawerclose() {
        ((MainActivity) getActivity()).drawerclose();
    }

    protected void draweropen() {
        (getMainActivity()).drawerOpen();
    }

    protected void replaceDockableSupportFragment(Fragment fragment) {
        (getMainActivity()).replaceDockableSupportFragment(fragment);
    }

    protected void setLeftButton(int drawable, View.OnClickListener listener) {
        (getMainActivity()).setLeftButton(drawable, listener);
    }

    protected void setRightButton(int drawable, View.OnClickListener listener) {
        (getMainActivity()).setRightButton(drawable, listener);
    }

    protected void addDockableFragment(Fragment fragment) {
        if (getMainActivity() != null)
            (getMainActivity()).addDockableFragment(fragment);
    }

    protected boolean isHomeOnTop() {
        if (getMainActivity() != null) {
            return getMainActivity().isHomeOnTop();
        }
        return true;
    }

    private void addDockablDrawerLock() {
        ((MainActivity) getActivity()).drawerLock();
    }

    private void addDockablDrawerUnLock() {
        ((MainActivity) getActivity()).drawerUnLock();
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();

        setTitleBar((getMainActivity()).getMainTitleBar());

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    public abstract void setTitleBar(TitleBar titleBar);


    protected void popBackStack() {

        if (getMainActivity() == null) {
            return;
        }

        getMainActivity().getSupportFragmentManager().popBackStack();
    }

    protected void popBackstackTillEntry() {
        getMainActivity().popBackstackTillEntry(1);
    }

    protected void emptyBackStack() {
        if (getMainActivity() == null) {
            return;
        }
        getMainActivity().emptyBackStack();
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }


    @NonNull
    public MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri

        File file = FileUtils.getFile(getMainActivity(), fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/*"),
                        file
                );


        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }


    protected void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getMainActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public String getFormattedNumber(double value, int factor) {
        NumberFormat format1 = NumberFormat.getInstance();
        format1.setMaximumFractionDigits(factor);
        format1.setMinimumFractionDigits(factor);
        return format1.format(value);

    }


    public File FileFromUrl(String imageUrl) throws IOException {
        File fileForImage = new File(Environment.getExternalStorageDirectory() + "/image.jpg");
        if (!fileForImage.exists()) {
            fileForImage.createNewFile();
        }
        InputStream sourceStream;
        File cachedImage = ImageLoader.getInstance().getDiscCache().get(imageUrl);
        if (cachedImage != null && cachedImage.exists()) { // if image was cached by UIL
            sourceStream = new FileInputStream(cachedImage);
        } else { // otherwise - download image
            ImageDownloader downloader = new BaseImageDownloader(getContext());
            sourceStream = downloader.getStream(imageUrl, null);
        }

        if (sourceStream != null) {
            try {
                OutputStream targetStream = new FileOutputStream(fileForImage);
                try {
                    IoUtils.copyStream(sourceStream, targetStream, null);
                } finally {
                    targetStream.close();
                }
            } finally {
                sourceStream.close();
            }
        }

        return fileForImage;
    }


    public boolean setDefaultLocale() {
        Resources resources = getMainActivity().getResources();
        Resources resourcesApp = getMainActivity().getApplicationContext().getResources();
        String localLanguage = resources.getConfiguration().locale.getLanguage();
        boolean isLanguageChanged = !prefHelper.getLang().equalsIgnoreCase(localLanguage);
        if (isLanguageChanged) {


            Locale locale = new Locale(prefHelper.getLang());
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            resources.updateConfiguration(config, resources.getDisplayMetrics());
            resourcesApp.updateConfiguration(config, resources.getDisplayMetrics());

            //for API 25
            Configuration configuration = resources.getConfiguration();
            configuration.setLocale(locale);
            getMainActivity().getApplicationContext().createConfigurationContext(configuration);
            getMainActivity().createConfigurationContext(configuration);

            getActivity().recreate();
            ((Activity) getMainActivity()).getWindow().getDecorView().setLayoutDirection(Locale.getDefault().getLanguage().equalsIgnoreCase("ar")
                    ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
        return isLanguageChanged;
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * This generic null string validator to be used FormEditText
     * <p/>
     * Usage : formEditText.addValicator(new EmptyStringValidator);
     *
     * @return Boolean and setError on respective field.
     */
    protected class EmptyStringValidator extends Validator {

        public EmptyStringValidator() {
            super("The field must not be empty ");
        }

        @Override
        public boolean isValid(EditText et) {
            return et.getText().toString().trim().length() >= 1;
        }

    }

    protected class MinLengthValidator extends Validator {

        private int length;

        public MinLengthValidator(int length) {
            super("Minimun length should be " + length + " digits");
            this.length = length;
        }

        @Override
        public boolean isValid(EditText et) {
            return TextUtils.getTrimmedLength(et.getText()) >= length;
        }

    }

}
