package com.wamztech.adhouse.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.andreabaccega.widget.FormEditText;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.wamztech.adhouse.Constants;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.AddressResultReceiver;
import com.wamztech.adhouse.helpers.CameraGalleryActionDialog;
import com.wamztech.adhouse.helpers.CurrentLocationHelper;
import com.wamztech.adhouse.helpers.FetchAddressIntentService;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.DialogListener;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.interfaces.OnItemDelete;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.AdImages;
import com.wamztech.adhouse.models.Attributes;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.models.Dropdownlist;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.ExpandedListView;
import com.wamztech.adhouse.ui.views.HorizontalListView;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AddImagesItemBinder;
import com.wamztech.adhouse.ui.views.viewbinders.CategoryBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SubAdsAttributesBinderV1;
import com.wamztech.adhouse.ui.views.viewbinders.SubCategoryBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

// NEW,

public class SubmitAdFragment extends BaseFragment implements View.OnClickListener, OnItemDelete, OnActivityResult,
        AdapterView.OnItemClickListener
//        CurrentLocationHelper.onLocationHelper, AddressResultReceiver.IAddressFetch
{

    public static final String ENTITY_KEY = "ENTITY_KEY";
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2222;
    private static final String IS_FROM_UPDATE = "IS_FROM_UPDATE";

    private String TAG = "SubmitAdFragment";


    private final int MAX_IMAGES = 15;
    @BindView(R.id.txtLocation)
//    FormAutoCompleteTextView txtLocation;
            AnyTextView txtLocation;
    @BindView(R.id.edAdTitle)
    AnyEditTextView edAdTitle;
    @BindView(R.id.currLocation)
    ImageView currLocation;
    @BindView(R.id.txtCategory)
    AnyTextView txtCategory;
    @BindView(R.id.txtSelectCategory)
    AnyTextView txtSelectCategory;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.txtUploadPhoto)
    AnyTextView txtUploadPhoto;
    @BindView(R.id.imgAdding)
    ImageView imgAdding;
    @BindView(R.id.btn_addimage)
    LinearLayout btnAddimage;
    @BindView(R.id.hlvAddingImages)
    HorizontalListView hlvAddingImages;
    @BindView(R.id.edPrice)
    AnyEditTextView edPrice;
    @BindView(R.id.edUnit)
    AnyTextView edUnit;
    @BindView(R.id.edDescription)
    AnyEditTextView edDescription;
    @BindView(R.id.txtSelectFeature)
    AnyTextView txtSelectFeature;
    @BindView(R.id.txtFeatured)
    AnyTextView txtFeatured;
    @BindView(R.id.spFeatured)
    Spinner spFeatured;
    @BindView(R.id.edMobile)
    AnyEditTextView edMobile;
    AnyButton btnPostAd;
    Unbinder unbinder;
    @BindView(R.id.listCategoriesAttributes)
    ExpandedListView listCategoriesAttributes;
    @BindView(R.id.txtSubCategory)
    AnyTextView txtSubCategory;
    @BindView(R.id.txtSelectSubCategory)
    AnyTextView txtSelectSubCategory;
    @BindView(R.id.spSubCategory)
    Spinner spSubCategory;
    @BindView(R.id.classified)
    RelativeLayout classified;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.progressUploading)
    ProgressBar progressUploading;
    Dropdownlist dropdownlistSelectItem;
    SubAdsAttributesBinderV1 subAdsAttributesBinderV1;
    private String item;
    private ArrayList<Category> arrSpCategories;
    private ArrayList<Category> arrSpSubCategories;
    private GenericArrayAdapter<Category> adapterCategory;
    private GenericArrayAdapter<Category> adapterSubCategory;
    private GenericArrayAdapter<Attributes> adapterAttributes;
    private int categoryID = -1;
    private GenericArrayAdapter<String> adapterImagesAdd;
    private ArrayList<Attributes> arrAttributes;
    private ArrayAdapter<String> adapterSpinner;
    private int isFeatured;
    private int subCategory;
    private String locationName;
    private double latitude;
    private double longitude;
    private String complete_image_path;
    private AnyEditTextView edtAttribute;
    private Spinner spAttribute;
    private ArrayList<Attributes> arrEdAtt;
    private JSONArray arrJason;
    private boolean isFromUpdate;
    private Ad adsEntity;
    private int ad_id;
    private String ad_images_ids = "";
    private String ad_attributes_ids = "";
    //    private PlaceAutocompleteAdapter mAdapter;
    private Call<WebResponse<Ad>> servicePostAds;
    private Call<WebResponse<Ad>> serviceUpdateAds;
    private Call<WebResponse<ArrayList<Category>>> serviceCallCategoryAds;
    private Call<WebResponse<ArrayList<Attributes>>> serviceCallAttributeAds;
    private Call<WebResponse<ArrayList<Category>>> serviceCallSubCategoryAds;

    private FusedLocationProviderClient fusedLocationClient;

    public static SubmitAdFragment newInstance(Ad adsEntity, Boolean isFromUpdate) {
        Bundle args = new Bundle();
        SubmitAdFragment fragment = new SubmitAdFragment();
        args.putString(ENTITY_KEY, new Gson().toJson(adsEntity));
        args.putBoolean(IS_FROM_UPDATE, isFromUpdate);
        fragment.setArguments(args);
        return fragment;

    }

    public static SubmitAdFragment newInstance(Boolean isFromUpdate) {
        Bundle args = new Bundle();
        SubmitAdFragment fragment = new SubmitAdFragment();
        args.putBoolean(IS_FROM_UPDATE, isFromUpdate);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String json = getArguments().getString(ENTITY_KEY);
        adsEntity = new Gson().fromJson(json, Ad.class);
        isFromUpdate = getArguments().getBoolean(IS_FROM_UPDATE);
        initArrayList();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getMainActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submit_ads, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnPostAd = (AnyButton) view.findViewById(R.id.btnPostAd);
        serviceCallCategory();
        if (isFromUpdate) {
            setUpdateData();
        } else {
            btnPostAd.setText(getResources().getString(R.string.post_ad));
            initCustomSpinner();

            edUnit.setText(prefHelper.getUser().getCurrency_symbol());

        }
        setAdapters();
        setListeners();
        getMainActivity().setOnActivityResult(SubmitAdFragment.this);

        dropdownlistSelectItem = new Dropdownlist();
        dropdownlistSelectItem.setAttributeParentId(0);
        dropdownlistSelectItem.setAttributeParentValueId(0);
        dropdownlistSelectItem.setId(0);
        dropdownlistSelectItem.setValue(getString(R.string.select_type));
    }

    private void setListeners() {
        btnPostAd.setOnClickListener(SubmitAdFragment.this);
        listCategoriesAttributes.setClickable(false);
//        mLocationHelper = new CurrentLocationHelper(getContext());
//        mLocationHelper.setOnLocationHelperListener(this);
//        mLocationHelper.getLastLocationAddress();

        currLocation.performClick();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();

        if (isFromUpdate) {
            titleBar.setHeading(getString(R.string.update_ads));
        } else {
            titleBar.setHeading(getString(R.string.submit_ad));
        }
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
    }

    @Override
    public void itemDelect(final int position) {


        GenericDialog dFragment = GenericDialog.newInstance();

        dFragment.setListener(new DialogListener() {


            @Override
            public void onOkClicked() {
                if (isFromUpdate) {
                    if (adsEntity.getAdImages().get(position).getId() == null) {
                        adsEntity.getAdImages().remove(position);
                        adapterImagesAdd.remove(position);

                        return;
                    }
                    ad_images_ids += (adsEntity.getAdImages().get(position).getId() + ",");

                    adsEntity.getAdImages().remove(position);
                    adapterImagesAdd.remove(position);
                    adapterImagesAdd.notifyDataSetChanged();
                } else {
                    adapterImagesAdd.remove(position);
                    adapterImagesAdd.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelClicked() {

            }
        });
        dFragment.setTitleContent(getString(R.string.Delete), getString(R.string.delete_item) + "?");
        dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");

    }

    @OnClick({R.id.txtSelectCategory, R.id.imgAdding, R.id.txtSelectSubCategory, R.id.btnPostAd, R.id.txtLocation, R.id.currLocation})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSelectCategory:
                spCategory.performClick();
                break;
            case R.id.currLocation:
                callForCurrentAddress();


                break;
            case R.id.txtLocation:
                callPlaceAutocompleteActivityIntent();
                break;
            case R.id.imgAdding:

                if (adapterImagesAdd.getList().size() < MAX_IMAGES) {
                    showCameraDialog();

                } else {
                    UtilHelper.showToast(getMainActivity(), "Max. images added");
                }
                break;
            case R.id.txtSelectSubCategory:
                spSubCategory.performClick();
                break;
            case R.id.btnPostAd:
                FormEditText[] allFields;

                allFields = new FormEditText[]{edAdTitle, edDescription,
                        edMobile};

                final int user_id = prefHelper.getUser().getId();
                final String title = edAdTitle.getText().toString().trim();
                final String price = edPrice.getText().toString().trim();
                //final String unit = edUnit.getText().toString().trim();
                final String description = edDescription.getText().toString().trim();
                final int featured = isFeatured;
                final String phone = edMobile.getText().toString();
                final String area = txtLocation.getText().toString().trim();
                final String _latitude = String.valueOf(latitude);
                final String _longitude = String.valueOf(longitude);

                if (latitude == 0 || longitude == 0) {
                    txtLocation.setError("Please select location");
                    UtilHelper.showToast(getMainActivity(), getString(R.string.field_cannot_be_empty));
                    return;
                }

                boolean allValid = true;
                for (FormEditText field : allFields)
                    allValid = field.testValidity() && allValid;
                if (allValid) {

                    if (!(phone.length() >= 8 && phone.length() < 51)) {
                        UtilHelper.showToast(getMainActivity(), getString(R.string.valid_number_));
                        return;
                    }

                    try {
                        arrJason = jsonAttArray();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (arrJason == null) {
                        return;
                    }

                    if (isFromUpdate) {
                        List<MultipartBody.Part> subImagesParts = new ArrayList<>();

                        if (adapterImagesAdd.getList().size() > 0 && adapterImagesAdd.getList().size() <= MAX_IMAGES) {
                            for (int index = 0; index < adapterImagesAdd.getList().size(); index++) {


                                if (!String.valueOf(adapterImagesAdd.getItem(index)).contains("http")) {
                                    String imgUrl = adapterImagesAdd.getList().get(index);
                                    subImagesParts.add(prepareFilePart("ad_images[]", Uri.parse(imgUrl)));
                                } else {
                                }
                            }
                        } else {
                            UtilHelper.showToast(getMainActivity(), "Check the limit i-e min 1 and max 15");
                            return;


                        }
                        if (!ad_images_ids.equals(""))
                            ad_images_ids = ad_images_ids.substring(0, ad_images_ids.length() - 1) + "";
                        postUpdateCall(user_id, title, price, description, featured, phone, area, _latitude, _longitude, categoryID, subCategory, ad_id, ad_attributes_ids, ad_images_ids, subImagesParts, arrJason);

                    } else {
                        List<MultipartBody.Part> subImagesParts = new ArrayList<>();
                        if (adapterImagesAdd.getList().size() > 0 && adapterImagesAdd.getList().size() <= MAX_IMAGES) {
                            for (int index = 0; index < adapterImagesAdd.getList().size(); index++) {
                                String imgUrl = adapterImagesAdd.getList().get(index);
                                subImagesParts.add(prepareFilePart("ad_images[]", Uri.parse(imgUrl)));
                            }
                        } else {
                            UtilHelper.showToast(getMainActivity(), getString(R.string.minimum_1_photo_required));
                            return;

                        }
                        postAdsCall(user_id, title, price, description, featured, phone, area, _latitude, _longitude, categoryID, subCategory, subImagesParts, arrJason);
                    }
                }
                break;
        }
    }

    private void callForCurrentAddress() {
        fusedLocationClient.getLastLocation().addOnCompleteListener(
                new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location != null) {
                            try {
                                testCurr(location);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );

    }


    private void setUpdateData() {

        initSpFeature();

        ArrayList<String> arrImg = new ArrayList<>();

        for (int index = 0; index < adsEntity.getAdImages().size(); index++) {

            arrImg.add(adsEntity.getAdImages().get(index).getImageUrl());
        }
        adapterImagesAdd = new GenericArrayAdapter<>(getActivity(), arrImg, new AddImagesItemBinder(this));

        latitude = Double.valueOf(adsEntity.getLatitude());
        longitude = Double.valueOf(adsEntity.getLongitude());

        ArrayList<String> attValue = new ArrayList<>();
        adapterAttributes.getList().clear();
        for (int index = 0; index < adsEntity.getAdDetails().size(); index++) {
            arrAttributes.add(adsEntity.getAdDetails().get(index).getAttributes());
            attValue.add(adsEntity.getAdDetails().get(index).getAttributeValue());
            ad_attributes_ids += (adsEntity.getAdDetails().get(index).getId() + ",");
        }

        if (!ad_attributes_ids.equals("")) {
            ad_attributes_ids = ad_attributes_ids.substring(0, ad_attributes_ids.length() - 1) + "";
            subAdsAttributesBinderV1 = new SubAdsAttributesBinderV1(getMainActivity(), attValue);
            adapterAttributes = new GenericArrayAdapter<>(getActivity(), arrAttributes, subAdsAttributesBinderV1);
            adapterAttributes.notifyDataSetChanged();
        }

    }

    private void showCameraDialog() {
        final CameraGalleryActionDialog dialog = new CameraGalleryActionDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Dialog);
        dialog.setOnCamGalleryActionListener(new CameraGalleryActionDialog.CamGalleryActionListener() {
            @Override
            public void setCameraOpen(File filePath) {

                if (filePath == null) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                if (filePath.isFile()) {

                    String path_prefix = "file:///";
                    complete_image_path = path_prefix + filePath.getAbsolutePath();

                    adapterImagesAdd.add(complete_image_path);
                    adapterImagesAdd.notifyDataSetChanged();
                    if (isFromUpdate) {
                        AdImages adImages = new AdImages();
                        adImages.setImages(complete_image_path);
                        adsEntity.getAdImages().add(adImages);
                    }

                } else {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                }
            }


            @Override
            public void closeClick() {
                dialog.dismiss();
            }

            @Override
            public void browsePic(String strFilePath, File imgFile) {

                if (strFilePath == null || strFilePath.trim().length() <= 0) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                String path_prefix = "file:///";
                complete_image_path = path_prefix + strFilePath;
                adapterImagesAdd.add(complete_image_path);
                adapterImagesAdd.notifyDataSetChanged();
                if (isFromUpdate) {
                    AdImages adImages = new AdImages();
                    adImages.setImages(complete_image_path);
                    adsEntity.getAdImages().add(adImages);
                }

            }
        });

        dialog.show(getFragmentManager(), "CameraGalleryActionDialog");
    }

    private void setAdapters() {
        spCategory.setAdapter(adapterCategory);
        spSubCategory.setAdapter(adapterSubCategory);
        hlvAddingImages.setAdapter(adapterImagesAdd);
        listCategoriesAttributes.setAdapter(adapterAttributes);
    }

    private void initArrayList() {
        arrSpCategories = new ArrayList<>();
        arrSpSubCategories = new ArrayList<>();
        arrAttributes = new ArrayList<>();
        arrEdAtt = new ArrayList<>();

        adapterImagesAdd = new GenericArrayAdapter<>(getActivity(), new ArrayList<String>(), new AddImagesItemBinder(this));
        subAdsAttributesBinderV1 = new SubAdsAttributesBinderV1(getMainActivity(), null);
        adapterAttributes = new GenericArrayAdapter<>(getActivity(), arrAttributes, subAdsAttributesBinderV1);
        adapterCategory = new GenericArrayAdapter<>(getMainActivity(), arrSpCategories, new CategoryBinder());
        adapterSubCategory = new GenericArrayAdapter<>(getMainActivity(), arrSpSubCategories, new SubCategoryBinder());
    }
    /*
      Service Call
    */

    private void initCustomSpinner() {

        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                item = arrSpCategories.get(position).getName();
                categoryID = arrSpCategories.get(position).getId();
                txtSelectCategory.setText(item);

                //if (arrSpCategories.get(position).getSubcategory() == 1) { old  => 26/02/2019
                if (arrSpCategories.get(position).getId() != 0 && arrSpCategories.get(position).getId() != 44) {
                    txtSubCategory.setVisibility(View.VISIBLE);
                    classified.setVisibility(View.VISIBLE);
                    serviceCallSubCategory(categoryID);

                } else {
                    txtSubCategory.setVisibility(View.GONE);
                    classified.setVisibility(View.GONE);
                    subCategory = 0;
                    callAttributes(categoryID, subCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        txtSelectFeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spFeatured.performClick();
            }
        });

        spFeatured.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    isFeatured = 1;
                    txtSelectFeature.setText(getResources().getString(R.string.yes));
                } else {
                    isFeatured = 0;
                    txtSelectFeature.setText(getResources().getString(R.string.no));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        List<String> categories = new ArrayList<String>();
        categories.add(getString(R.string.yes));
        categories.add(getString(R.string.no));
        adapterSpinner = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, categories);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFeatured.setAdapter(adapterSpinner);

    }

    private void initSpFeature() {

        txtSelectFeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spFeatured.performClick();
            }
        });

        spFeatured.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    isFeatured = 1;
                    txtSelectFeature.setText("Yes");
                } else if (position == 1) {
                    isFeatured = 0;
                    txtSelectFeature.setText("No");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        List<String> categories = new ArrayList<String>();
        categories.add("Yes");
        categories.add("No");
        adapterSpinner = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, categories);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFeatured.setAdapter(adapterSpinner);
    }

    private void classifiedCategory() {

        if (spSubCategory != null && txtSelectSubCategory != null) {
            spSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = arrSpSubCategories.get(position).getName();
                    subCategory = arrSpSubCategories.get(position).getId();
                    txtSelectSubCategory.setText(item);
                    callAttributes(categoryID, subCategory);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void preFilledData() {
        ad_id = adsEntity.getId();
        if (btnPostAd != null
                && btnPostAd != null
                && txtLocation != null
                && edAdTitle != null
                && edPrice != null
                && edDescription != null
                && edMobile != null
                && txtSelectCategory != null
                && txtSelectSubCategory != null
        ) {
            btnPostAd.setText(getResources().getString(R.string.update_ads));
            txtLocation.setText(adsEntity.getArea());
            edAdTitle.setText(adsEntity.getTitle());
            edPrice.setText(adsEntity.getPrice());
            edUnit.setText(adsEntity.getCurrency_symbol());
            edDescription.setText(adsEntity.getDescription());
            edMobile.setText(adsEntity.getPhone());
            categoryID = adsEntity.getCategoryId();
            subCategory = adsEntity.getSubCategoryId();
            isFeatured = adsEntity.getFeatured();


            for (int index = 0; index < arrSpCategories.size(); index++) {

                if (categoryID == arrSpCategories.get(index).getId()) {
                    txtSelectCategory.setText(arrSpCategories.get(index).getName());
                }


            }

            txtSelectSubCategory.setEnabled(false);
            txtSelectCategory.setEnabled(false);

            boolean hasSubCategories = false;
            for (int i = 0; i < arrSpCategories.size(); i++) {
                if (arrSpCategories.get(i).getId() == categoryID) {
                    if (arrSpCategories.get(i).getSubcategory() == 1) {
                        hasSubCategories = true;
                        break;
                    }
                }
            }

            if (hasSubCategories) {
                txtSubCategory.setVisibility(View.VISIBLE);
                classified.setVisibility(View.VISIBLE);
                serviceCallSubCategory(categoryID);
            } else {
                txtSubCategory.setVisibility(View.GONE);
                classified.setVisibility(View.GONE);
            }

            if (isFeatured == 1) {
                spFeatured.setSelection(0);
                txtSelectFeature.setText(getString(R.string.yes));
            } else if (isFeatured == 0) {
                spFeatured.setSelection(1);
                txtSelectFeature.setText(getString(R.string.no));
            }
        }
    }

    private JSONArray jsonAttArray() throws JSONException {
        arrEdAtt.clear();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < adapterAttributes.getCount(); i++) {
            edtAttribute = (AnyEditTextView) listCategoriesAttributes.getChildAt(i).findViewById(R.id.edtCategory);
            AnyTextView txtSpinner = (AnyTextView) listCategoriesAttributes.getChildAt(i).findViewById(R.id.txtSpinnerCat);
            spAttribute = (Spinner) listCategoriesAttributes.getChildAt(i).findViewById(R.id.spAttrCategory);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", ((Attributes) adapterAttributes.getItem(i)).getId());


            if (((Attributes) adapterAttributes.getItem(i)).getAttributeType().equals("text")) {
                if (edtAttribute.getText().toString().length() > 0) {
                    jsonObject.put("value", edtAttribute.getText().toString());
                } else {
                    UtilHelper.showToast(getMainActivity(), "Please fill all fields");
                    return null;
                }
            }
            if (((Attributes) adapterAttributes.getItem(i)).getAttributeType().equals("dropdown")) {
                String value = ((Dropdownlist) spAttribute.getSelectedItem()).getValue();
                if (!value.equals(getString(R.string.select_type))) {
                    jsonObject.put("value", ((Dropdownlist) spAttribute.getSelectedItem()).getValue());
                    jsonObject.put("selection_id", ((Dropdownlist) spAttribute.getSelectedItem()).getId());
                } else {
                    UtilHelper.showToast(getMainActivity(), "Please fill all fields");
                    return null;
                }
            }
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    private void postAdsCall(int user_id, String title, String price, String
            description, int featured, String phone, String area,
                             String _latitude, String _longitude, int categoryID, int subCategory,
                             List<MultipartBody.Part> subImagesParts, JSONArray arrJason) {
        btnPostAd.setEnabled(false);
        progressUploading.setVisibility(View.VISIBLE);
        if (!title.equals("") && !price.equals("") && !description.equals("") && !phone.equals("") && !area.equals("")) {
            servicePostAds = WebServiceFactory.getInstance().postAds(
                    createPartFromString(user_id + ""),
                    createPartFromString(title),
                    createPartFromString(price),
                    createPartFromString(description),
                    createPartFromString(featured + ""),
                    createPartFromString(phone),
                    createPartFromString(area),
                    createPartFromString(_latitude),
                    createPartFromString(_longitude),
                    createPartFromString(categoryID + ""),
                    createPartFromString(subCategory + ""),
                    subImagesParts,
                    createPartFromString(arrJason.toString()),
                    createPartFromString(prefHelper.getLang())
            );

            servicePostAds.enqueue(new Callback<WebResponse<Ad>>() {
                @Override
                public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                    btnPostAd.setEnabled(true);
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }
                    if (response.body().isSuccess()) {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        popBackStack();
                    } else {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                    }
                    progressUploading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                    if (btnPostAd != null) {
                        btnPostAd.setEnabled(true);
                        progressUploading.setVisibility(View.GONE);
                    }
                    t.printStackTrace();
                }
            });


        } else {
            UtilHelper.showToast(getMainActivity(), "Please fill all fields");
        }
    }

    private void postUpdateCall(int user_id, String title, String
            price, String description, int featured, String phone, String area,
                                String _latitude, String _longitude, int categoryID, int subCategory,
                                int ad_id, String ad_attributes_ids, String
                                        ad_images_ids, List<MultipartBody.Part> subImagesParts, JSONArray
                                        arrJason) {
        btnPostAd.setEnabled(false);
        progressUploading.setVisibility(View.VISIBLE);
        if (!title.equals("") && !price.equals("") && !description.equals("") && !phone.equals("") && !area.equals("")) {


            serviceUpdateAds = WebServiceFactory.getInstance().postAdsUpdate(
                    createPartFromString(user_id + ""),
                    createPartFromString(title),
                    createPartFromString(price),
                    createPartFromString(description),
                    createPartFromString(featured + ""),
                    createPartFromString(phone),
                    createPartFromString(area),
                    createPartFromString(_latitude),
                    createPartFromString(_longitude),
                    createPartFromString(categoryID + ""),
                    createPartFromString(subCategory + ""),
                    createPartFromString(ad_id + ""),
                    createPartFromString(ad_images_ids + ""),
                    createPartFromString(ad_attributes_ids + ""),
                    subImagesParts,
                    createPartFromString(arrJason.toString()),
                    createPartFromString(prefHelper.getLang())
            );
            serviceUpdateAds.enqueue(new Callback<WebResponse<Ad>>() {
                @Override
                public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }
                    if (response.body().isSuccess()) {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        popBackStack();

                    } else {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                    }
                    btnPostAd.setEnabled(true);
                    progressUploading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                    if (btnPostAd != null) {
                        btnPostAd.setEnabled(true);
                        progressUploading.setVisibility(View.GONE);
                    }
                    t.printStackTrace();
                }
            });

        } else {
            UtilHelper.showToast(getMainActivity(), "Fields cannot be empty");
        }
    }

    public void serviceCallCategory() {
        serviceCallCategoryAds = WebServiceFactory.getInstance().getCategory(prefHelper.getLang());
        serviceCallCategoryAds.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body() != null && response.body().isSuccess()) {
                    arrSpCategories.clear();
                    arrSpCategories.addAll(response.body().getResult());
                    adapterCategory.notifyDataSetChanged();
                    if (isFromUpdate) {
                        preFilledData();
                    }
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }


        });
    }

    public void callAttributes(int categoryID,
                               int sub1Category) {
        serviceCallAttributeAds = WebServiceFactory.getInstance().getAttributes(
                categoryID,
                sub1Category,
                prefHelper.getLang()
        );
        serviceCallAttributeAds.enqueue(new Callback<WebResponse<ArrayList<Attributes>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Attributes>>> call,
                                   Response<WebResponse<ArrayList<Attributes>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrAttributes.clear();
                    arrAttributes.addAll(response.body().getResult());
                    for (int i = 0; i < arrAttributes.size(); i++) {
                        if (arrAttributes.get(i).getAttributeType().equals("dropdown")) {
                            arrAttributes.get(i).getDropdownlist().add(0, dropdownlistSelectItem);
                        }
                    }
                    adapterAttributes.notifyDataSetChanged();
                    scrollToTop();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Attributes>>> call, Throwable t) {
                t.printStackTrace();
            }

        });

    }

    public void serviceCallSubCategory(int categoryID) {
        serviceCallSubCategoryAds = WebServiceFactory.getInstance().getSubCategory(categoryID, prefHelper.getLang());
        serviceCallSubCategoryAds.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrSpSubCategories.clear();
                    arrSpSubCategories.addAll(response.body().getResult());
                    adapterSubCategory.notifyDataSetChanged();
                    classifiedCategory();
                    spSubCategory.setSelection(2, true);
                    spSubCategory.setSelection(0, true);

                    if (isFromUpdate) {
                        for (int i = 0; i < adapterSubCategory.getList().size(); i++) {
                            if (adsEntity.getSubCategoryId() == adapterSubCategory.getList().get(i).getId()) {
                                spSubCategory.setSelection(i, true);
                            }
                        }
                        adsEntity.getSubCategoryId();
                    }

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    public void scrollToTop() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                scrollView.scrollTo(0, 0);
            }
        }, 0);
    }

    @Override
    public void onPause() {
        if (serviceCallAttributeAds != null) {
            serviceCallAttributeAds.cancel();
        }
        if (serviceCallCategoryAds != null) {
            serviceCallCategoryAds.cancel();
        }
        if (serviceCallSubCategoryAds != null) {
            serviceCallSubCategoryAds.cancel();
        }
        if (servicePostAds != null) {
            servicePostAds.cancel();
        }
        if (serviceUpdateAds != null) {
            serviceUpdateAds.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceCallAttributeAds != null) {
            serviceCallAttributeAds.cancel();
        }
        if (serviceCallCategoryAds != null) {
            serviceCallCategoryAds.cancel();
        }
        if (serviceCallSubCategoryAds != null) {
            serviceCallSubCategoryAds.cancel();
        }
        if (servicePostAds != null) {
            servicePostAds.cancel();
        }
        if (serviceUpdateAds != null) {
            serviceUpdateAds.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLocation.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLocation.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getMainActivity(), "AIzaSyBzTbQtJLcISc46HpYe-1g6Kwrde8j0ZjM");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getMainActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(Location location) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getMainActivity(), Locale.getDefault());

//        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        txtLocation.setText(address);

        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }


}


