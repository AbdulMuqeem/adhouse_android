package com.wamztech.adhouse.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SettingsFragment extends BaseFragment {


    @BindView(R.id.txtLangauge)
    AnyTextView txtLangauge;
    @BindView(R.id.txtChangePassword)
    AnyTextView txtChangePassword;
    @BindView(R.id.txtContactUs)
    AnyTextView txtContactUs;
    @BindView(R.id.txtAbout)
    AnyTextView txtAbout;
    @BindView(R.id.txtHelp)
    AnyTextView txtHelp;
    @BindView(R.id.txtPrivacyPolicy)
    AnyTextView txtPrivacyPolicy;
    @BindView(R.id.txtTermsAndCondition)
    AnyTextView txtTermsAndCondition;
    @BindView(R.id.btnToggle)
    ImageView btnToggle;
    private Unbinder unbinder;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (prefHelper.getLang().equals("ar")) {

            btnToggle.setImageResource(R.drawable.toggle_off);
        } else {
            btnToggle.setImageResource(R.drawable.toggle_on);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getContext().getString(R.string.settings));
        titleBar.showBackButton(getMainActivity());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.btnToggle, R.id.txtChangePassword, R.id.txtContactUs, R.id.txtAbout, R.id.txtHelp, R.id.txtPrivacyPolicy, R.id.txtTermsAndCondition})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnToggle:

                if (prefHelper.getLang().equals("en")) {
                    btnToggle.setImageResource(R.drawable.toggle_off);
                    prefHelper.putLang("ar");
                    setDefaultLocale();
                } else {
                    btnToggle.setImageResource(R.drawable.toggle_on);
                    prefHelper.putLang("en");
                    setDefaultLocale();
                }
                break;
            case R.id.txtChangePassword:
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getString(R.string.guest_signin_on_app));
                    return;
                }
                addDockableFragment(ChangePassword.newInstance());
                break;
            case R.id.txtContactUs:
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getString(R.string.guest_signin_on_app));
                    return;
                }

                addDockableFragment(ContactUsFragment.newInstance());
                break;
            case R.id.txtAbout:
                addDockableFragment(AboutFragmenrt.newInstance());
                break;
            case R.id.txtHelp:
                addDockableFragment(HelpFragment.newInstance());
                break;
            case R.id.txtPrivacyPolicy:
                addDockableFragment(PrivacyPolicyFragment.newInstance());
                break;
            case R.id.txtTermsAndCondition:
                addDockableFragment(TermsConditionFragment.newInstance());
                break;

        }
    }


}
