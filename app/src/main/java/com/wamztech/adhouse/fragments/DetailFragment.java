package com.wamztech.adhouse.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.CustomPager;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.adapters.ImageViewPagerAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.DialogFactory;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.helpers.Utils;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.AdDetail;
import com.wamztech.adhouse.models.AdImages;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.HorizontalListView;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AdsGridItemBinder;
import com.wamztech.adhouse.ui.views.viewbinders.AttributeItemBinder;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {


    public static final String KEY_OBJECT = "key-dataaa";
    @BindView(R.id.viewPagerImages)
    ViewPager viewPagerImages;
    @BindView(R.id.txtFeatured)
    AnyTextView txtFeatured;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.txtVehicleName)
    AnyTextView txtVehicleName;
    @BindView(R.id.txtAdViews)
    AnyTextView txtAdViews;
    @BindView(R.id.txtDetails)
    AnyTextView txtDetails;
    @BindView(R.id.txtQAR)
    AnyTextView txtQAR;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.viewPagerFragment)
    CustomPager viewPagerFragment;
    @BindView(R.id.lvProductDetail)
    ListView lvProductDetail;
    @BindView(R.id.txtName)
    AnyTextView txtName;
    @BindView(R.id.txtLocations)
    AnyTextView txtLocations;
    @BindView(R.id.txtPhone)
    AnyTextView txtPhone;
    @BindView(R.id.txtEmail)
    AnyTextView txtEmail;
    @BindView(R.id.llSeller)
    LinearLayout llSeller;
    @BindView(R.id.txtAddInfo)
    AnyTextView txtAddInfo;
    @BindView(R.id.txtAddInfoDetail)
    AnyTextView txtAddInfoDetail;
    @BindView(R.id.llAdditionalInfo)
    LinearLayout llAdditionalInfo;
    @BindView(R.id.btnContactSeller)
    Button btnContactSeller;
    @BindView(R.id.txtfeatureAdd)
    AnyTextView txtfeatureAdd;
    @BindView(R.id.lvHorizontal)
    HorizontalListView lvHorizontal;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    Unbinder unbinder;
    int isLike;
    @BindView(R.id.txtSimilarOwnerRelatedAds)
    AnyTextView txtSimilarOwnerRelatedAds;
    @BindView(R.id.lvSimilarOwnerRelatedAds)
    HorizontalListView lvSimilarOwnerRelatedAds;
    @BindView(R.id.linearLocation)
    LinearLayout linearLocation;
    @BindView(R.id.ivMapImage)
    ImageView ivMapImage;
    @BindView(R.id.flMapView)
    FrameLayout flMapView;

    SupportMapFragment mapFragment;


    private ImageViewPagerAdapter adapterImageViewPager;
    private GenericArrayAdapter<AdDetail> adapterProduct;
    private Ad entityAds;
    private ArrayList<Ad> arrCategoryAds;
    private TitleBar titleBar;
    private GenericArrayAdapter<Ad> adapterCategoryAd;
    private GenericArrayAdapter<Ad> adapterOwnerAds;
    private ImageView btnRight;
    private Call<WebResponse<Ad>> serviceAdDescriptionArabic;
    private Call<WebResponse<Ad>> serviceAdFavourite;
    private Call<WebResponse<ArrayList<Ad>>> serviceCategoryFeatured;
    private Call<WebResponse<ArrayList<Ad>>> serviceGetOwnerads;

    public static DetailFragment newInstance(Ad product) {

        Bundle args = new Bundle();
        args.putString(KEY_OBJECT, new Gson().toJson(product)); // Convert Product object to JSON string
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        float oneRowHeight = 0.0f;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
            oneRowHeight = view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + (int) (oneRowHeight * 2);
        listView.setLayoutParams(params);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrCategoryAds = new ArrayList<>();
        dataSet();
    }

    private void dataSet() {
        entityAds = new Gson().fromJson(getArguments().getString(KEY_OBJECT), Ad.class);
        adapterCategoryAd = new GenericArrayAdapter<>(getActivity(), arrCategoryAds, new AdsGridItemBinder());
        adapterOwnerAds = new GenericArrayAdapter<>(getActivity(), new ArrayList<Ad>(), new AdsGridItemBinder());
        adapterImageViewPager = new ImageViewPagerAdapter(getActivity(), new ArrayList<AdImages>(), this);
        adapterProduct = new GenericArrayAdapter<>(getActivity(), new ArrayList<AdDetail>(), new AttributeItemBinder());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_page, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void serviceCallAdDescriptionArabic() {

        serviceAdDescriptionArabic = WebServiceFactory.getInstance().getAdDetails(entityAds.getId(), prefHelper.getUser().getId(), prefHelper.getLang());
        serviceAdDescriptionArabic.enqueue(new Callback<WebResponse<Ad>>() {
            @Override
            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    entityAds = response.body().getResult();
                    setDateFormat();
                    setAdapters();
                    setGuestUserData();
                    setViews();
                    serviceCallCategoryAds();
                    sellerInformation();
                    setHeadingData();
                    setMapData();
                    if (entityAds.getUserDetails().getAddress().equals("")) {
                        linearLocation.setVisibility(View.GONE);
                    } else {
                        linearLocation.setVisibility(View.VISIBLE);
                    }
                    if (entityAds.isFavourite()) {
                        isLike = 1;
                        btnRight.setImageResource(R.drawable.heartfilled);
                    } else {
                        isLike = 0;
                        btnRight.setImageResource(R.drawable.heart);
                    }

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                t.printStackTrace();
            }


        });
    }

    private void setMapData() {


//        String lat = entityAds.getLatitude();
//        String lng = entityAds.getLongitude();
//        //String MAP_API_KEY = "AIzaSyBdauqPhRaDmT46b6O7wrAcCONJQlyv1OQ";
//        String MAP_API_KEY = getString(R.string.API_KEY);
//        ivMapImage.setTag(0);
//
//
//        String mapURI = "https://maps.googleapis.com/maps/api/staticmap?center="+lat+","+lng+"&zoom=10&size=500x200&maptype=roadmap&markers=color:red%7Clabel:C%7C"+lat+","+lng+"&key="+MAP_API_KEY;
//
//        ImageLoader.getInstance().displayImage(mapURI, ivMapImage, null, new ImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(String imageUri, View view) {
//
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//
//            }
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                ivMapImage.setVisibility(View.VISIBLE);
//                ivMapImage.setImageBitmap(loadedImage);
//                ivMapImage.setTag(1);
//            }
//
//            @Override
//            public void onLoadingCancelled(String imageUri, View view) {
//
//            }
//        });

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);


                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(entityAds.getLatitude()), Double.parseDouble(entityAds.getLongitude())))
                        .title(entityAds.getTitle())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(entityAds.getLatitude()),
                        Double.parseDouble(entityAds.getLongitude())), 10));

                flMapView.setVisibility(View.VISIBLE);


            }
        });


    }

    private void setHeadingData() {

        if (prefHelper != null && prefHelper.getUser() != null && prefHelper.getUser().getRoleId() != null && prefHelper.getUser().getRoleId() == 1) { // indivisual
            txtSimilarOwnerRelatedAds.setText(getString(R.string.SimilarAdForSameAdvertiser));
        } else { //comercial
            if (entityAds.getIsVehicle() == 1) {
                txtSimilarOwnerRelatedAds.setText(getString(R.string.SimilarAdForSameAdvertiser));
            } else if (entityAds.getIsProperty() == 1) {
                txtSimilarOwnerRelatedAds.setText(getString(R.string.SimilarAdForSameAdvertiser));
            } else {
                txtSimilarOwnerRelatedAds.setText(getString(R.string.SimilarAdForSameAdvertiser));
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvProductDetail.setFocusable(false);
        listners();
        serviceCallAdDescriptionArabic();

    }

    private void setViews() {
        txtQAR.setText(entityAds.getCurrency_symbol() + " " + Utils.formatNumber(entityAds.getPrice()));

        if (entityAds.isFeatured()) {
            txtFeatured.setVisibility(View.VISIBLE);
        } else {
            txtFeatured.setVisibility(View.GONE);
        }

        txtVehicleName.setText(entityAds.getTitle());
        txtAdViews.setText(entityAds.getTotal_views() + "");
    }

    private void setGuestUserData() {
        if (prefHelper.getUser().isGuestUser()) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.product_information));
            tabLayout.addTab(tabLayout.newTab().setText(R.string.seller_information));
            llAdditionalInfo.setVisibility(View.VISIBLE);

//            if(ivMapImage.getTag() != null && !Utils.isEmptyOrNull(ivMapImage.getTag().toString()) && Integer.parseInt(ivMapImage.getTag().toString()) == 1)
//                ivMapImage.setVisibility(View.VISIBLE);

            flMapView.setVisibility(View.VISIBLE);


            txtAddInfoDetail.setText(entityAds.getDescription());
        } else {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.product_information));
            tabLayout.addTab(tabLayout.newTab().setText(R.string.seller_information));
            llAdditionalInfo.setVisibility(View.VISIBLE);


//            if(ivMapImage.getTag() != null && !Utils.isEmptyOrNull(ivMapImage.getTag().toString()) && Integer.parseInt(ivMapImage.getTag().toString()) == 1)
//                ivMapImage.setVisibility(View.VISIBLE);

            flMapView.setVisibility(View.VISIBLE);


            txtAddInfoDetail.setText(entityAds.getDescription());

            setDividerTabLayout();
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                int position = tab.getPosition();
                switch (position) {
                    case 0: {
                        scrollToTop();
                        llSeller.setVisibility(View.GONE);
                        lvProductDetail.setVisibility(View.VISIBLE);
                        llAdditionalInfo.setVisibility(View.VISIBLE);

//                        if(ivMapImage.getTag() != null && !Utils.isEmptyOrNull(ivMapImage.getTag().toString()) && Integer.parseInt(ivMapImage.getTag().toString()) == 1)
//                            ivMapImage.setVisibility(View.VISIBLE);
                        flMapView.setVisibility(View.VISIBLE);
                        btnContactSeller.setVisibility(View.VISIBLE);

                        txtAddInfoDetail.setText(entityAds.getDescription());

                    }
                    break;
                    case 1: {

                        llSeller.setVisibility(View.VISIBLE);
                        lvProductDetail.setVisibility(View.GONE);
                        llAdditionalInfo.setVisibility(View.GONE);
//                        ivMapImage.setVisibility(View.GONE);
                        flMapView.setVisibility(View.GONE);
                        btnContactSeller.setVisibility(View.GONE);


                    }
                    break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setAdapters() {
        viewPagerImages.setAdapter(adapterImageViewPager);

        adapterImageViewPager.clear();
        adapterImageViewPager.addAll(entityAds.getAdImages());

        indicator.setViewPager(viewPagerImages);
        indicator.setSnap(true);

        lvHorizontal.setAdapter(adapterCategoryAd);
        lvSimilarOwnerRelatedAds.setAdapter(adapterOwnerAds);
        lvProductDetail.setAdapter(adapterProduct);
        adapterProduct.clear();

        //Add Some Fields

        //Phone Added
        AdDetail phoneDetail = new AdDetail();
        phoneDetail.setAttributeTitle(getString(R.string.phone_number).toUpperCase());
        phoneDetail.setAttributeValue(entityAds.getPhone());
        adapterProduct.add(phoneDetail);

        //Location Added
        AdDetail locationDetail = new AdDetail();
        locationDetail.setAttributeTitle(getString(R.string.location).toUpperCase());
//        locationDetail.setAttributeValue(LocationHelper.getCompleteAddressString(getMainActivity(), Double.parseDouble(entityAds.getLatitude()), Double.parseDouble(entityAds.getLongitude())));
        locationDetail.setAttributeValue(entityAds.getArea());
        adapterProduct.add(locationDetail);


        adapterProduct.addAll(entityAds.getAdDetails());


        setListViewHeightBasedOnChildren(lvProductDetail);
    }

    private void setDateFormat() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date1 = null;
        try {
            date1 = df.parse(entityAds.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        df.setTimeZone(TimeZone.getDefault());

//        SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MMM-yyyy");
//        df2.setTimeZone(TimeZone.getDefault());
        txtDetails.setText(entityAds.getArea() + "\n\n" + df2.format(date1));
    }

    private String returnArea() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date1 = null;
        try {
            date1 = df.parse(entityAds.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        df.setTimeZone(TimeZone.getDefault());

//        SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MMM-yyyy");
//        df2.setTimeZone(TimeZone.getDefault());
        return entityAds.getArea();
    }

    private void setDividerTabLayout() {
        //set divider in tab layout
        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getActivity().getResources().getColor(R.color.light_gray));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(20);
        linearLayout.setDividerDrawable(drawable);
    }

    public void sellerInformation() {
        txtEmail.setText(entityAds.getUserDetails().getEmail());
        txtPhone.setText(entityAds.getUserDetails().getPhone());
        txtLocations.setText(entityAds.getUserDetails().getAddress());
        txtName.setText(entityAds.getUserDetails().getFullName());
    }

    private void listners() {
        txtLocations.setOnClickListener(this);
        txtPhone.setOnClickListener(this);
        txtEmail.setOnClickListener(this);
        btnContactSeller.setOnClickListener(this);
        lvHorizontal.setOnItemClickListener(this);
        lvSimilarOwnerRelatedAds.setOnItemClickListener(this);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        btnRight = titleBar.getBtnRight();
        titleBar.resetTitleBar();
        titleBar.setHeading(entityAds.getTitle());
        titleBar.showBackButton(getMainActivity());

        if (entityAds.isFavourite()) {
            isLike = 1;
            btnRight.setImageResource(R.drawable.heartfilled);
            titleBar.setRightButton(R.drawable.heartfilled, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (prefHelper.getUser().isGuestUser()) {
                        UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.guest_user));
                        return;
                    }

                    if (entityAds.isFavourite()) {
                        isLike = 0;
                        serviceAdFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), entityAds.getId(), isLike);
                        serviceAdFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                                btnRight.setEnabled(true);
                                if (response == null || response.body() == null || response.body().getResult() == null) {
                                    return;
                                }
                                if (response.body().isSuccess()) {
                                    btnRight.setImageResource(R.drawable.listheart);
                                    isLike = 0;
                                    entityAds.setIsLike(isLike);

                                    for (int i = 0; i < adapterOwnerAds.getList().size(); i++) {
                                        if (adapterOwnerAds.getList().get(i).getId() == entityAds.getId()) {
                                            adapterOwnerAds.getList().get(i).setIsLike(0);
                                            adapterOwnerAds.notifyDataSetChanged();
                                        }
                                    }
                                    for (int i = 0; i < adapterCategoryAd.getList().size(); i++) {
                                        if (adapterCategoryAd.getList().get(i).getId() == entityAds.getId()) {
                                            adapterCategoryAd.getList().get(i).setIsLike(1);
                                            adapterCategoryAd.notifyDataSetChanged();
                                        }
                                    }
                                }
                                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                                if (btnRight != null) {
                                    btnRight.setEnabled(true);
                                }
                                t.printStackTrace();
                            }
                        });
                    } else {
                        isLike = 1;
                        btnRight.setImageResource(R.drawable.heartfilled);
                        serviceCallFavorite();
                    }
                    isLike = 0;
                    btnRight.setEnabled(false);

                }
            });

        } else {
            isLike = 0;
            btnRight.setImageResource(R.drawable.heart);
            titleBar.setRightButton(R.drawable.listheart, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (prefHelper.getUser().isGuestUser()) {
                        UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.guest_user));

                        return;
                    }

                    if (entityAds.isFavourite()) {
                        isLike = 0;
                        serviceAdFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), entityAds.getId(), isLike);
                        serviceAdFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                                btnRight.setEnabled(true);
                                if (response == null || response.body() == null || response.body().getResult() == null) {
                                    return;
                                }
                                if (response.body().isSuccess()) {
                                    btnRight.setImageResource(R.drawable.listheart);
                                    isLike = 0;
                                    entityAds.setIsLike(isLike);
                                }
                                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                                if (btnRight != null) {
                                    btnRight.setEnabled(true);
                                }
                                t.printStackTrace();
                            }
                        });
                    } else {
                        isLike = 1;
                        serviceAdFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), entityAds.getId(), isLike);
                        serviceAdFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                                btnRight.setEnabled(true);
                                if (response == null || response.body() == null || response.body().getResult() == null) {
                                    return;
                                }
                                if (response.body().isSuccess()) {
                                    btnRight.setImageResource(R.drawable.heartfilled);
                                    isLike = 1;
                                    entityAds.setIsLike(isLike);
                                }
                                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                                if (btnRight != null) {
                                    btnRight.setEnabled(true);
                                }
                                t.printStackTrace();
                            }
                        });
                    }
                    isLike = 1;
                    btnRight.setEnabled(false);
                }
            });

        }
    }

    private void serviceCallFavorite() {
        serviceAdFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), entityAds.getId(), isLike);
        serviceAdFavourite.enqueue(new Callback<WebResponse<Ad>>() {
            @Override
            public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                btnRight.setEnabled(true);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    btnRight.setImageResource(R.drawable.heartfilled);
                    isLike = 1;
                    entityAds.setIsLike(isLike);
                    for (int i = 0; i < adapterOwnerAds.getList().size(); i++) {
                        if (adapterOwnerAds.getList().get(i).getId() == entityAds.getId()) {
                            adapterOwnerAds.getList().get(i).setIsLike(1);
                            adapterOwnerAds.notifyDataSetChanged();
                        }
                    }
                    for (int i = 0; i < adapterCategoryAd.getList().size(); i++) {
                        if (adapterCategoryAd.getList().get(i).getId() == entityAds.getId()) {
                            adapterCategoryAd.getList().get(i).setIsLike(1);
                            adapterCategoryAd.notifyDataSetChanged();
                        }
                    }
                }
                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
            }

            @Override
            public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                if (btnRight != null) {
                    btnRight.setEnabled(true);
                }
                t.printStackTrace();
            }
        });
    }

    public void scrollToTop() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                scrollView.scrollTo(0, 0);
            }
        }, 0);
    }

    protected void intentOpenMap(String label) {
        String uriString = "https://maps.google.com/?q=" + label;
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        if (intent.resolveActivity(getMainActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    protected void intentOpenDialUp(String number) {

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txtLocations: {
                String label;
                if (entityAds.getUserDetails().getLatitude().equals("") || entityAds.getUserDetails().getLongitude().equals("")) {
                    label = entityAds.getUserDetails().getAddress();
                } else {
                    label = entityAds.getUserDetails().getLatitude() + "," + entityAds.getUserDetails().getLongitude();
                }
                intentOpenMap(label);
            }
            break;
            case R.id.txtPhone: {
                openOptionsDialog();
            }
            break;

            case R.id.txtEmail: {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", entityAds.getUserDetails().getEmail(), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Adhouse");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey I saw your ad on AdHosue");
                    getMainActivity().startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
            break;

            case R.id.btnContactSeller: {
                if (prefHelper.getUser().getId().equals(entityAds.getUserDetails().getId())) {
                    UtilHelper.showToast(getMainActivity(), getString(R.string.you_cannot_contact_your_own_ad));
                    drawerclose();
                    return;
                }
                if (prefHelper.getUser().isGuestUser()) {
                    // Select Tab
                    TabLayout.Tab tab = tabLayout.getTabAt(1);
                    tab.select();
                    drawerclose();
                    return;
                }

                addDockableFragment(ContactUsFragment.newInstance());
            }
            break;

            case R.id.imgPager: {
                addDockableFragment(ImageViewFragment.newInstance(viewPagerImages.getCurrentItem(), adapterImageViewPager.getList()));
            }
            break;
        }
    }

    private void openOptionsDialog() {

        final ArrayList<String> options = new ArrayList<>();
        options.add("Phone (call)");
        options.add("Phone (sms)");
        options.add("WhatsApp");
        DialogFactory.listDialog(getMainActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0)
                    openPhoneCall();
                else if (position == 1)
                    openPhoneMessanger();
                else
                    openWhatsAppMessanger();

                dialog.dismiss();
            }
        }, getString(R.string.select), options);

    }

    private void openPhoneCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + txtPhone.getText().toString()));
        getMainActivity().startActivity(intent);

    }


    private void openPhoneMessanger() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + txtPhone.getText().toString()));
        sendIntent.putExtra("address", txtPhone.getText().toString());
        sendIntent.putExtra("sms_body", "Hey I saw your ad on AdHosue");
        getMainActivity().startActivity(sendIntent);
    }

    private void openWhatsAppMessanger() {
        try {
//            PackageManager pm = getMainActivity().getPackageManager();

//            Intent waIntent = new Intent(Intent.ACTION_SEND);
//            waIntent.setType("text/plain");
//            String text = "Hey I saw your ad on AdHosue";
//
//            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
//            //Check if package exists or not. If not then code
//            //in catch block will be called
//            waIntent.setPackage("com.whatsapp");
//
//            waIntent.putExtra(Intent.EXTRA_TEXT, text);
//            startActivity(Intent.createChooser(waIntent, "Share with"));

            String phoneNumber = txtPhone.getText().toString();
            phoneNumber = phoneNumber.replaceFirst("^0+(?!$)", "").trim();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("whatsapp://send?phone=+" + phoneNumber + "&text=Hey I saw your ad on AdHouse"));
            startActivity(browserIntent);

        } catch (Exception e) {
            Toast.makeText(getMainActivity(), "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }

    }

    public void serviceCallCategoryAds() {
        serviceCategoryFeatured = WebServiceFactory.getInstance().getCategoryFeatured(entityAds.getCategoryId(), prefHelper.getUser().getId());
        serviceCategoryFeatured.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapterCategoryAd.clear();
                    adapterCategoryAd.addAll(response.body().getResult());
                    adapterCategoryAd.notifyDataSetChanged();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }

        });


        serviceGetOwnerads = WebServiceFactory.getInstance().getOwnerads(entityAds.getUserId(), prefHelper.getUser().getId());
        serviceGetOwnerads.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapterOwnerAds.clear();
                    adapterOwnerAds.addAll(response.body().getResult());
                    adapterOwnerAds.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }

        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.lvHorizontal:
                addDockableFragment(DetailFragment.newInstance(((Ad) adapterCategoryAd.getItem(position))));
                break;
            case R.id.lvSimilarOwnerRelatedAds:
                addDockableFragment(DetailFragment.newInstance(((Ad) adapterOwnerAds.getItem(position))));
                break;
        }
    }


    @Override
    public void onDestroy() {

        try {
            if (mapFragment != null) {
                getFragmentManager().beginTransaction().remove(mapFragment).commit();
                mapFragment.onDestroy();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


    @Override
    public void onPause() {

        if (serviceAdDescriptionArabic != null) {
            serviceAdDescriptionArabic.cancel();
        }
        if (serviceAdFavourite != null) {
            serviceAdFavourite.cancel();
        }
        if (serviceGetOwnerads != null) {
            serviceGetOwnerads.cancel();
        }
        if (serviceCategoryFeatured != null) {
            serviceCategoryFeatured.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {


        try {
            if (mapFragment != null) {
                getFragmentManager().beginTransaction().remove(mapFragment).commit();
                mapFragment.onDestroyView();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (serviceAdDescriptionArabic != null) {
            serviceAdDescriptionArabic.cancel();
        }
        if (serviceAdFavourite != null) {
            serviceAdFavourite.cancel();
        }
        if (serviceGetOwnerads != null) {
            serviceGetOwnerads.cancel();
        }
        if (serviceCategoryFeatured != null) {
            serviceCategoryFeatured.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }


}

