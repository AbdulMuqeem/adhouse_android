package com.wamztech.adhouse.fragments;
//

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.wamztech.adhouse.Constants;
import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.CameraGalleryActionDialog;
import com.wamztech.adhouse.helpers.CurrentLocationHelper;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.models.City;
import com.wamztech.adhouse.models.Currency;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.SpinnerPlus;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.SpCityBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SpCurrencyBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SpGenderBinder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wamztech.adhouse.R.id.txtChangePassword;
import static com.nostra13.universalimageloader.core.ImageLoader.TAG;


public class MyProfileFragment extends BaseFragment implements View.OnClickListener, OnActivityResult {
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5656;

    private static String USERID;
    @BindView(R.id.imgHolder)
    ImageView imgHolder;
    @BindView(R.id.imgPictureChoose)
    ImageView imgPictureChoose;
    @BindView(R.id.edName)
    AnyEditTextView edName;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.spGender)
    SpinnerPlus spGender;
    @BindView(R.id.spCity)
    SpinnerPlus spCity;
    @BindView(R.id.spCountry)
    SpinnerPlus spCountry;
    @BindView(R.id.spCurrency)
    SpinnerPlus spCurrency;
    @BindView(R.id.txtAddress)
    AnyTextView txtAddress;
    @BindView(R.id.edPhone)
    AnyEditTextView edPhone;
    @BindView(R.id.btnSubmit)
    AnyButton btnSubmit;
    Unbinder unbinder;

    @BindView(R.id.txtSelectGender)
    AnyTextView txtSelectGender;
    @BindView(R.id.txtSelectCity)
    AnyTextView txtSelectCity;
    @BindView(R.id.txtSelectCountry)
    AnyTextView txtSelectCountry;
    @BindView(R.id.txtSelectCurrency)
    AnyTextView txtSelectCurrency;
    @BindView(R.id.currLocation)
    ImageView currLocation;
    boolean isGenderSet = false;
    boolean isCitySet = false;
    boolean isCountrySet = false;
    boolean isCurrencySet = false;
    String user_id;
    GenericArrayAdapter<City> cityAdapter;
    GenericArrayAdapter<City> countryAdapter;
    GenericArrayAdapter<Currency> currencyAdapter;
    GenericArrayAdapter<String> genderAdapter;
    File file;
    double latitude;
    double longitude;
    private ArrayList<City> arrCities;
    private ArrayList<City> arrCountry;
    private ArrayList<Currency> arrCurrency;
    private String cityName;
    private String countryName;
    private String currencyName;
    private String gender;
    private Call<WebResponse<User>> serviceUpdateProfile;
    private Call<WebResponse<ArrayList<City>>> serviceGetCity;
    private Call<WebResponse<ArrayList<City>>> serviceGetCountry;
    private Call<WebResponse<ArrayList<Currency>>> serviceGetCurrency;
    private String locationName;
    private FusedLocationProviderClient fusedLocationClient;

    public static MyProfileFragment newInstance(int user_id) {
        Bundle args = new Bundle();
        args.putString(USERID, String.valueOf(user_id));
        MyProfileFragment fragment = new MyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getMainActivity());

        if (getArguments() != null) {
            user_id = getArguments().getString(USERID);
        } else {
            user_id = prefHelper.getUser().getId().toString();
        }

        arrCountry = new ArrayList<>();
        arrCities = new ArrayList<>();
        arrCurrency = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCallCurrency();
        serviceCallCountry();
        serviceCallCity();
        edEmail.setEnabled(false);

        //For Country
        countryAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCountry, new SpCityBinder());
        spCountry.setAdapter(countryAdapter);


        //For City
        cityAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCities, new SpCityBinder());
        spCity.setAdapter(cityAdapter);

        //For Currency
        currencyAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCurrency, new SpCurrencyBinder());
        spCurrency.setAdapter(currencyAdapter);

        genderAdapter = new GenericArrayAdapter<>(getMainActivity(), Constants.getGender(), new SpGenderBinder());
        spGender.setAdapter(genderAdapter);
        listeners();
        showProfileData();
        setSpCountry();
        setSpCity();
        setSpCurrency();
        setSpGender();
        getMainActivity().setOnActivityResult(MyProfileFragment.this);
    }

    private void showProfileData() {
        edName.setText(prefHelper.getUser().getFullName());
        edEmail.setText(prefHelper.getUser().getEmail());

        if (prefHelper.getUser().getGender().equals("")) {
            txtSelectGender.setText(getResources().getString(R.string.gender));
        } else {
            txtSelectGender.setText(prefHelper.getUser().getGender());
        }


        txtSelectCountry.setText(getCountryNameByID(prefHelper.getUser().getCountryId()));
        txtSelectCountry.setTag(prefHelper.getUser().getCountryId());


        txtSelectCurrency.setText(getCurrencyNameByID(prefHelper.getUser().getCurrencyId()));
        txtSelectCurrency.setTag(prefHelper.getUser().getCurrencyId());

        txtSelectCity.setText(prefHelper.getUser().getCity());

        txtAddress.setText(prefHelper.getUser().getAddress());
        edPhone.setText(prefHelper.getUser().getPhone());
        ImageLoader.getInstance().displayImage(prefHelper.getUser().getProfileImage(), imgHolder);
    }

    private void listeners() {
        btnSubmit.setOnClickListener(this);
        imgPictureChoose.setOnClickListener(this);
        txtSelectCurrency.setOnClickListener(this);
        txtSelectCountry.setOnClickListener(this);
        txtSelectCity.setOnClickListener(this);
        txtSelectGender.setOnClickListener(this);
        txtAddress.setOnClickListener(this);
        currLocation.setOnClickListener(this);
    }

    private boolean isValidatePhone(String phone) {
        if (phone.length() >= 8 && phone.length() <= 50) {
            return true;
        }
        return false;
    }

    public void serviceCallUpdateProfile() {
        MultipartBody.Part body = null;
        if (file != null)
            body = MultipartBody.Part.createFormData("profile_picture", file.getName(), RequestBody.create(MediaType.parse("file:///"), file));

        final String fullName = edName.getText().toString();
        String selectGender = txtSelectGender.getText().toString();
        final String selectCurrency = txtSelectCurrency.getTag().toString();
        final String selectCountry = txtSelectCountry.getTag().toString();
        final String selectCity = txtSelectCity.getText().toString();
        final String address = txtAddress.getText().toString();
        final String phone = edPhone.getText().toString();

        if (!isValidatePhone(phone)) {
            edPhone.setError(getResources().getString(R.string.invalid_phone_no));
            return;
        }

        currencyName = txtSelectCurrency.getStringTrimmed();
        countryName = txtSelectCountry.getStringTrimmed();
        cityName = txtSelectCity.getStringTrimmed();
        gender = txtSelectGender.getStringTrimmed();
        btnSubmit.setEnabled(false);
        serviceUpdateProfile = WebServiceFactory.getInstance().updateProfile(
                createPartFromString(prefHelper.getUser().getId() + ""),
                createPartFromString(fullName),
                createPartFromString(phone),
                createPartFromString(address),
                createPartFromString(selectCurrency),
                createPartFromString(selectCountry),
                createPartFromString(selectCity),
                createPartFromString(selectGender),
                body);
        serviceUpdateProfile.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                btnSubmit.setEnabled(true);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    prefHelper.putUser(response.body().getResult());
                    prefHelper.getUser().getId();
                    edName.setText(fullName);
                    edPhone.setText(phone);
                    txtAddress.setText(address);
                    txtSelectGender.setText(gender);
                    txtSelectCountry.setText(getCountryNameByID(Integer.parseInt(selectCountry)));
                    txtSelectCountry.setTag(selectCountry);


                    txtSelectCurrency.setText(getCurrencyNameByID(Integer.parseInt(selectCurrency)));
                    txtSelectCurrency.setTag(selectCurrency);

                    txtSelectCity.setText(cityName);
                    ImageLoader.getInstance().displayImage(prefHelper.getUser().getProfileImage(), imgHolder);
                    popBackStack();
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                    SideMenuFragment fragment = (SideMenuFragment) getFragmentManager().findFragmentByTag("SideMenuFragment");
                    if (fragment != null) {
                        fragment.bindData();
                    }


                } else {
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                if (btnSubmit != null) {
                    btnSubmit.setEnabled(true);
                }
                t.printStackTrace();
            }
        });
    }

    private String getCountryNameByID(Integer countryID) {

        if (countryID != null && arrCountry.size() > 0) {
            for (int i = 0; i < arrCountry.size(); i++) {
                if (arrCountry.get(i).getId() == countryID)
                    return arrCountry.get(i).getTitle();
            }
        }

        return "";

    }


    private String getCurrencyNameByID(Integer currencyID) {

        if (currencyID != null && arrCurrency.size() > 0) {
            for (int i = 0; i < arrCurrency.size(); i++) {
                if (arrCurrency.get(i).getId() == currencyID)
                    return arrCurrency.get(i).getName() + " - " + arrCurrency.get(i).getSymbol();
            }
        }

        return "";

    }

    public void serviceCallCurrency() {


        serviceGetCurrency = WebServiceFactory.getInstance().getCurrency(-1);
        serviceGetCurrency.enqueue(new Callback<WebResponse<ArrayList<Currency>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Currency>>> call, Response<WebResponse<ArrayList<Currency>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    arrCurrency.clear();
                    arrCurrency.addAll(response.body().getResult());
                    currencyAdapter.notifyDataSetChanged();


                    txtSelectCurrency.setText(getCurrencyNameByID(prefHelper.getUser().getCurrencyId()));
                    txtSelectCurrency.setTag(prefHelper.getUser().getCurrencyId());

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Currency>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    public void serviceCallCountry() {


        serviceGetCountry = WebServiceFactory.getInstance().getCountry(prefHelper.getLang());
        serviceGetCountry.enqueue(new Callback<WebResponse<ArrayList<City>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<City>>> call, Response<WebResponse<ArrayList<City>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    arrCountry.clear();
                    arrCountry.addAll(response.body().getResult());
                    countryAdapter.notifyDataSetChanged();


                    txtSelectCountry.setText(getCountryNameByID(prefHelper.getUser().getCountryId()));
                    txtSelectCountry.setTag(prefHelper.getUser().getCountryId());

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<City>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void serviceCallCity() {


        serviceGetCity = WebServiceFactory.getInstance().getCity(prefHelper.getLang());
        serviceGetCity.enqueue(new Callback<WebResponse<ArrayList<City>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<City>>> call, Response<WebResponse<ArrayList<City>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    arrCities.clear();
                    arrCities.addAll(response.body().getResult());
                    cityAdapter.notifyDataSetChanged();


                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<City>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void setSpGender() {
        spGender.setVisibility(View.INVISIBLE);

        spGender.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtSelectGender.setText(getResources().getString(R.string.gender));
                gender = Constants.getGender().get(position);

                if (isGenderSet) {
                    spGender.setVisibility(View.INVISIBLE);
                    txtSelectGender.setText(gender);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAddress:
                callPlaceAutocompleteActivityIntent();
                break;
            case R.id.btnSubmit:
                //todo
                serviceCallUpdateProfile();
                break;

            case R.id.imgPictureChoose:
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
                } else {
                    showCameraDialog();
                }
                break;


            case R.id.txtSelectCity: {
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
                } else {
                    isCitySet = true;

                    spCity.performClick();
                }
            }
            break;

            case R.id.txtSelectCountry: {
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
                } else {
                    isCountrySet = true;

                    spCountry.performClick();
                }
            }
            break;

            case R.id.txtSelectCurrency: {
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
                } else {
                    isCurrencySet = true;

                    spCurrency.performClick();
                }
            }
            break;
            case R.id.txtSelectGender: {
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.sign_in_req));
                } else {
                    isGenderSet = true;
                    spGender.performClick();
                }
                break;
            }
            case R.id.currLocation:
                callForCurrentAddress();


                break;
        }
    }

    private void setSpCurrency() {

        spCurrency.setVisibility(View.INVISIBLE);

        spCurrency.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                currencyName = arrCurrency.get(position).getName() + " - " + arrCurrency.get(position).getSymbol();

                if (isCurrencySet) {
                    spCurrency.setVisibility(View.INVISIBLE);
                    txtSelectCurrency.setText(currencyName);
                    txtSelectCurrency.setTag(arrCurrency.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    private void setSpCountry() {

        spCountry.setVisibility(View.INVISIBLE);

        spCountry.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                countryName = arrCountry.get(position).getTitle();

                if (isCountrySet) {
                    spCountry.setVisibility(View.INVISIBLE);
                    txtSelectCountry.setText(countryName);
                    txtSelectCountry.setTag(arrCountry.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    private void setSpCity() {

        spCity.setVisibility(View.INVISIBLE);

        spCity.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityName = arrCities.get(position).getTitle();

                if (isCitySet) {
                    spCity.setVisibility(View.INVISIBLE);
                    txtSelectCity.setText(cityName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.showLeftButton();
        titleBar.setHeading(getString(R.string.My_Profile));
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
    }

    @Override
    public void onPause() {

        if (serviceGetCurrency != null) {
            serviceGetCurrency.cancel();
        }

        if (serviceGetCountry != null) {
            serviceGetCountry.cancel();
        }
        if (serviceGetCity != null) {
            serviceGetCity.cancel();
        }
        if (serviceUpdateProfile != null) {
            serviceUpdateProfile.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {

        if (serviceGetCurrency != null) {
            serviceGetCurrency.cancel();
        }

        if (serviceGetCountry != null) {
            serviceGetCountry.cancel();
        }
        if (serviceGetCity != null) {
            serviceGetCity.cancel();
        }
        if (serviceUpdateProfile != null) {
            serviceUpdateProfile.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showCameraDialog() {

        final CameraGalleryActionDialog dialog = new CameraGalleryActionDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Dialog);
        dialog.setOnCamGalleryActionListener(new CameraGalleryActionDialog.CamGalleryActionListener() {

            @Override
            public void setCameraOpen(File filePath) {

                if (filePath == null) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                if (filePath.isFile()) {

                    String path_prefix = "file:///";
                    String complete_image_path = path_prefix + filePath.getAbsolutePath();

                    ImageLoader.getInstance().displayImage(complete_image_path, imgHolder);
                    file = filePath;

                } else {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                }
            }


            @Override
            public void closeClick() {
                dialog.dismiss();
            }

            @Override
            public void browsePic(String strFilePath, File imgFile) {

                if (strFilePath == null || strFilePath.trim().length() <= 0) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                String path_prefix = "file:///";
                String complete_image_path = path_prefix + strFilePath;

                ImageLoader.getInstance().displayImage(complete_image_path, imgHolder);
                file = imgFile;
            }


        });

        dialog.show(getFragmentManager(), "CameraGalleryActionDialog");
    }
//
//    @Override
//    public void onResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = PlaceAutocomplete.getPlace(getMainActivity(), data);
//                latitude = place.getLatLng().latitude;
//                longitude = place.getLatLng().longitude;
//                txtAddress.setText(place.getName().toString());
//                Log.i(TAG, "Place:" + place.toString());
//            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                Status status = PlaceAutocomplete.getStatus(getMainActivity(), data);
//                Log.i(TAG, status.getStatusMessage());
//            } else if (requestCode == RESULT_CANCELED) {
//
//            }
//        }


    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtAddress.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtAddress.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getMainActivity(), "AIzaSyBzTbQtJLcISc46HpYe-1g6Kwrde8j0ZjM");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getMainActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(Location location) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getMainActivity(), Locale.getDefault());

        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        txtAddress.setText(address);

        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    private void callForCurrentAddress() {
        fusedLocationClient.getLastLocation().addOnCompleteListener(
                new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location != null) {
                            try {
                                testCurr(location);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );

    }
}

