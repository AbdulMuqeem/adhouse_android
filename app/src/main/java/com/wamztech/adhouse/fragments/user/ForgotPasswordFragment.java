package com.wamztech.adhouse.fragments.user;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.btnSubmit)
    AnyButton btnSubmit;
    Unbinder unbinder;
    @BindView(R.id.progressUploading)
    ProgressBar progressUploading;
    private Call<WebResponse<User>> serviceForgotPassword;

    public static ForgotPasswordFragment newInstance() {
        Bundle args = new Bundle();
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (edEmail.getText().toString().equals("")) {
                    UtilHelper.showToast(getActivity(), getResources().getString(R.string.field_cannot_be_empty));

                } else {
                    serviceCallForgetPassword();


                }
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.showLeftButton();
        titleBar.setHeading(getContext().getString(R.string.forgot_password));
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
    }

    @Override
    public void onPause() {
        if (serviceForgotPassword != null) {
            serviceForgotPassword.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceForgotPassword != null) {
            serviceForgotPassword.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    public void serviceCallForgetPassword() {
        progressUploading.setVisibility(View.VISIBLE);
        String email = edEmail.getText().toString();

        serviceForgotPassword = WebServiceFactory.getInstance().forgotPassword(email);
        serviceForgotPassword.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                progressUploading.setVisibility(View.GONE);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    popBackStack();
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                } else {
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                if (progressUploading != null)
                    progressUploading.setVisibility(View.GONE);
                t.printStackTrace();
                UtilHelper.showToast(getMainActivity(), t.getMessage());
            }
        });
    }
}
