package com.wamztech.adhouse.fragments.user;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.google.firebase.iid.FirebaseInstanceId;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.constants.APIConstants;
import com.wamztech.adhouse.fragments.HomeFragment;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.RunTimePermissions;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.edPassword)
    AnyEditTextView edPassword;
    @BindView(R.id.btnSignIn)
    AnyButton btnSignIn;
    @BindView(R.id.txtForgotPwd)
    AnyTextView txtForgotPwd;
    @BindView(R.id.txtGuestSignIn)
    com.ctrlplusz.anytextview.AnyTextView txtGuestSignIn;
    @BindView(R.id.txtRegisterNow)
    AnyTextView txtRegisterNow;
    Unbinder unbinder;

    Boolean isClick = true;
    @BindView(R.id.progressUploading)
    ProgressBar progressUploading;
    private Call<WebResponse> serviceUpdateDeviceToken;
    private Call<WebResponse<User>> serviceLogin;

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (prefHelper.getLang() == "") {
            prefHelper.putLang("en");
        }
        setDefaultLocale();
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spanable();
        listners();
    }

    @Override
    public void onResume() {
        super.onResume();

        getMainActivity().drawerLock();
    }

    private void spanable() {
        int color = 0xFF931919;
        SpannableString forgot = new SpannableString(getResources().getString(R.string.forgot_password));
        forgot.setSpan(new UnderlineSpan(), 0, forgot.length(), 0);
        txtForgotPwd.setText(forgot);


        SpannableString signin = new SpannableString(getResources().getString(R.string.sign_in_as_guest_user));
        ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        signin.setSpan(fcs, 0, 7, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        txtGuestSignIn.setText(signin);


        SpannableString registered = new SpannableString(getResources().getString(R.string.sign_up_new_account));
        registered.setSpan(new ForegroundColorSpan(color), 23, registered.length() - 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtRegisterNow.setText(registered);
    }

    private void listners() {
        btnSignIn.setOnClickListener(this);
        txtForgotPwd.setOnClickListener(this);
        txtRegisterNow.setOnClickListener(this);
        txtGuestSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignIn: {

                String email = edEmail.getText().toString();
                String pass = edPassword.getText().toString();

                if (email.equals("")) {
                    edEmail.setError(getResources().getString(R.string.enter_email));
                } else if (pass.equals("")) {
                    edPassword.setError(getResources().getString(R.string.enter_password));
                } else if (!isValidEmail(email)) {
                    edEmail.setError(getResources().getString(R.string.invalid_email));
                } else {
                    if (!RunTimePermissions.isAllPhonePermissionGiven(getContext(), getMainActivity(),
                            true, getResources().getText(R.string.permission_message).toString())) {
                        return;
                    }
                    serviceCallLogin();
                }
            }
            break;
            case R.id.txtForgotPwd: {
                addDockableFragment(ForgotPasswordFragment.newInstance());
            }
            break;
            case R.id.txtRegisterNow: {
                if (!RunTimePermissions.isAllPhonePermissionGiven(getContext(), getMainActivity(),
                        true, getResources().getText(R.string.permission_message).toString())) {
                    return;
                }
                addDockableFragment(SignupFragment.newInstance());
            }
            break;


            case R.id.txtGuestSignIn: {
                emptyBackStack();
                User user = new User();
                user.setId(-1);
                user.setCurrency_symbol("QAR");
                prefHelper.putUser(user);
                addDockableFragment(HomeFragment.newInstance());
                getMainActivity().initDrawer();
            }
            break;
        }

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }
        return false;
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.hideTitleBar();

    }

    @Override
    public void onPause() {
        if (serviceLogin != null) {
            serviceLogin.cancel();
        }
        if (serviceUpdateDeviceToken != null) {
            serviceUpdateDeviceToken.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceLogin != null) {
            serviceLogin.cancel();
        }
        if (serviceUpdateDeviceToken != null) {
            serviceUpdateDeviceToken.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    public void serviceCallLogin() {
        progressUploading.setVisibility(View.VISIBLE);
        String email = edEmail.getText().toString();
        String pass = edPassword.getText().toString();
        btnSignIn.setEnabled(false);
        serviceLogin = WebServiceFactory.getInstance().login(email, pass);
        serviceLogin.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                btnSignIn.setEnabled(true);
                progressUploading.setVisibility(View.GONE);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    emptyBackStack();
                    prefHelper.putUser(response.body().getResult());
                    addDockableFragment(HomeFragment.newInstance());
                    getMainActivity().initDrawer();
                    UpdateDeviceToken();

                } else {

                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }
            }


            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                if (btnSignIn != null) {
                    btnSignIn.setEnabled(true);
                }
                if (progressUploading != null) {
                    progressUploading.setVisibility(View.GONE);
                }
                UtilHelper.showToast(getMainActivity(), t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void UpdateDeviceToken() {
        serviceUpdateDeviceToken = WebServiceFactory.getInstance().updateDeviceToken
                (
                        prefHelper.getUser().getId(),
                        FirebaseInstanceId.getInstance().getToken(),
                        APIConstants.device_type

                );
        serviceUpdateDeviceToken.enqueue(new Callback<WebResponse>() {
            @Override
            public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                }

                UtilHelper.showToast(getMainActivity(), response.body().getMessage());
            }

            @Override
            public void onFailure(Call<WebResponse> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}






