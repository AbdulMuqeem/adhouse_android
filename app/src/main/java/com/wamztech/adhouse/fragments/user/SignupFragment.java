package com.wamztech.adhouse.fragments.user;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.CameraGalleryActionDialog;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.models.City;
import com.wamztech.adhouse.models.Currency;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.SpinnerPlus;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.SpCityBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SpCurrencyBinder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.nostra13.universalimageloader.core.ImageLoader.TAG;


public class SignupFragment extends BaseFragment implements View.OnClickListener, OnActivityResult {


    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.txtIndividual)
    AnyTextView txtIndividual;
    @BindView(R.id.txtLocation)
    AnyTextView txtLocation;
    @BindView(R.id.txtCommercial)
    AnyTextView txtCommercial;
    @BindView(R.id.edName)
    AnyEditTextView edName;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.spCity)
    SpinnerPlus spCity;
    @BindView(R.id.spCountry)
    SpinnerPlus spCountry;
    @BindView(R.id.spCurrency)
    SpinnerPlus spCurrency;
    @BindView(R.id.edPassword)
    AnyEditTextView edPassword;
    @BindView(R.id.edConfirmPassword)
    AnyEditTextView edConfirmPassword;
    @BindView(R.id.edPhone)
    AnyEditTextView edPhone;
    @BindView(R.id.btnSignUp)
    AnyButton btnSignUp;
    Unbinder unbinder;
    @BindView(R.id.imgLayout)
    LinearLayout imgLayout;
    @BindView(R.id.txtSelectCity)
    AnyTextView txtSelectCity;
    @BindView(R.id.txtSelectCountry)
    AnyTextView txtSelectCountry;
    @BindView(R.id.txtSelectCurrency)
    AnyTextView txtSelectCurrency;
    @BindView(R.id.currLocation)
    ImageView currLocation;
    GenericArrayAdapter<City> cityAdapter;
    GenericArrayAdapter<City> countryAdapter;
    GenericArrayAdapter<Currency> currencyAdapter;
    boolean isCitySet = false;
    boolean isCountrySet = false;
    boolean isCurrencySet = false;
    String cityName;
    String countryName;
    String currencyName;
    int role_id = 1;
    boolean roleId;
    File file;
    @BindView(R.id.progressUploading)
    ProgressBar progressUploading;
    private ArrayList<City> arrCities;
    private ArrayList<City> arrCountry;
    private ArrayList<Currency> arrCurrency;
    private String city;
    private String locationName;
    private double latitude;
    private double longitude;

    private Call<WebResponse<User>> serviceUserRegistration;
    private Call<WebResponse<ArrayList<City>>> serviceGetCity;
    private Call<WebResponse<ArrayList<City>>> serviceGetCountry;
    private Call<WebResponse<ArrayList<Currency>>> serviceGetCurrency;
    private FusedLocationProviderClient fusedLocationClient;

    public static SignupFragment newInstance() {
        Bundle args = new Bundle();
        SignupFragment fragment = new SignupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getMainActivity());

        //For City
        arrCities = new ArrayList<>();
        cityAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCities, new SpCityBinder());


        //For Country
        arrCountry = new ArrayList<>();
        countryAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCountry, new SpCityBinder());


        //For Currency
        arrCurrency = new ArrayList<>();
        currencyAdapter = new GenericArrayAdapter<>(getMainActivity(), arrCurrency, new SpCurrencyBinder());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCallCurrency();
        serviceCallCountry();
        serviceCallCity();
        setListeners();
        currLocation.performClick();
        // For City
        txtSelectCity.setText(getResources().getString(R.string.select_city));
        spCity.setAdapter(cityAdapter);
        setSpCity();


        // For Country
        txtSelectCountry.setText(getResources().getString(R.string.select_country));
        spCountry.setAdapter(countryAdapter);
        setSpCountry();


        // For Currency
        txtSelectCurrency.setText(getResources().getString(R.string.select_currency));
        spCurrency.setAdapter(currencyAdapter);
        setSpCurrency();


        getMainActivity().setOnActivityResult(SignupFragment.this);
    }

    private void setSpCity() {

        spCity.setVisibility(View.INVISIBLE);

        spCity.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityName = arrCities.get(position).getTitle();
                if (isCitySet) {
                    spCity.setVisibility(View.INVISIBLE);
                    txtSelectCity.setText(cityName);
                    txtSelectCity.setError(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void setSpCurrency() {
        spCurrency.setVisibility(View.INVISIBLE);

        spCurrency.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currencyName = arrCurrency.get(position).getName() + " - " + arrCurrency.get(position).getSymbol();
                if (isCurrencySet) {
                    spCurrency.setVisibility(View.INVISIBLE);
                    txtSelectCurrency.setText(currencyName);
                    txtSelectCurrency.setTag(arrCurrency.get(position).getId());
                    txtSelectCurrency.setError(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setSpCountry() {
        spCountry.setVisibility(View.INVISIBLE);

        spCountry.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryName = arrCountry.get(position).getTitle();
                if (isCountrySet) {
                    spCountry.setVisibility(View.INVISIBLE);
                    txtSelectCountry.setText(countryName);
                    txtSelectCountry.setTag(arrCountry.get(position).getId());
                    txtSelectCountry.setError(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setListeners() {

        txtIndividual.setOnClickListener(this);
        txtCommercial.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        imgCamera.setOnClickListener(this);
        txtSelectCurrency.setOnClickListener(this);
        txtSelectCountry.setOnClickListener(this);
        txtSelectCity.setOnClickListener(this);
        txtLocation.setOnClickListener(this);
        currLocation.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getMainActivity().drawerLock();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                String name = edName.getText().toString();
                String email = edEmail.getText().toString();
                String address = txtLocation.getText().toString();
                String pass = edPassword.getText().toString();
                String comPass = edConfirmPassword.getText().toString();
                String phone = edPhone.getText().toString();

                if (name.equals("") && email.equals("") && address.equals("") && pass.equals("") && comPass.equals("") && phone.equals("")) {
                    edName.setError(getResources().getString(R.string.enter_name));
                    edEmail.setError(getResources().getString(R.string.enter_email));
                    edPassword.setError(getResources().getString(R.string.enter_password));
                    edConfirmPassword.setError(getResources().getString(R.string.enter_password));
                    edPhone.setError(getResources().getString(R.string.enter_phone));

                } else if (!isValidEmail(email)) {
                    edEmail.setError(getResources().getString(R.string.invalid_email));
                } else if (!isValidPassword(pass)) {
                    edPassword.setError(getResources().getString(R.string.invalid_pass));
                } else if (!isValidatePhone(phone)) {
                    edPhone.setError(getResources().getString(R.string.invalid_phone_no));
                } else if (!comPass.equals(pass)) {
                    edConfirmPassword.setError(getResources().getString(R.string.pass_not_matched));
                } else if (countryName == null || countryName.equals(getString(R.string.select_country))) {
                    txtSelectCountry.setError(getString(R.string.please_select_country));
                    UtilHelper.showToast(getMainActivity(), getString(R.string.please_select_country));
                } else if (cityName == null || cityName.equals(getString(R.string.select_city))) {
                    txtSelectCity.setError(getString(R.string.please_select_city));
                    UtilHelper.showToast(getMainActivity(), getString(R.string.please_select_city));
                } else if (currencyName == null || currencyName.equals(getString(R.string.select_currency))) {
                    txtSelectCurrency.setError(getString(R.string.please_select_currency));
                    UtilHelper.showToast(getMainActivity(), getString(R.string.please_select_currency));
                } else {
                    serviceCallSignUp();
                }
                break;
            case R.id.txtIndividual:
                role_id = 1;
                txtIndividual.setTextColor(getActivity().getResources().getColor(R.color.blue));//white
                txtCommercial.setTextColor(getActivity().getResources().getColor(R.color.gary));//grey

                break;

            case R.id.txtCommercial:
                role_id = 0;
                txtCommercial.setTextColor(getActivity().getResources().getColor(R.color.blue));
                txtIndividual.setTextColor(getActivity().getResources().getColor(R.color.gary));
                break;
            case R.id.imgCamera: {

                showCameraDialog();
            }
            break;
            case R.id.txtSelectCity: {
                isCitySet = true;
                spCity.performClick();

            }
            break;

            case R.id.txtSelectCountry: {
                isCountrySet = true;
                spCountry.performClick();

            }
            break;

            case R.id.txtSelectCurrency: {
                isCurrencySet = true;
                spCurrency.performClick();

            }
            break;


            case R.id.txtLocation: {
                callPlaceAutocompleteActivityIntent();
            }
            break;
            case R.id.currLocation:
                callForCurrentAddress();


                break;
        }

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }

        return false;
    }

    private boolean isValidatePhone(String phone) {
        if (phone.length() >= 8 && phone.length() <= 50) {
            return true;
        }

        UtilHelper.showToast(getMainActivity(), getString(R.string.msg_error_min_phone_length));

        return false;
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getResources().getString(R.string.sign_up));
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
    }

    private void showCameraDialog() {


        final CameraGalleryActionDialog dialog = new CameraGalleryActionDialog();

        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Dialog);

        dialog.setOnCamGalleryActionListener(new CameraGalleryActionDialog.CamGalleryActionListener() {

            @Override
            public void setCameraOpen(File filePath) {

                if (filePath == null) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                if (filePath.isFile()) {

                    String path_prefix = "file:///";
                    String complete_image_path = path_prefix + filePath.getAbsolutePath();
                    ImageLoader.getInstance().displayImage(complete_image_path, imgCamera);
                    file = filePath;

                } else {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                }
            }


            @Override
            public void closeClick() {
                dialog.dismiss();
            }

            @Override
            public void browsePic(String strFilePath, File imgFile) {

                if (strFilePath == null || strFilePath.trim().length() <= 0) {
                    UtilHelper.showToast(getMainActivity(), "Unable to get image. Please try again..");
                    return;
                }

                String path_prefix = "file:///";
                String complete_image_path = path_prefix + strFilePath;

                ImageLoader.getInstance().displayImage(complete_image_path, imgCamera);
                file = imgFile;
            }


        });

        dialog.show(getFragmentManager(), "CameraGalleryActionDialog");
    }

    public void serviceCallSignUp() {

        String name = edName.getStringTrimmed();
        String email = edEmail.getStringTrimmed();
        String pass = edPassword.getStringTrimmed();
        String address = txtLocation.getText().toString().trim();
        String comPass = edConfirmPassword.getStringTrimmed();
        String phone = edPhone.getStringTrimmed();
        countryName = txtSelectCountry.getStringTrimmed();
        cityName = txtSelectCity.getStringTrimmed();
        currencyName = txtSelectCurrency.getStringTrimmed();
        String selectCountry = txtSelectCountry.getTag().toString();
        String selectCurrency = txtSelectCurrency.getTag().toString();

        MultipartBody.Part body = null;

        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("file:///"), file);
            body = MultipartBody.Part.createFormData("profile_picture", file.getName(), requestFile);
        }


        btnSignUp.setEnabled(false);
        progressUploading.setVisibility(View.VISIBLE);
        serviceUserRegistration = WebServiceFactory.getInstance().userRegistration
                (
                        createPartFromString(name),
                        createPartFromString(email),
                        createPartFromString(phone),
                        createPartFromString(pass),
                        createPartFromString(comPass),
                        createPartFromString(address),
                        createPartFromString(selectCurrency),
                        createPartFromString(selectCountry),
                        createPartFromString(cityName + ""),
                        createPartFromString(role_id + ""),
                        body
                );
        serviceUserRegistration.enqueue(new Callback<WebResponse<User>>() {

            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                btnSignUp.setEnabled(true);
                progressUploading.setVisibility(View.GONE);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    popBackStack();

                }

                UtilHelper.showToast(getMainActivity(), response.body().getMessage());


            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                if (btnSignUp != null)
                    btnSignUp.setEnabled(true);
                if (progressUploading != null) {
                    progressUploading.setVisibility(View.GONE);
                }
                t.printStackTrace();
                UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.no_internet_connection));
            }
        });
    }


    public void serviceCallCurrency() {

        serviceGetCurrency = WebServiceFactory.getInstance().getCurrency(-1);
        serviceGetCurrency.enqueue(new Callback<WebResponse<ArrayList<Currency>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Currency>>> call, Response<WebResponse<ArrayList<Currency>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrCurrency.clear();
                    arrCurrency.addAll(response.body().getResult());
                    currencyAdapter.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Currency>>> call, Throwable t) {
                t.printStackTrace();
            }

        });

    }


    public void serviceCallCountry() {

        serviceGetCountry = WebServiceFactory.getInstance().getCountry(prefHelper.getLang());
        serviceGetCountry.enqueue(new Callback<WebResponse<ArrayList<City>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<City>>> call, Response<WebResponse<ArrayList<City>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrCountry.clear();
                    arrCountry.addAll(response.body().getResult());
                    countryAdapter.notifyDataSetChanged();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<City>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    public void serviceCallCity() {
        serviceGetCity = WebServiceFactory.getInstance().getCity(prefHelper.getLang());
        serviceGetCity.enqueue(new Callback<WebResponse<ArrayList<City>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<City>>> call, Response<WebResponse<ArrayList<City>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrCities.clear();
                    arrCities.addAll(response.body().getResult());
                    cityAdapter.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<City>>> call, Throwable t) {
                t.printStackTrace();
            }


        });

    }

    @Override
    public void onPause() {


        if (serviceGetCurrency != null) {
            serviceGetCurrency.cancel();
        }

        if (serviceGetCountry != null) {
            serviceGetCountry.cancel();
        }

        if (serviceGetCity != null) {
            serviceGetCity.cancel();
        }
        if (serviceUserRegistration != null) {
            serviceUserRegistration.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceGetCurrency != null) {
            serviceGetCurrency.cancel();
        }
        if (serviceGetCountry != null) {
            serviceGetCountry.cancel();
        }
        if (serviceGetCity != null) {
            serviceGetCity.cancel();
        }
        if (serviceUserRegistration != null) {
            serviceUserRegistration.cancel();
        }

        super.onDestroyView();
        unbinder.unbind();
    }

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2222;

    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLocation.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLocation.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getMainActivity(), "AIzaSyBzTbQtJLcISc46HpYe-1g6Kwrde8j0ZjM");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getMainActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void callForCurrentAddress() {
        fusedLocationClient.getLastLocation().addOnCompleteListener(
                new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location != null) {
                            try {
                                testCurr(location);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );

    }

    private void testCurr(Location location) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getMainActivity(), Locale.getDefault());

//        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        txtLocation.setText(address);

        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }
}

