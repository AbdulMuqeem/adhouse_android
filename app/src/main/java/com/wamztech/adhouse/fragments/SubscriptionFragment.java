package com.wamztech.adhouse.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
 ;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.gson.Gson;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.adapters.SubscriptionImageAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.DialogFactory;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.ContactUsModal;
import com.wamztech.adhouse.models.Subscription;
import com.wamztech.adhouse.models.SubscriptionImages;
import com.wamztech.adhouse.models.SubscriptionResult;
import com.wamztech.adhouse.models.UserSubscription;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.SubscriptionBinder;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubscriptionFragment extends BaseFragment implements View.OnClickListener {

    public static final String KEY_OBJECT = "key-dataaa";
    Button btnContactUs;
    ViewPager viewPagerImages;
    GridView gridIndividual;
    GridView gridCommercial;
    CirclePageIndicator indicator;

    double conversion = 1;

    GenericArrayAdapter<Subscription> adapterCommercial;
    GenericArrayAdapter<Subscription> adapterIndividual;
    SubscriptionImageAdapter adapterImageViewPager;

    BraintreeFragment braintreeFragment;
    String currency = "USD";
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    Unbinder unbinder;
    @BindView(R.id.progressUploading)
    ProgressBar progressUploading;
    int entityId;
    String clientId = "";
    boolean isScreenSmall;
    private Call<WebResponse<ContactUsModal>> serviceGetContactInfo;
    private Call<WebResponse<UserSubscription>> serviceSubscribe;
    private Call<WebResponse<SubscriptionResult>> serviceSubscription;
    String contactInfo = null;


    View individualSelectedView = null;
    View commercialSelectedView = null;

    public static SubscriptionFragment newInstance() {
        SubscriptionFragment fragment = new SubscriptionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = new DisplayMetrics();
        getMainActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        if (screenInches < 4.5) {
            isScreenSmall = true;
        }
        dataSet();
    }

    private void dataSet() {
        adapterImageViewPager = new SubscriptionImageAdapter(getActivity(), new ArrayList<SubscriptionImages>(), this);
        adapterIndividual = new GenericArrayAdapter<>(getActivity(), new ArrayList<Subscription>(), new SubscriptionBinder(isScreenSmall));
        adapterCommercial = new GenericArrayAdapter<>(getActivity(), new ArrayList<Subscription>(), new SubscriptionBinder(isScreenSmall));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.subscriptiion, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        gridIndividual.setAdapter(adapterIndividual);
        gridCommercial.setAdapter(adapterCommercial);
        viewPagerImages.setAdapter(adapterImageViewPager);
        indicator.setViewPager(viewPagerImages);
        indicator.setSnap(true);

        serviceGetContactInfo();
        serviceCallSubscription();
        listeners();
    }

    private void listeners() {
        gridIndividual.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if(view != null){
                    if(individualSelectedView != null){
                        individualSelectedView.findViewById(R.id.llContent).setBackgroundResource(R.drawable.rectangle_borders);
                    }
                    view.findViewById(R.id.llContent).setBackgroundResource(R.drawable.rectangle_borders_blue);
                    individualSelectedView = view;
                }

//                if (linearLayout.isEnabled()) {
//                    final Subscription entity = (Subscription) adapterIndividual.getItem(position);
//
//                    GenericDialog dFragment = GenericDialog.newInstance();
//                    dFragment.setListener(new DialogListener() {
//
//                        @Override
//                        public void onOkClicked() {
//                            if (braintreeFragment != null) {
//                                Double price = Double.valueOf(entity.getPrice()) * conversion;
//                                PayPalRequest request = new PayPalRequest(String.valueOf(price))
//                                        .currencyCode(currency)
//                                        .intent(PayPalRequest.INTENT_AUTHORIZE);
//                                PayPal.requestOneTimePayment(braintreeFragment, request);
//                                progressUploading.setVisibility(View.VISIBLE);
//                                linearLayout.setEnabled(false);
//                                entityId = entity.getId();
//                            }
////                        WebServiceFactory.getInstance().subscribePackage(
////                                prefHelper.getUser().getId(),
////                                entity.getId(),
////                                "success"
////                        )
////                                .enqueue(new Callback<WebResponse<UserSubscription>>() {
////                                    @Override
////                                    public void onResponse(Call<WebResponse<UserSubscription>> call, Response<WebResponse<UserSubscription>> response) {
////                                        if (response.body().isSuccess()) {
////                                            getMainActivity().getSideMenuFragment().serviceCallUserSubscription();
////
////                                            SideMenuFragment fragment = (SideMenuFragment) getFragmentManager().findFragmentByTag("SideMenuFragment");
////                                            if (fragment != null) {
////                                                fragment.bindData();
////                                            }
////                                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
////                                        } else {
////                                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
////                                        }
////                                    }
////
////                                    @Override
////                                    public void onFailure(Call<WebResponse<UserSubscription>> call, Throwable t) {
////                                        t.printStackTrace();
////                                    }
////                                });
//                        }
//
//                        @Override
//                        public void onCancelClicked() {
//
//                        }
//                    });
//                    dFragment.setTitleContent(getResources().getString(R.string.subscribe_pack), (getResources().getString(R.string.are_you_sure_subs_pack)));
//                    dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");
//                }
            }
        });


        gridCommercial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(view != null){
                    if(commercialSelectedView != null){
                        commercialSelectedView.findViewById(R.id.llContent).setBackgroundResource(R.drawable.rectangle_borders);
                    }
                    view.findViewById(R.id.llContent).setBackgroundResource(R.drawable.rectangle_borders_blue);
                    commercialSelectedView = view;

                }


//                if (linearLayout.isEnabled()) {
//                    final Subscription entity = (Subscription) adapterCommercial.getItem(position);
//
//                    GenericDialog dFragment = GenericDialog.newInstance();
//                    dFragment.setListener(new DialogListener() {
//
//                        @Override
//                        public void onOkClicked() {
//
////                            BraintreeFragment braintreeFragment = BraintreeFragment.newInstance(getMainActivity(), "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI4NWUyZDgzMGU1MDIxZGU1ODFiODIxNGI5ZjM2OGQ5NzJiZDRkNTA3NWRiMWZlNzc5NDRhM2UxMWIxOGJlYzM2fGNyZWF0ZWRfYXQ9MjAxNy0xMi0yN1QwNzowOToyOS45MjAxNTk5MTcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=");
//                            if (braintreeFragment != null) {
//                                Double price = Double.valueOf(entity.getPrice()) * conversion;
//                                PayPalRequest request = new PayPalRequest(String.valueOf(price))
//                                        .currencyCode(currency)
//                                        .intent(PayPalRequest.INTENT_AUTHORIZE);
//                                PayPal.requestOneTimePayment(braintreeFragment, request);
//                                progressUploading.setVisibility(View.VISIBLE);
//                                linearLayout.setEnabled(false);
//                                entityId = entity.getId();
//                            }
////                        WebServiceFactory.getInstance().subscribePackage(
////                                prefHelper.getUser().getId(),
////                                entity.getId(),
////                                "success"
////                        )
////                                .enqueue(new Callback<WebResponse<UserSubscription>>() {
////                                    @Override
////                                    public void onResponse(Call<WebResponse<UserSubscription>> call, Response<WebResponse<UserSubscription>> response) {
////                                        if (response.body().isSuccess()) {
////                                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
////                                        } else {
////                                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
////                                        }
////
////
////                                    }
////
////                                    @Override
////                                    public void onFailure(Call<WebResponse<UserSubscription>> call, Throwable t) {
////                                        t.printStackTrace();
//////                                        UtilHelper.showToast(getMainActivity(), "failed");
////                                    }
////                                });
//                        }
//
//                        @Override
//                        public void onCancelClicked() {
//
//                        }
//                    });
//                    dFragment.setTitleContent(getResources().getString(R.string.subscribe_pack), getResources().getString(R.string.are_you_sure_subs_pack));
//                    dFragment.show(getActivity().getSupportFragmentManager(), "GenericDialog");
//                }
            }
        });


        btnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(contactInfo);
            }
        });



    }

    private void showPopup(String contactUsInfo) {

        DialogFactory.createOneButtonMessageDialog(getMainActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }, getString(R.string.get_in_touch), contactUsInfo).show();
    }

    private void serviceGetContactInfo() {

        serviceGetContactInfo = WebServiceFactory.getInstance().getInTouchDetail(prefHelper.getLang());
        serviceGetContactInfo.enqueue(new Callback<WebResponse<ContactUsModal>>() {
            @Override
            public void onResponse(Call<WebResponse<ContactUsModal>> call, Response<WebResponse<ContactUsModal>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    //contactInfo

                    ContactUsModal contactUsModal = response.body().getResult();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder
                             .append(getString(R.string.address).toUpperCase()).append( " : ").append(contactUsModal.getAddress())
                             .append("\n")
                             .append(getString(R.string.phone_number).toUpperCase()).append( " : ").append(contactUsModal.getPhone())
                             .append("\n")
                             .append(getString(R.string.email).toUpperCase()).append( " : ").append(contactUsModal.getEmail());

                    contactInfo = stringBuilder.toString();

                } else {
                    //UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ContactUsModal>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void serviceCallSubscribe(int subscriptionId, String payload, String nonce) {
        if (subscriptionId == 0) return;

        progressUploading.setVisibility(View.VISIBLE);
        linearLayout.setEnabled(false);

        serviceSubscribe = WebServiceFactory.getInstance().subscribePackage(
                prefHelper.getUser().getId(),
                entityId, payload, nonce
        );
        serviceSubscribe.enqueue(new Callback<WebResponse<UserSubscription>>() {
            @Override
            public void onResponse(Call<WebResponse<UserSubscription>> call, Response<WebResponse<UserSubscription>> response) {
                progressUploading.setVisibility(View.GONE);
                linearLayout.setEnabled(true);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    //getMainActivity().getSideMenuFragment().serviceCallUserSubscription();

                    SideMenuFragment fragment = (SideMenuFragment) getFragmentManager().findFragmentByTag("SideMenuFragment");
                    if (fragment != null) {
                        fragment.bindData();
                    }
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                } else {
                    UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<UserSubscription>> call, Throwable t) {
                if (progressUploading != null) {
                    progressUploading.setVisibility(View.GONE);
                    linearLayout.setEnabled(true);
                }
                t.printStackTrace();
            }
        });
    }

    private void initViews(View view) {
        btnContactUs = (Button) view.findViewById(R.id.btnContactUs);
        viewPagerImages = (ViewPager) view.findViewById(R.id.viewPagerImages);
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        gridIndividual = (GridView) view.findViewById(R.id.gridIndividual);
        gridCommercial = (GridView) view.findViewById(R.id.gridCommercial);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.subscription));
        titleBar.showBackButton(getMainActivity());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPager: {
//                addDockableFragment(ImageViewFragment.newInstance(viewPagerImages.getCurrentItem() , adapterImageViewPager.getList() ));
            }
            break;
        }
    }

    public void serviceCallSubscription() {
        linearLayout.setEnabled(false);
        progressUploading.setVisibility(View.VISIBLE);
        serviceSubscription = WebServiceFactory.getInstance().getSubscriptionPackages(prefHelper.getUser().getId());
        serviceSubscription.enqueue(new Callback<WebResponse<SubscriptionResult>>() {
            @Override
            public void onResponse(Call<WebResponse<SubscriptionResult>> call, Response<WebResponse<SubscriptionResult>> response) {
                linearLayout.setEnabled(true);
                progressUploading.setVisibility(View.GONE);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {

                    adapterImageViewPager.clear();
                    adapterImageViewPager.addAll(response.body().getResult().getImages());

                    adapterIndividual.clear();
                    adapterIndividual.addAll(response.body().getResult().getIndividual());
                    adapterIndividual.notifyDataSetChanged();

                    adapterCommercial.clear();
                    adapterCommercial.addAll(response.body().getResult().getCommercial());
                    adapterCommercial.notifyDataSetChanged();

                    clientId = response.body().getResult().getClientId();
                    conversion = response.body().getResult().getConversionRate();
//                            packageId - response.body().getResult().getCommercial().
                    //new getCurrencyConversionRate().execute("http://www.apilayer.net/api/live?access_key=4922423de85fb227a9464b72ba77466b&format=1&Currencies=qar");
                    setupBrainTree();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<SubscriptionResult>> call, Throwable t) {
                if (linearLayout != null) {
                    linearLayout.setEnabled(true);
                    progressUploading.setVisibility(View.GONE);
                }
                t.printStackTrace();
            }


        });
    }

    private void setupBrainTree() {
        try {

//            String clientId = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI4ZWZkNTU0YTIzYmRkZDZjOGFmMzBiMDAxMTBiNzZhZTUxYTJlNmY0ZDU4MzVmNmQ1MThhYjA1Mjg5NzJlNzc3fGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTAxLTAxVDA5OjQyOjQyLjY1Njk0NTUwMCswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9eGp3bnc4cmRiM3N4YzNzNiIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy94andudzhyZGIzc3hjM3M2L2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveGp3bnc4cmRiM3N4YzNzNi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9jbGllbnQtYW5hbHl0aWNzLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20veGp3bnc4cmRiM3N4YzNzNiJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiSm9obiBQYWdlJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVVxZkhfZU5nMzVzQnc4MnZMelpiSEtxdFJLRFlUWEVlSjVGSlJ2U1g0bnNNYXctVXR1U2Q1QkJVWWFwM2dPU2tyWnlXS09BV2RUNUY4OTkiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJ4andudzhyZGIzc3hjM3M2IiwidmVubW8iOiJvZmYifQ==";
            braintreeFragment = BraintreeFragment.newInstance(getMainActivity(), clientId);
//            braintreeFragment = BraintreeFragment.newInstance(getMainActivity(), "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI4NWUyZDgzMGU1MDIxZGU1ODFiODIxNGI5ZjM2OGQ5NzJiZDRkNTA3NWRiMWZlNzc5NDRhM2UxMWIxOGJlYzM2fGNyZWF0ZWRfYXQ9MjAxNy0xMi0yN1QwNzowOToyOS45MjAxNTk5MTcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=");

            braintreeFragment.addListener(new PaymentMethodNonceCreatedListener() {
                @Override
                public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
                    if (progressUploading != null) {
                        progressUploading.setVisibility(View.GONE);
                        linearLayout.setEnabled(true);
                        // Send nonce to server
                        String nonce = paymentMethodNonce.getNonce();
                        if (paymentMethodNonce instanceof PayPalAccountNonce) {
                            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce) paymentMethodNonce;
                            String payload = new Gson().toJson(payPalAccountNonce);
                            serviceCallSubscribe(entityId, payload, payPalAccountNonce.getNonce());
                        }
                    }
                }
            });


            braintreeFragment.addListener(new BraintreeCancelListener() {
                @Override
                public void onCancel(int requestCode) {
                    if (progressUploading != null) {
                        progressUploading.setVisibility(View.GONE);
                        linearLayout.setEnabled(true);
                        UtilHelper.showToast(getMainActivity(), "Paypal - Payment Rejected");
                    }
                    // Use this to handle a canceled activity, if the given requestCode is important.
                    // You may want to use this callback to hide loading indicators, and prepare your UI for input
                }
            });

            braintreeFragment.addListener(new BraintreeErrorListener() {
                @Override
                public void onError(Exception error) {
                    if (progressUploading != null) {
                        progressUploading.setVisibility(View.GONE);
                        linearLayout.setEnabled(true);
                        UtilHelper.showToast(getMainActivity(), error.getMessage());
                    }
//                    ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
//                    BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
//                    if (cardErrors != null) {
//                        // There is an issue with the credit card.
//                        BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
//                        if (expirationMonthError != null) {
//                            // There is an issue with the expiration month.
//                            UtilHelper.showToast(getMainActivity(), expirationMonthError.getMessage());
//                        }
//                    }
                }
            });

        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            UtilHelper.showToast(getMainActivity(), e.getMessage());
        }
    }

    @Override
    public void onPause() {
        if (serviceSubscription != null) {
            serviceSubscription.cancel();
        }
        if (serviceSubscribe != null) {
            serviceSubscribe.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceSubscription != null) {
            serviceSubscription.cancel();
        }
        if (serviceSubscribe != null) {
            serviceSubscribe.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

//    private class getCurrencyConversionRate extends AsyncTask<String, String, String> {
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressUploading.setVisibility(View.VISIBLE);
//            linearLayout.setEnabled(false);
//        }
//
//        protected String doInBackground(String... params) {
//
//
//            HttpURLConnection connection = null;
//            BufferedReader reader = null;
//
//            try {
//                URL url = new URL(params[0]);
//                connection = (HttpURLConnection) url.openConnection();
//                connection.connect();
//
//
//                InputStream stream = connection.getInputStream();
//
//                reader = new BufferedReader(new InputStreamReader(stream));
//
//                StringBuffer buffer = new StringBuffer();
//                String line = "";
//
//                while ((line = reader.readLine()) != null) {
//                    buffer.append(line + "\n");
//                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
//
//                }
//
//                return buffer.toString();
//
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if (connection != null) {
//                    connection.disconnect();
//                }
//                try {
//                    if (reader != null) {
//                        reader.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            progressUploading.setVisibility(View.GONE);
//            linearLayout.setEnabled(true);
//            Gson gson = new Gson();
//            JsonElement element = gson.fromJson(result, JsonElement.class);
//            JsonObject jsonObj = element.getAsJsonObject();
//            String conversionrate = jsonObj.get("quotes").getAsJsonObject().get("USDQAR").getAsString();
//            conversion = Double.valueOf(conversionrate);
//            setupBrainTree();
//        }
//    }
}
