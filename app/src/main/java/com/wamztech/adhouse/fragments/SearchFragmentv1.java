package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.models.Search;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AdsListItemBinder;
import com.wamztech.adhouse.ui.views.viewbinders.CategoryBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamztech.adhouse.R.id.autoCompleteSearch;


public class SearchFragmentv1 extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener, TextView.OnEditorActionListener, AbsListView.OnScrollListener {


    private static final String KEY_SEARCH_ARRAY = "KEY_SEARCH_ARRAY";
    private static final String KEY_SEARCH_TITLE = "KEY_SEARCH_TITLE";
    private static final String IS_FROM_FILTER = "IS_FROM_FILTER";

    @BindView(R.id.layoutSearch)
    RelativeLayout layoutSearch;
    @BindView(R.id.listViewSearch)
    ListView listViewSearch;
    Unbinder unbinder;
    int offset = 0;
    int limit = 10;
    boolean isLoading = false;
    int isLike;
    int retrofitCount;
    @BindView(autoCompleteSearch)
    AutoCompleteTextView edtSearch;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    private GenericArrayAdapter adapterSearch;
    private String searchKeyword;
    private boolean isFromFilter;
    private Call<WebResponse<ArrayList<Ad>>> requestFiltered;
    private GenericArrayAdapter<Category> adapterCategory;
    private int categoryID = 70;
    private ArrayList<Category> arrSpCategories;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> updatedList;
    private Call<WebResponse<Ad>> serviceAddFavourite;
    private Call<WebResponse<Search>> serviceGetSearchResult;
    private Call<WebResponse<ArrayList<Category>>> serviceGetSearchcategories;

    public static SearchFragmentv1 newInstance() {
        SearchFragmentv1 fragment = new SearchFragmentv1();
        return fragment;
    }

    public static SearchFragmentv1 newInstance(String mSearchType, ArrayList<Ad> result, boolean b) {
        Bundle args = new Bundle();
        SearchFragmentv1 fragment = new SearchFragmentv1();
        args.putString(KEY_SEARCH_TITLE, mSearchType);
        args.putParcelableArrayList(KEY_SEARCH_ARRAY, result);
        args.putBoolean(IS_FROM_FILTER, b);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrSpCategories = new ArrayList<>();
        updatedList = new ArrayList<>();
        adapterCategory = new GenericArrayAdapter<>(getMainActivity(), arrSpCategories, new CategoryBinder());
        adapterSearch = new GenericArrayAdapter<>(getActivity(), new ArrayList<Ad>(), new AdsListItemBinder(this));
        adapter = new ArrayAdapter<String>
                (getMainActivity(), android.R.layout.select_dialog_item, updatedList);

    }

    @Override
    public boolean onEditorAction(TextView view, int arg1, KeyEvent arg2) {
        if (edtSearch.getText().toString().trim().length() > 0) {
            if ((arg1 == EditorInfo.IME_ACTION_SEARCH)) {
                searchKeyword = edtSearch.getText().toString().trim();
                offset = 0;
                adapterSearch.clear();
                serviceCallSearch(0, false);
            }
        } else {
            UtilHelper.showToast(getMainActivity(), getString(R.string.field_cannot_be_empty));
        }
        hideKeyboard(getMainActivity().getCurrentFocus());
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCallCategory();
        listeners();
        autoCompleteAppendedText();
        listViewSearch.setAdapter(adapterSearch);
        edtSearch.setAdapter(adapter);
        edtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchKeyword = edtSearch.getText().toString().split(" ")[0];
                edtSearch.setText(searchKeyword);
                categoryID = arrSpCategories.get(position).getId();
                hideKeyboard(getMainActivity().getCurrentFocus());
                serviceCallSearch(0, true);
            }
        });
    }

    public void bindFilterData(ArrayList<Ad> result, boolean isFilter, Call<WebResponse<ArrayList<Ad>>> filtered) {
        this.requestFiltered = filtered.clone();

        if (result != null) {
            isFromFilter = isFilter;
            adapterSearch.clear();
            adapterSearch.addAll(result);
            adapterSearch.notifyDataSetChanged();
        } else {
            adapterSearch.clear();
            adapterSearch.notifyDataSetChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onPause() {
        if (getMainActivity() != null)
            getMainActivity().setSearchKeyWord(searchKeyword);

        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetSearchcategories != null) {
            serviceGetSearchcategories.cancel();
        }
        if (serviceGetSearchResult != null) {
            serviceGetSearchResult.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetSearchcategories != null) {
            serviceGetSearchcategories.cancel();
        }
        if (serviceGetSearchResult != null) {
            serviceGetSearchResult.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getMainActivity() != null && getMainActivity().getSearchKeyWord() != null) {
            isLoading = true;
            offset = 0;
            if (isFromFilter) {
                Call<WebResponse<ArrayList<Ad>>> clone = requestFiltered.clone();

                clone.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
                    @Override
                    public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {

                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response != null && response.body() != null && response.body().getResult() != null) {

                            adapterSearch.clear();
                            adapterSearch.addAll(response.body().getResult());

                        } else {
                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {

                    }
                });
            }
        }
    }

    private void listeners() {
        imgSearch.setOnClickListener(this);
        edtSearch.setOnEditorActionListener(this);
        listViewSearch.setOnItemClickListener(this);
        listViewSearch.setOnScrollListener(this);


    }

    @Override
    public void setTitleBar(TitleBar titleBar) {

        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.Search));
        titleBar.showBackButton(getMainActivity());
        titleBar.setRightButton(R.drawable.filter, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDockableFragment(FilterFragment.newInstance(searchKeyword));
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Ad product = ((Ad) listViewSearch.getAdapter().getItem(position));
        addDockableFragment(DetailFragment.newInstance(product));
    }

    @Override
    public void onClick(final View v) {

        final Ad item;
        switch (v.getId()) {

            case R.id.imgSearch: {
                hideKeyboard(v);

                if (edtSearch.getText().toString().trim().length() > 0) {
                    isLoading = true;

                    searchKeyword = edtSearch.getText().toString().trim();
                    offset = 0;
                    adapterSearch.clear();
                    serviceCallSearch(0, false);

                } else {
                    UtilHelper.showToast(getMainActivity(), getString(R.string.field_cannot_be_empty));
                }
            }

            break;
            case R.id.imgFavourite: {
                final ImageView imgFavourite = (ImageView) v;
                final int position = (int) v.getTag();
                item = (Ad) adapterSearch.getItem(position);
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.guest_user));
                    return;
                }
                if (item.isFavourite()) {
                    isLike = 0;
                } else {
                    isLike = 1;
                }

                imgFavourite.setEnabled(false);
                serviceAddFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), item.getId(), isLike);
                serviceAddFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                        imgFavourite.setEnabled(true);
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {
                            if (response.body().getResult().isFavourite()) {
                                imgFavourite.setImageResource(R.drawable.listheart);
                                item.setIsLike(isLike);

                            } else {

                                imgFavourite.setImageResource(R.drawable.heartfilled);
                                item.setIsLike(isLike);
                            }
                        }
                        adapterSearch.notifyDataSetChanged();
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                        if (imgFavourite != null) {
                            imgFavourite.setEnabled(true);
                        }
                        t.printStackTrace();
                    }
                });
            }
            break;
        }
    }

    public void serviceCallSearch(int offset, boolean isFromSearch) {


        if (isFromSearch) {
            offset = 0;
            limit = 10;
        }

        isLoading = true;

        serviceGetSearchResult = WebServiceFactory.getInstance().getSearchResult(
                prefHelper.getUser().getId(),
                offset,
                limit,
                searchKeyword,
                categoryID,
                prefHelper.getLang()
        );
        serviceGetSearchResult.enqueue(new Callback<WebResponse<Search>>() {
            @Override
            public void onResponse(Call<WebResponse<Search>> call, Response<WebResponse<Search>> response) {
                isLoading = false;
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    retrofitCount = response.body().getResult().getCount();
                    adapterSearch.clear();
                    adapterSearch.addAll(response.body().getResult().getAd());
                    adapterSearch.notifyDataSetChanged();
                } else {
                    if (response.body().getResult() != null && searchKeyword != null) {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        adapterSearch.clear();
                        adapterSearch.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Search>> call, Throwable t) {
                isLoading = false;
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    public void serviceCallCategory() {
        serviceGetSearchcategories = WebServiceFactory.getInstance().getSearchcategories(prefHelper.getLang());
        serviceGetSearchcategories.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrSpCategories.clear();
                    arrSpCategories.addAll(response.body().getResult());
                    adapterCategory.notifyDataSetChanged();
                    isFromFilter = false;

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                    adapterSearch.clear();
                    adapterSearch.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }


        });

    }

    private void autoCompleteAppendedText() {

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                offset = 0;
                limit = 10;
                getMainActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        filterListing(s);
                    }
                });
            }

            @Override
            public void afterTextChanged(final Editable s) {
            }
        });
    }

    private void filterListing(CharSequence s) {
        String itemSearch;

        if (s.length() > 0) {
            offset = 0;
            limit = 10;
            adapter.clear();
            if (arrSpCategories != null) {
                for (int i = 0; i < arrSpCategories.size(); i++) {
                    itemSearch = (s.toString() + " " + getString(R.string.in_category) + " " + arrSpCategories.get(i).getName());
                    adapter.add(itemSearch);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

}


