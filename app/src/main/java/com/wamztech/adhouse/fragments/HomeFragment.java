package com.wamztech.adhouse.fragments;

import android.os.Bundle;
;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;


import com.google.firebase.iid.FirebaseInstanceId;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.adapters.GridClassifiedCategoryAdapter;
import com.wamztech.adhouse.constants.APIConstants;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.fragments.user.LoginFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.OnItemDelete;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.Ads;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.models.ClassifiedCategory;
import com.wamztech.adhouse.models.Currency;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.CustomGridViewHeight;
import com.wamztech.adhouse.ui.views.HorizontalListView;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AdsGridItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends BaseFragment implements View.OnClickListener, OnItemDelete, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.btnSubmitAd)
    AnyButton btnSubmitAd;
    @BindView(R.id.txtVehicle)
    AnyTextView txtVehicle;
    @BindView(R.id.txtVehicleViewAll)
    AnyTextView txtVehicleViewAll;
    @BindView(R.id.lvVehicle)
    HorizontalListView lvVehicle;
    @BindView(R.id.txtProperty)
    AnyTextView txtProperty;
    @BindView(R.id.txtPropertiesViewAll)
    AnyTextView txtPropertiesViewAll;
    @BindView(R.id.lvProperties)
    HorizontalListView lvProperties;
    @BindView(R.id.txtClassifed)
    AnyTextView txtClassifed;
    @BindView(R.id.txtClassifedViewAll)
    AnyTextView txtClassifedViewAll;
    @BindView(R.id.gdClasified)
    CustomGridViewHeight gdClasified;
    Unbinder unbinder;
    GenericArrayAdapter<Ad> adapterVehicleAd;
    GenericArrayAdapter<Ad> adapterPropertiesAd;
    GridClassifiedCategoryAdapter adapterClassified;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private ArrayList<Ad> arrVehecleAds;
    private ArrayList<Ad> arrPropertiesAds;
    private ArrayList<ClassifiedCategory> arrClasifiedAds;
    private Call<WebResponse<User>> serviceCheckBlock;
    private Call<WebResponse> serviceUpdateDeviceToken;
    private Call<WebResponse<Ads>> serviceAds;
    private Call<WebResponse<ArrayList<Ad>>> serviceGetVehicleAds;
    private Call<WebResponse<ArrayList<Category>>> serviceCategory;
    private Call<WebResponse<Currency>> serviceGetCurrency;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrVehecleAds = new ArrayList<>();
        arrPropertiesAds = new ArrayList<>();
        arrClasifiedAds = new ArrayList<>();
        dataSet();
    }

    private void getCategoryNames() {
        serviceCategory = WebServiceFactory.getInstance().getCategory(prefHelper.getLang());
        serviceCategory.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess() && isAdded()) {
                    if (response.body().getResult().size() > 2) {
                        txtVehicle.setText(response.body().getResult().get(0).getName());
                        txtProperty.setText(response.body().getResult().get(1).getName());
                        txtClassifed.setText(response.body().getResult().get(2).getName());
                    }

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void dataSet() {
        adapterVehicleAd = new GenericArrayAdapter<>(getActivity(), arrVehecleAds, new AdsGridItemBinder());
        adapterPropertiesAd = new GenericArrayAdapter<>(getActivity(), arrPropertiesAds, new AdsGridItemBinder());
        adapterClassified = new GridClassifiedCategoryAdapter(getActivity(), arrClasifiedAds, false, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        setDefaultLocale();
        gdClasified.setFocusable(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setListeners();

        lvVehicle.setAdapter(adapterVehicleAd);
        lvProperties.setAdapter(adapterPropertiesAd);
        gdClasified.setAdapter(adapterClassified);

        serviceCallAds();
        serviceCallVehicleAds();
        getCategoryNames();

    }

    private void setListeners() {
        swipeLayout.setOnRefreshListener(this);
        txtVehicleViewAll.setOnClickListener(this);
        txtPropertiesViewAll.setOnClickListener(this);
        txtClassifedViewAll.setOnClickListener(this);

        lvVehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addDockableFragment(DetailFragment.newInstance(((Ad) adapterVehicleAd.getItem(position))));
            }
        });

        lvProperties.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addDockableFragment(DetailFragment.newInstance(((Ad) adapterPropertiesAd.getItem(position))));
            }
        });
        btnSubmitAd.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        UpdateDeviceToken();
        checkUserBlock();
        getUserCurrency();
    }

    private void getUserCurrency() {
        if (!prefHelper.getUser().isGuestUser()) {
            serviceGetCurrency = WebServiceFactory.getInstance().getUserCurrency(prefHelper.getUser().getId());
            serviceGetCurrency.enqueue(new Callback<WebResponse<Currency>>() {
                @Override
                public void onResponse(Call<WebResponse<Currency>> call, Response<WebResponse<Currency>> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }
                    if (response.body().isSuccess()) {

                        User user = prefHelper.getUser();
                        user.setCurrency_symbol(response.body().getResult().getSymbol());
                        prefHelper.putUser(user);

                    } else {
                        UtilHelper.showToast(getMainActivity(), response.message());
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Currency>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }





    }

    private void checkUserBlock() {
        if (prefHelper != null && prefHelper.getUser() != null && prefHelper.getUser().getId() != null) {
            serviceCheckBlock = WebServiceFactory.getInstance().checkBlockStatus(prefHelper.getUser().getId());
            serviceCheckBlock.enqueue(new Callback<WebResponse<User>>() {
                @Override
                public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }
                    if (response != null && response.body() != null && response.body().isSuccess()) {
                        if (response.body().getResult().getStatus() == 0) {
                            emptyBackStack();
                            addDockableFragment(LoginFragment.newInstance());
                            prefHelper.putUser(null);
                            UtilHelper.showToast(getMainActivity(), "The user is blocked, Please contact admin");
                        }
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    private void UpdateDeviceToken() {
        if (prefHelper != null && prefHelper.getUser() != null && prefHelper.getUser().getId() != null) {
            serviceUpdateDeviceToken = WebServiceFactory.getInstance().updateDeviceToken(prefHelper.getUser().getId(), FirebaseInstanceId.getInstance().getToken(), APIConstants.device_type);
            serviceUpdateDeviceToken.enqueue(new Callback<WebResponse>() {
                @Override
                public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }
                    if (response != null && response.body() != null && response.body().isSuccess()) {
                    }
                }

                @Override
                public void onFailure(Call<WebResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtVehicleViewAll:
                addDockableFragment(VehicleListingFragment.newInstance());
                break;
            case R.id.txtPropertiesViewAll:
                addDockableFragment(PropertesListingFragment.newInstance());
                break;
            case R.id.txtClassifedViewAll:
                addDockableFragment(ClassifiedListingFragment.newInstance(-1, ""));
                break;
            case R.id.btnSubmitAd:
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getString(R.string.sign_in_req));
                } else {
                    addDockableFragment(SubmitAdFragment.newInstance(false));
                }
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();

        titleBar.setHeading(getString(R.string.Home));

        titleBar.setLeftButton(R.drawable.dropdown, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draweropen();
            }
        });

        titleBar.setRightButton(R.drawable.searchbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDockableFragment(SearchFragmentv1.newInstance());
            }
        });

        titleBar.setRightTwoButton(R.drawable.notification2, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), "please sign in");
                } else {
                    addDockableFragment(NotificationFragment.newInstance());
                }
            }
        });
    }

    @Override
    public void itemDelect(int position) {

    }

    public void serviceCallAds() {
        serviceAds = WebServiceFactory.getInstance().getAds(prefHelper.getUser().getId(), prefHelper.getLang());
        serviceAds.enqueue(new Callback<WebResponse<Ads>>() {
            @Override
            public void onResponse(Call<WebResponse<Ads>> call, Response<WebResponse<Ads>> response) {
                swipeLayout.setRefreshing(false);
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                try {
                    if (response != null && response.body() != null && response.body().isSuccess()) {
//                        adapterVehicleAd.clear();
//                        adapterVehicleAd.addAll(response.body().getResult().getVehicles());
//                        adapterVehicleAd.notifyDataSetChanged();


                        adapterPropertiesAd.clear();
                        adapterPropertiesAd.addAll(response.body().getResult().getProperties());
                        adapterPropertiesAd.notifyDataSetChanged();

                        adapterClassified.clear();
                        adapterClassified.addAll(response.body().getResult().getClassifiedCategory());
                        adapterClassified.notifyDataSetChanged();

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Ads>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void serviceCallVehicleAds() {
        serviceGetVehicleAds = WebServiceFactory.getInstance().getVehicleAds(prefHelper.getUser().getId(), prefHelper.getLang());
        serviceGetVehicleAds.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapterVehicleAd.clear();
                    adapterVehicleAd.addAll(response.body().getResult());
                    adapterVehicleAd.notifyDataSetChanged();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    @Override
    public void onPause() {
        swipeLayout.setRefreshing(false);
        if (serviceCheckBlock != null) {
            serviceCheckBlock.cancel();
        }
        if (serviceAds != null) {
            serviceAds.cancel();
        }
        if (serviceUpdateDeviceToken != null) {
            serviceUpdateDeviceToken.cancel();
        }

        if (serviceCategory != null) {
            serviceCategory.cancel();
        }

        if (serviceGetVehicleAds != null) {
            serviceGetVehicleAds.cancel();
        }

        super.onPause();
    }

    @Override
    public void onDestroyView() {
        swipeLayout.setRefreshing(false);
        if (serviceCheckBlock != null) {
            serviceCheckBlock.cancel();
        }
        if (serviceAds != null) {
            serviceAds.cancel();
        }
        if (serviceUpdateDeviceToken != null) {
            serviceUpdateDeviceToken.cancel();
        }

        if (serviceCategory != null) {
            serviceCategory.cancel();
        }

        if (serviceGetVehicleAds != null) {
            serviceGetVehicleAds.cancel();
        }

        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        if (isNetworkConnected()) {
            serviceCallAds();
            serviceCallVehicleAds();
        } else
            UtilHelper.showToast(getMainActivity(), getString(R.string.no_internet_connection));
    }
}
