package com.wamztech.adhouse.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.util.IabHelper;
import com.wamztech.adhouse.util.IabResult;
import com.wamztech.adhouse.util.Inventory;
import com.wamztech.adhouse.util.Purchase;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;



public class InAppBillingFragmenrt extends BaseFragment implements OnActivityResult{


    static final String ITEM_SKU = "android.test.purchased";
    @BindView(R.id.buyButton)
    Button buyButton;
    @BindView(R.id.clickButton)
    Button clickButton;
    private Unbinder unbinder;
    IabHelper mHelper;
    private String TAG = "InAppBillingFragmenrt";

    public static InAppBillingFragmenrt newInstance() {
        Bundle args = new Bundle();
        InAppBillingFragmenrt fragment = new InAppBillingFragmenrt();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_in_app_billing, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMainActivity().setOnActivityResult(InAppBillingFragmenrt.this);

//
        mHelper = new IabHelper(getMainActivity(),"abcd");
        mHelper.startSetup(new
                                   IabHelper.OnIabSetupFinishedListener() {
                                       public void onIabSetupFinished(IabResult result)
                                       {
                                           if (!result.isSuccess()) {
                                               Log.d(TAG, "In-app Billing setup failed: " +
                                                       result);
                                           } else {
                                               Log.d(TAG, "In-app Billing is set up OK");
                                           }
                                       }
                                   });

}

    @Override
    public void setTitleBar(TitleBar titleBar) {

    }



    @OnClick({R.id.buyButton, R.id.clickButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buyButton:
                break;
            case R.id.clickButton:
                mHelper.launchPurchaseFlow(getMainActivity(), ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
                break;
        }
    }

    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                UtilHelper.showToast(getMainActivity(),"Item Already Owned");
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                buyButton.setEnabled(false);
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        clickButton.setEnabled(true);
                    } else {
                        // handle error
                    }
                }
            };
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

   
}
