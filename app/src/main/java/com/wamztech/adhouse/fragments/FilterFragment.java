package com.wamztech.adhouse.fragments;

import android.content.Intent;
import android.os.Bundle;
 ;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.helpers.Utils;
import com.wamztech.adhouse.interfaces.OnActivityResult;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.Attributes;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.models.City;
import com.wamztech.adhouse.models.Dropdownlist;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.AnyButton;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.ExpandedListView;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.CategoryBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SpCityBinder;
import com.wamztech.adhouse.ui.views.viewbinders.SubAdsAttributesBinderV1;
import com.wamztech.adhouse.ui.views.viewbinders.SubCategoryBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.apptik.widget.MultiSlider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FilterFragment extends BaseFragment implements View.OnClickListener, OnActivityResult, AdapterView.OnItemClickListener {

    public static final int SUB_CATEGORY_ID = 46;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2222;
    private static final String KEY_SEARCH_TITLE = "KEY_SEARCH_TITLE";
    @BindView(R.id.txtLocation)
    FormAutoCompleteTextView txtLocation;
    @BindView(R.id.edAdTitle)
    AnyEditTextView edAdTitle;
    @BindView(R.id.txtCategory)
    AnyTextView txtCategory;
    @BindView(R.id.txtCountry)
    AnyTextView txtCountry;
    @BindView(R.id.txtSelectCategory)
    AnyTextView txtSelectCategory;
    @BindView(R.id.txtSelectCountry)
    AnyTextView txtSelectCountry;
    @BindView(R.id.txtInitialPrice)
    AnyTextView txtInitialPrice;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.txtFeatured)
    AnyTextView txtFeatured;
    @BindView(R.id.spFeatured)
    Spinner spFeatured;
    @BindView(R.id.btnPostAd)
    AnyButton btnPostAd;
    Unbinder unbinder;
    @BindView(R.id.listCategoriesAttributes)
    ExpandedListView listCategoriesAttributes;
    @BindView(R.id.txtSubCategory)
    AnyTextView txtSubCategory;
    @BindView(R.id.txtSelectSubCategory)
    AnyTextView txtSelectSubCategory;
    @BindView(R.id.spSubCategory)
    Spinner spSubCategory;
    @BindView(R.id.classified)
    RelativeLayout classified;
    @BindView(R.id.txtPrice)
    AnyTextView txtPrice;
    @BindView(R.id.txtPriceRange)
    AnyTextView txtPriceRange;
    @BindView(R.id.txtSelectFeature)
    AnyTextView txtSelectFeature;
    @BindView(R.id.rangeSeekbar)
    MultiSlider rangeSeekbar;
    @BindView(R.id.edMinPrice)
    AnyEditTextView edMinPrice;
    @BindView(R.id.edMaxPrice)
    AnyEditTextView edMaxPrice;
    Dropdownlist dropdownlistSelectItem;
    int offset = 0;
    int limit = 10;
    Call<WebResponse<ArrayList<Ad>>> filteredv1;
    private String item;
    private ArrayList<City> arrSpCountry;
    private ArrayList<Category> arrSpCategories;
    private ArrayList<Category> arrSpSubCategories;
    private GenericArrayAdapter<City> adapterCountry;
    private GenericArrayAdapter<Category> adapterCategory;
    private GenericArrayAdapter<Category> adapterSubCategory;
    private GenericArrayAdapter<Attributes> adapterAttributes;
    private

    @Nullable
    int categoryID;

    @Nullable
    int countryID;

    private ArrayList<Attributes> arrAttributes;
    private ArrayAdapter<String> adapterSpinner;
    private
    @Nullable
    int isFeatured;
    private
    @Nullable
    int subCategory;
    private String locationName;
    private
    @Nullable
    double latitude;

    private String TAG = "FilterFragment";

    private
    @Nullable
    double longitude;
    private AnyEditTextView edtAttribute;
    private Spinner spAttribute;
    private ArrayList<Attributes> arrEdAtt;
    private JSONArray arrJason;
    private String mSearchType;
    private int user_id;
    private int priceMin = 0;
    private int priceMax = 0;
//    private PlaceAutocompleteAdapter mAdapter;
//    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
//            = new ResultCallback<PlaceBuffer>() {
//        @Override
//        public void onResult(PlaceBuffer places) {
//            if (!places.getStatus().isSuccess()) {
//                places.release();
//                return;
//            }
//            final Place place = places.get(0);
//
//            latitude = place.getLatLng().latitude;
//            longitude = place.getLatLng().longitude;
//
//            final CharSequence thirdPartyAttribution = places.getAttributions();
//            if (thirdPartyAttribution == null) {
//            } else {
//            }
//
//            places.release();
//        }
//    };
//    private AdapterView.OnItemClickListener mAutocompleteClickListener
//            = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////            final AutocompletePrediction item = mAdapter.getItem(position);
////            final String placeId = item.getPlaceId();
//
////            final CharSequence primaryText = item.getPrimaryText(null);
////            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
////                    .getPlaceById(getMainActivity().mGoogleApiClient, placeId);
////            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
//
//        }
//    };
    private Call<WebResponse<ArrayList<City>>> serviceCountry;
    private Call<WebResponse<ArrayList<Category>>> serviceCategory;
    private Call<WebResponse<ArrayList<Attributes>>> serviceAttributes;
    private Call<WebResponse<ArrayList<Category>>> serviceSubCategories;

    public static FilterFragment newInstance() {
        Bundle args = new Bundle();
        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FilterFragment newInstance(String searchtitle) {
        Bundle args = new Bundle();
        FilterFragment fragment = new FilterFragment();
        args.putString(KEY_SEARCH_TITLE, searchtitle);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (arrSpCategories.size() == 0) {
            serviceCallCategory();
        }

        if (arrSpCountry.size() == 0) {
            serviceCallCountry();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchType = getArguments().getString(KEY_SEARCH_TITLE);
        initArrayList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        unbinder = ButterKnife.bind(this, view);
//        txtLocation.setOnItemClickListener(mAutocompleteClickListener);
//        mAdapter = new PlaceAutocompleteAdapter(getActivity(), getMainActivity().mGoogleApiClient, null,
//                null);
//        txtLocation.setAdapter(mAdapter);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user_id = prefHelper.getUser().getId();


        serviceCallCountry();
        serviceCallCategory();
        setAdapters();
        initCustomSpinner();
        initCustomSpinnerCountry();
        getMainActivity().setOnActivityResult(FilterFragment.this);
        setRangeBar();
        setSeekbarMinMaxEditextListener();
        txtPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + Utils.formatNumber(priceMax + ""));
        txtInitialPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + Utils.formatNumber(priceMin + ""));
        if (mSearchType == null) {
            edAdTitle.setEnabled(true);
        } else {
            edAdTitle.setText(mSearchType);
            edAdTitle.setEnabled(true);
        }
        txtPriceRange.setText(prefHelper.getUser().getCurrency_symbol() + "  0 - 50,000,000");

        rangeSeekbar.setMax(50000000);

        dropdownlistSelectItem = new Dropdownlist();
        dropdownlistSelectItem.setAttributeParentId(0);
        dropdownlistSelectItem.setAttributeParentValueId(0);
        dropdownlistSelectItem.setId(0);
        dropdownlistSelectItem.setValue(getString(R.string.select_type));
    }

    private void setSeekbarMinMaxEditextListener() {

        edMinPrice.setFilters(new InputFilter[]{ new InputFilterMinMax(priceMin, priceMax)});
        edMaxPrice.setFilters(new InputFilter[]{ new InputFilterMinMax(priceMin, priceMax)});

        edMinPrice.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                int value;
                if(!Utils.isEmptyOrNull(s.toString()))
                    value = Integer.parseInt(s.toString());
                else
                    value = 0;

                txtInitialPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + getFormattedNumber(Double.valueOf(value), 0));
                priceMin = value;
                rangeSeekbar.getThumb(0).setValue(priceMin);
            }
        });


        edMaxPrice.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                int value;
                if(!Utils.isEmptyOrNull(s.toString()))
                    value = Integer.parseInt(s.toString());
                else
                    value = 50000000;

                txtPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + getFormattedNumber(Double.valueOf(value), 0));
                priceMax = value;
                rangeSeekbar.getThumb(1).setValue(priceMax);
            }
        });

    }

    private void setRangeBar() {
        rangeSeekbar.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider,
                                       MultiSlider.Thumb thumb,
                                       int thumbIndex,
                                       int value) {
                if (thumbIndex == 0) {
                    txtInitialPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + getFormattedNumber(Double.valueOf(value), 0));
                    priceMin = value;
//                    if(priceMin >= 0)
//                        edMinPrice.setText(priceMin + "");
                } else {
                    txtPrice.setText(prefHelper.getUser().getCurrency_symbol()+" " + getFormattedNumber(Double.valueOf(value), 0));
                    priceMax = value;

//                    if(priceMax >= 0)
//                        edMaxPrice.setText(priceMax + "");
                }
            }
        });
    }

    private JSONArray jsonAttArray() throws JSONException {
        arrEdAtt.clear();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < adapterAttributes.getCount(); i++) {

            edtAttribute = (AnyEditTextView) listCategoriesAttributes.getChildAt(i).findViewById(R.id.edtCategory);
            spAttribute = (Spinner) listCategoriesAttributes.getChildAt(i).findViewById(R.id.spAttrCategory);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", ((Attributes) adapterAttributes.getItem(i)).getId());


            if (((Attributes) adapterAttributes.getItem(i)).getAttributeType().equals("text")) {
                jsonObject.put("value", edtAttribute.getText().toString());
            }

            if (((Attributes) adapterAttributes.getItem(i)).getAttributeType().equals("dropdown")) {
                String value = ((Dropdownlist) spAttribute.getSelectedItem()).getValue();
                if (!value.equals(getString(R.string.select_type))) {
                    jsonObject.put("value", ((Dropdownlist) spAttribute.getSelectedItem()).getId().toString());
                    jsonObject.put("selection_id", ((Dropdownlist) spAttribute.getSelectedItem()).getId());
                } else {
                    jsonObject.put("value", "");
                    jsonObject.put("selection_id", 0);
                }
            }

            jsonArray.put(jsonObject);
        }

        return jsonArray;

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Attributes entity = (Attributes) adapterCategory.getItem(i);
        AnyEditTextView edtCategory = (AnyEditTextView) view.findViewById(R.id.edtCategory);
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", entity.getId());
            obj.put("value", edtCategory.getStringTrimmed());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();


        titleBar.setHeading(getString(R.string.filter));
        titleBar.setLeftButton(R.drawable.backbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
    }

    @OnClick({R.id.txtSelectCountry, R.id.txtSelectCategory, R.id.txtSelectSubCategory, R.id.btnPostAd, R.id.txtLocation})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSelectCategory:
                spCategory.performClick();
                break;

            case R.id.txtSelectCountry:
                spCountry.performClick();
                break;

            case R.id.txtSelectSubCategory:
                spSubCategory.performClick();
                break;
            case R.id.btnPostAd:

                try {
                    arrJason = jsonAttArray();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //if (edAdTitle.getText().toString().length() > 0) {
                    postAdsCall(user_id, String.valueOf(edAdTitle.getText()), priceMax + "", priceMin + "", isFeatured, txtLocation.getText().toString(), latitude + "",
                            longitude + "", categoryID, countryID, subCategory, arrJason.toString());

//                } else {
//                    UtilHelper.showToast(getMainActivity(), getString(R.string.please_search_something));
//                    popBackStack();
//                }

                break;
            case R.id.txtLocation:
                callPlaceAutocompleteActivityIntent();
                break;
        }
    }

    /*
      Service Call

       */

    private void setAdapters() {
        spCountry.setAdapter(adapterCountry);
        spCategory.setAdapter(adapterCategory);
        spSubCategory.setAdapter(adapterSubCategory);
        listCategoriesAttributes.setAdapter(adapterAttributes);
    }

    private void initArrayList() {
        arrSpCountry = new ArrayList<>();
        arrSpCategories = new ArrayList<>();
        arrSpSubCategories = new ArrayList<>();
        arrAttributes = new ArrayList<>();
        arrEdAtt = new ArrayList<>();

//        adapterAttributes = new GenericArrayAdapter<>(getActivity(), arrAttributes, new SubAdsAttributesBinder(getMainActivity(), false, null));
        SubAdsAttributesBinderV1 baseView = new SubAdsAttributesBinderV1(getMainActivity(), null);
        baseView.setListenerNotifyDataSetChanged(new SubAdsAttributesBinderV1.NotifyDataSetChangedListener() {
            @Override
            public void notifyDataSetChanged() {
                adapterAttributes.notifyDataSetChanged();
            }
        });
        adapterAttributes = new GenericArrayAdapter<>(getActivity(), arrAttributes, baseView);


        adapterCategory = new GenericArrayAdapter<>(getMainActivity(), arrSpCategories, new CategoryBinder());
        adapterSubCategory = new GenericArrayAdapter<>(getMainActivity(), arrSpSubCategories, new SubCategoryBinder());

        //For Country
        adapterCountry = new GenericArrayAdapter<>(getMainActivity(), arrSpCountry, new SpCityBinder());
    }

    private void initCustomSpinner() {

        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                priceMin = adapterCategory.getList().get(position).getRangeMin();
                priceMax = adapterCategory.getList().get(position).getRangeMax();

                rangeSeekbar.setMin(priceMin);
                rangeSeekbar.setMax(priceMax);

                rangeSeekbar.getThumb(0).setValue(priceMin);
                rangeSeekbar.getThumb(1).setValue(priceMax);

                setSeekbarMinMaxEditextListener();



                item = arrSpCategories.get(position).getName();
                txtSelectCategory.setText(item);
//                if (arrSpCategories.get(position).getId() != null) {
                categoryID = arrSpCategories.get(position).getId();

                //if (arrSpCategories.get(position).getSubcategory() == 1) { old  => 26/02/2019
                if (arrSpCategories.get(position).getId() != 0 && arrSpCategories.get(position).getId() != 44) {
                    txtSubCategory.setVisibility(View.VISIBLE);
                    classified.setVisibility(View.VISIBLE);
                    serviceCallSubCategory(categoryID);


                } else {
                    txtSubCategory.setVisibility(View.GONE);
                    classified.setVisibility(View.GONE);
                    subCategory = 0;
                    callAttributes(categoryID, subCategory);
                }
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtSelectFeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spFeatured.performClick();
            }
        });

        spFeatured.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    isFeatured = 1;
                    txtSelectFeature.setText(getResources().getString(R.string.yes));
                } else if (position == 2) {
                    isFeatured = 0;
                    txtSelectFeature.setText(getResources().getString(R.string.no));
                } else {
                    isFeatured = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        List<String> categories = new ArrayList<String>();
        categories.add(getString(R.string.select));
        categories.add(getString(R.string.yes));
        categories.add(getString(R.string.no));
        adapterSpinner = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, categories);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFeatured.setAdapter(adapterSpinner);
        spFeatured.setSelection(0);
        txtSelectFeature.setText(getString(R.string.select));
    }

    private void initCustomSpinnerCountry() {

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                countryID = arrSpCountry.get(position).getId();
                String countryName = arrSpCountry.get(position).getTitle();
                txtSelectCountry.setText(countryName);
                txtSelectCountry.setTag(arrSpCountry.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void classifiedCategory() {

        spSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = arrSpSubCategories.get(position).getName();
                subCategory = arrSpSubCategories.get(position).getId();
                // categoryID = arrSpSubCategories.get(position).getParentId();
                txtSelectSubCategory.setText(item);
                callAttributes(categoryID, subCategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void postAdsCall(int user_id, String title, String price, String priceMin, @Nullable int featured, String area, String _latitude, String _longitude, @Nullable int categoryID,@Nullable int countryID, @Nullable int sub_category_id, String arrJason) {

        NumberFormat formatter = new DecimalFormat("#0.000000");
        _latitude = formatter.format(Double.parseDouble(_latitude));
        _longitude = formatter.format(Double.parseDouble(_longitude));

        if (isFeatured == -1) {

            btnPostAd.setEnabled(false);

            filteredv1 = WebServiceFactory.getInstance().getFilteredv1(
                    user_id,
                    0,
                    10,
                    title,
                    priceMin,
                    price,
                    area,
                    _latitude,
                    _longitude,
                    countryID,
                    categoryID,
                    sub_category_id,
                    arrJason,
                    prefHelper.getLang());


            filteredv1.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
                @Override
                public void onResponse(Call<WebResponse<ArrayList<Ad>>> call,
                                       Response<WebResponse<ArrayList<Ad>>> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }

                    if (btnPostAd != null)
                        btnPostAd.setEnabled(true);

                    if (response.body().isSuccess()) {
                        SearchFragmentv1 fragment = new SearchFragmentv1();
                        if (fragment != null) {
                            fragment = (SearchFragmentv1) getFragmentManager().findFragmentByTag("SearchFragmentv1");
                            fragment.bindFilterData(response.body().getResult(), true, filteredv1);
                        }
                        popBackStack();
                    } else {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        SearchFragmentv1 fragment = new SearchFragmentv1();
                        if (fragment != null) {
                            fragment = (SearchFragmentv1) getFragmentManager().findFragmentByTag("SearchFragmentv1");
                            fragment.bindFilterData(null, false, filteredv1);
                        }

                        popBackStack();

                    }


                }

                @Override
                public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                    if (btnPostAd != null) {
                        btnPostAd.setEnabled(true);
                    }
                    t.printStackTrace();
                }


            });
        } else {
            filteredv1 = WebServiceFactory.getInstance().getFiltered(
                    user_id,
                    0,
                    10,
                    title,
                    featured,
                    priceMin,
                    price,
                    area,
                    _latitude,
                    _longitude,
                    categoryID,
                    sub_category_id,
                    arrJason,
                    prefHelper.getLang());


            filteredv1.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
                @Override
                public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                    if (response == null || response.body() == null || response.body().getResult() == null) {
                        return;
                    }

                    btnPostAd.setEnabled(true);

                    if (response.body().isSuccess()) {

                        SearchFragmentv1 fragment = new SearchFragmentv1();
                        if (fragment != null) {
                            fragment = (SearchFragmentv1) getFragmentManager().findFragmentByTag("SearchFragmentv1");
                            fragment.bindFilterData(response.body().getResult(), true, filteredv1);
                        }
                        popBackStack();
                    } else {
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                        SearchFragmentv1 fragment = new SearchFragmentv1();
                        if (fragment != null) {
                            fragment = (SearchFragmentv1) getFragmentManager().findFragmentByTag("SearchFragmentv1");
                            fragment.bindFilterData(null, false, filteredv1);
                            popBackStack();
                        }


                    }


                }

                @Override
                public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                    if (btnPostAd != null) {
                        btnPostAd.setEnabled(true);
                    }
                    t.printStackTrace();
                }


            });
        }
    }

    public void serviceCallCountry() {


        serviceCountry = WebServiceFactory.getInstance().getCountry(prefHelper.getLang());
        serviceCountry.enqueue(new Callback<WebResponse<ArrayList<City>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<City>>> call,
                                   Response<WebResponse<ArrayList<City>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess() && isAdded()) {

                    arrSpCountry.clear();
                    arrSpCountry.addAll(response.body().getResult());
                    adapterCountry.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }


            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<City>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void serviceCallCategory() {
        serviceCategory = WebServiceFactory.getInstance().getCategory(prefHelper.getLang());
        serviceCategory.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess() && isAdded()) {
                    arrSpCategories.clear();
                    arrSpCategories.add(new Category(0, getString(R.string.select_category)));
                    arrSpCategories.addAll(response.body().getResult());
                    adapterCategory.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void callAttributes(int categoryID,
                               int subCategory) {
        serviceAttributes = WebServiceFactory.getInstance().getAttributes(
                categoryID,
                subCategory,
                prefHelper.getLang()
        );
        serviceAttributes.enqueue(new Callback<WebResponse<ArrayList<Attributes>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Attributes>>> call,
                                   Response<WebResponse<ArrayList<Attributes>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrAttributes.clear();
                    arrAttributes.addAll(response.body().getResult());
                    for (int i = 0; i < arrAttributes.size(); i++) {
                        if (arrAttributes.get(i).getAttributeType().equals("dropdown")) {
                            arrAttributes.get(i).getDropdownlist().add(0, dropdownlistSelectItem);
                        }
                    }
                    adapterAttributes.notifyDataSetChanged();
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Attributes>>> call, Throwable t) {
                t.printStackTrace();

            }

        });

    }

    public void serviceCallSubCategory(int categoryID) {
        serviceSubCategories = WebServiceFactory.getInstance().getSubCategory(categoryID, prefHelper.getLang());
        serviceSubCategories.enqueue(new Callback<WebResponse<ArrayList<Category>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Category>>> call,
                                   Response<WebResponse<ArrayList<Category>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    arrSpSubCategories.clear();
                    arrSpSubCategories.addAll(response.body().getResult());
                    adapterSubCategory.notifyDataSetChanged();
                    classifiedCategory();
                    spSubCategory.setSelection(2, true);
                    spSubCategory.setSelection(0, true);
                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Category>>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onPause() {
        if (serviceAttributes != null) {
            serviceAttributes.cancel();
        }
        if (serviceSubCategories != null) {
            serviceSubCategories.cancel();
        }
        if (serviceCountry != null) {
            serviceCountry.cancel();
        }

        if (serviceCategory != null) {
            serviceCategory.cancel();
        }
        if (filteredv1 != null) {
            filteredv1.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceAttributes != null) {
            serviceAttributes.cancel();
        }
        if (serviceSubCategories != null) {
            serviceSubCategories.cancel();
        }
        if (serviceCountry != null) {
            serviceCountry.cancel();
        }
        if (serviceCategory != null) {
            serviceCategory.cancel();
        }
        if (filteredv1 != null) {
            filteredv1.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = PlaceAutocomplete.getPlace(getMainActivity(), data);
//                locationName = place.getName().toString();
//                latitude = place.getLatLng().latitude;
//                longitude = place.getLatLng().longitude;
//                txtLocation.setText(locationName);
//                Log.i(TAG, "Place:" + place.toString());
//            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                Status status = PlaceAutocomplete.getStatus(getMainActivity(), data);
//                Log.i(TAG, status.getStatusMessage());
//            } else if (requestCode == RESULT_CANCELED) {
//
//            }
//        }
    }

    private void callPlaceAutocompleteActivityIntent() {

    }


    public class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}

