package com.wamztech.adhouse.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AdsListItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClassifiedListingFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {


    int classifiedId = -1;
    String title;
    int isLike;
    private ListView lvSearchResult;
    private GenericArrayAdapter<Ad> adapter;
    private ArrayList<Ad> arrClasifiedAds;
    private Call<WebResponse<ArrayList<Ad>>> serviceGetClassifiedSubAds;
    private Call<WebResponse<ArrayList<Ad>>> serviceGetClassifiedAds;
    private Call<WebResponse<Ad>> serviceAddFavourite;

    public static ClassifiedListingFragment newInstance(int classifiedId, String title) {
        ClassifiedListingFragment fragment = new ClassifiedListingFragment();
        fragment.classifiedId = classifiedId;
        fragment.title = title;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrClasifiedAds = new ArrayList<>();
        dataSet();
    }

    private void dataSet() {
        adapter = new GenericArrayAdapter<>(getActivity(), arrClasifiedAds, new AdsListItemBinder(this));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_listing, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);

        lvSearchResult.setAdapter(adapter);
        if (classifiedId == -1) {
            serviceCallClassifiedAds();
        } else {
            serviceCallSubClassifiedAds();
        }
        lvSearchResult.setOnItemClickListener(this);
    }

    private void initViews(View view) {
        lvSearchResult = (ListView) view.findViewById(R.id.lvProductListing);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Ad entity = ((Ad) adapter.getItem(position));

        addDockableFragment(DetailFragment.newInstance(entity));

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        if (classifiedId == -1) {
            titleBar.setHeading(getContext().getString(R.string.classified));
        } else {
            titleBar.setHeading(title);
        }
        titleBar.showBackButton(getMainActivity());
        titleBar.setRightButton(R.drawable.searchbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDockableFragment(SearchFragmentv1.newInstance());
            }
        });

        titleBar.setTitleBarBoldAndRed();
    }

    public void serviceCallSubClassifiedAds() {
        serviceGetClassifiedSubAds = WebServiceFactory.getInstance().getClassifiedSubAds(prefHelper.getUser().getId(), classifiedId, prefHelper.getLang());
        serviceGetClassifiedSubAds.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapter.clear();
                    adapter.addAll(response.body().getResult());
                    adapter.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }


        });

    }

    public void serviceCallClassifiedAds() {
        serviceGetClassifiedAds = WebServiceFactory.getInstance().getClassifiedAds(prefHelper.getUser().getId(), prefHelper.getLang());
        serviceGetClassifiedAds.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }

                if (response.body().isSuccess()) {
                    adapter.clear();
                    adapter.addAll(response.body().getResult());
                    adapter.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }

            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
            }


        });

    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.imgFavourite: {
                final ImageView imgFavourite = (ImageView) v;
                final int position = (int) v.getTag();
                final Ad item = (Ad) adapter.getItem(position);
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.guest_user));
                    return;
                } else {
                    if (item.isFavourite()) {
                        isLike = 0;

                    } else {
                        isLike = 1;
                    }

                    imgFavourite.setEnabled(false);
                    serviceAddFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), item.getId(), isLike);
                    serviceAddFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                        @Override
                        public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                            imgFavourite.setEnabled(true);
                            if (response == null || response.body() == null || response.body().getResult() == null) {
                                return;
                            }
                            if (response.body().isSuccess()) {


                                if (response.body().getResult().isFavourite()) {
                                    imgFavourite.setImageResource(R.drawable.listheart);
                                    item.setIsLike(isLike);

                                } else {

                                    imgFavourite.setImageResource(R.drawable.heartfilled);
                                    item.setIsLike(isLike);

                                }

                                adapter.notifyDataSetChanged();
                            }

                            UtilHelper.showToast(getMainActivity(), response.body().getMessage());


                        }

                        @Override
                        public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                            if (imgFavourite != null) {
                                imgFavourite.setEnabled(true);
                            }
                            t.printStackTrace();
                        }
                    });
                }
            }
            break;
        }

    }

    @Override
    public void onPause() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetClassifiedAds != null) {
            serviceGetClassifiedAds.cancel();
        }
        if (serviceGetClassifiedSubAds != null) {
            serviceGetClassifiedSubAds.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetClassifiedAds != null) {
            serviceGetClassifiedAds.cancel();
        }
        if (serviceGetClassifiedSubAds != null) {
            serviceGetClassifiedSubAds.cancel();
        }
        super.onDestroyView();
    }
}

