package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.models.Cms;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AboutFragmenrt extends BaseFragment {

    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.txtAbout)
    AnyTextView txtAbout;
    Unbinder unbinder;

    public static AboutFragmenrt newInstance() {

        Bundle args = new Bundle();

        AboutFragmenrt fragment = new AboutFragmenrt();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHeading("Generic Fragment");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setText();
        listners();
    }


    private void listners() {

    }

    private void setText() {

        serviceCallCms();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getContext().getString(R.string.about));
        titleBar.showBackButton(getMainActivity());
    }


    @Override
    public void onPause() {
        if (serviceGetCms != null) {
            serviceGetCms.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceGetCms != null) {
            serviceGetCms.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    private Call<WebResponse<Cms>> serviceGetCms;
    public void serviceCallCms() {
//        prefHelper.getLang().equals("ar")
        serviceGetCms = WebServiceFactory.getInstance().getCms("about",prefHelper.getLang());
        serviceGetCms.enqueue(new Callback<WebResponse<Cms>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Cms>> call, Response<WebResponse<Cms>> response) {
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {

                            txtAbout.setText((Html.fromHtml(response.body().getResult().getContent())));
                        }

//                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());


                    }

                    @Override
                    public void onFailure(Call<WebResponse<Cms>> call, Throwable t) {
                        t.printStackTrace();
                        //UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.no_internet_connection));
                    }
                });
    }
}
