package com.wamztech.adhouse.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.retrofit.WebResponse;
import com.wamztech.adhouse.retrofit.WebServiceFactory;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.wamztech.adhouse.ui.views.viewbinders.AdsListItemBinder;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PropertesListingFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    @BindView(R.id.lvProductListing)
    ListView lvProductListing;
    Unbinder unbinder;
    private GenericArrayAdapter<Ad> adapter;
    private ArrayList<Ad> arrPropertyAds;
    private Call<WebResponse<ArrayList<Ad>>> serviceGetPropertiesAds;
    private Call<WebResponse<Ad>> serviceAddFavourite;
    private int isLike;

    public static PropertesListingFragment newInstance() {
        Bundle args = new Bundle();
        PropertesListingFragment fragment = new PropertesListingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrPropertyAds = new ArrayList<>();
        dataSet();
    }

    private void dataSet() {
        adapter = new GenericArrayAdapter<>(getActivity(), arrPropertyAds, new AdsListItemBinder(this));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvProductListing.setAdapter(adapter);
        serviceCallPropertiesAds();
        lvProductListing.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Ad product = ((Ad) lvProductListing.getAdapter().getItem(position));
        addDockableFragment(DetailFragment.newInstance(product));
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading(getString(R.string.properties));
        titleBar.showBackButton(getMainActivity());
        titleBar.setRightButton(R.drawable.searchbtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDockableFragment(SearchFragmentv1.newInstance());
            }
        });
        titleBar.setTitleBarBoldAndRed();
    }

    public void serviceCallPropertiesAds() {
        serviceGetPropertiesAds = WebServiceFactory.getInstance().getPropertiesAds(prefHelper.getUser().getId(), prefHelper.getLang());
        serviceGetPropertiesAds.enqueue(new Callback<WebResponse<ArrayList<Ad>>>() {
            @Override
            public void onResponse(Call<WebResponse<ArrayList<Ad>>> call, Response<WebResponse<ArrayList<Ad>>> response) {
                if (response == null || response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().isSuccess()) {
                    adapter.clear();
                    adapter.addAll(response.body().getResult());
                    adapter.notifyDataSetChanged();

                } else {
                    UtilHelper.showToast(getMainActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ArrayList<Ad>>> call, Throwable t) {
                t.printStackTrace();
                UtilHelper.showToast(getMainActivity(), "No internet connection");
            }
        });
    }

    @Override
    public void onPause() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetPropertiesAds != null) {
            serviceGetPropertiesAds.cancel();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (serviceAddFavourite != null) {
            serviceAddFavourite.cancel();
        }
        if (serviceGetPropertiesAds != null) {
            serviceGetPropertiesAds.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.imgFavourite: {
                final ImageView imgFavourite = (ImageView) v;
                final int position = (int) v.getTag();
                final Ad item = (Ad) adapter.getItem(position);
                if (prefHelper.getUser().isGuestUser()) {
                    UtilHelper.showToast(getMainActivity(), getResources().getString(R.string.guest_user));
                    return;
                }
                if (item.isFavourite()) {
                    isLike = 0;

                } else {
                    isLike = 1;
                }

                imgFavourite.setEnabled(false);
                serviceAddFavourite = WebServiceFactory.getInstance().addFavourite(prefHelper.getUser().getId(), item.getId(), isLike);
                serviceAddFavourite.enqueue(new Callback<WebResponse<Ad>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Ad>> call, Response<WebResponse<Ad>> response) {
                        imgFavourite.setEnabled(true);
                        if (response == null || response.body() == null || response.body().getResult() == null) {
                            return;
                        }
                        if (response.body().isSuccess()) {
                            if (response.body().getResult().isFavourite()) {
                                imgFavourite.setImageResource(R.drawable.listheart);
                                item.setIsLike(isLike);
                            } else {
                                imgFavourite.setImageResource(R.drawable.heartfilled);
                                item.setIsLike(isLike);
                            }
                        }
                        adapter.notifyDataSetChanged();
                        UtilHelper.showToast(getMainActivity(), response.body().getMessage());
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Ad>> call, Throwable t) {
                        if (imgFavourite != null) {
                            imgFavourite.setEnabled(true);
                        }
                        t.printStackTrace();
                    }
                });
            }
            break;
        }
    }
}

