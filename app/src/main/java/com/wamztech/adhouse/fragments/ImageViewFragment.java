package com.wamztech.adhouse.fragments;

import android.os.Bundle;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.ZoomPagerAdapter;
import com.wamztech.adhouse.fragments.abstracts.BaseFragment;
import com.wamztech.adhouse.models.AdImages;
import com.wamztech.adhouse.ui.views.TitleBar;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ImageViewFragment extends BaseFragment implements View.OnClickListener {


    public static final String KEY_OBJECT = "key-dataaa";
    public static final String KEY_POSITION = "key-dataaa-positions";
    Unbinder unbinder;
    @BindView(R.id.viewPagerImages)
    ViewPager viewPagerImages;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;

     private int position ;
    private ArrayList<AdImages> entityAds;
    private ZoomPagerAdapter adapterZoomPager;

    public static ImageViewFragment newInstance(int position , ArrayList<AdImages> product) {

        Bundle args = new Bundle();
        args.putString(KEY_OBJECT, new Gson().toJson(product)); // Convert Product object to JSON string
        args.putInt(KEY_POSITION, position);


        ImageViewFragment fragment = new ImageViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entityAds = new Gson().fromJson(getArguments().getString(KEY_OBJECT), new TypeToken<ArrayList<AdImages>>(){}.getType());
        position = getArguments().getInt(KEY_POSITION);

        dataSet();
    }

    private void dataSet() {
        adapterZoomPager = new ZoomPagerAdapter(getActivity(), new ArrayList<AdImages>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_full_image_view, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//         int position = (int) view.getTag();



        adapterZoomPager.clear();
        adapterZoomPager.addAll(entityAds);
        viewPagerImages.setAdapter(adapterZoomPager);

        indicator.setViewPager(viewPagerImages);
        indicator.setSnap(true);



        viewPagerImages.setCurrentItem(position);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        titleBar.resetTitleBar();
        titleBar.setHeading("Preview");
        titleBar.showBackButton(getMainActivity());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onClick(View v) {

    }
}
