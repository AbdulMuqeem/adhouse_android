package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.ui.views.AnyTextView;


public class MyFavouriteItemBinder extends BaseView<Ad> {

    private View.OnClickListener onClickListener;

    public MyFavouriteItemBinder(View.OnClickListener onClickListener) {
        super(R.layout.item_my_fav);
        this.onClickListener = onClickListener;
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(final Ad model, final int position, int grpPosition, View view, Context context) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        ImageLoader.getInstance().displayImage(model.getAdImages().get(0).getImageUrl(), viewHolder.imgResult);
        viewHolder.txtName.setText(model.getTitle());
        viewHolder.txtPrice.setText(model.getPrice());

        switch (model.getType()) {
            case Ad.TYPE_VEHICLE: {
                viewHolder.txtLoc.setText("km " + model.getKilometer());
                viewHolder.txtLoc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            break;

            case Ad.TYPE_PROPERTIES:
            case Ad.TYPE_CLASSIFIED: {
                viewHolder.txtLoc.setText(model.getArea());
                viewHolder.txtLoc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location, 0, 0, 0);
            }
            break;

        }
        if (model.isFavourite()) {
            viewHolder.imgFavourit.setImageResource(R.drawable.heartfilled);
        } else {
            viewHolder.imgFavourit.setImageResource(R.drawable.listheart);
        }


        viewHolder.imgFavourit.setOnClickListener(onClickListener);
        viewHolder.imgFavourit.setTag(position);
        viewHolder.btnViewDetails.setOnClickListener(onClickListener);
        viewHolder.btnViewDetails.setTag(position);

    }


    public class ViewHolder extends GenericViewHolder {
        ImageView imgResult;
        AnyTextView txtName;
        AnyTextView txtPrice;
        AnyTextView txtLoc;
        ImageView imgFavourit;
        Button btnViewDetails;

        public ViewHolder(View view) {
            imgResult = (ImageView) view.findViewById(R.id.imgProduct);
            imgFavourit = (ImageView) view.findViewById(R.id.imgFavourit);
            txtName = (AnyTextView) view.findViewById(R.id.txtName);
            txtPrice = (AnyTextView) view.findViewById(R.id.txtPrice);
            txtLoc = (AnyTextView) view.findViewById(R.id.txtLocation);
            btnViewDetails = (Button) view.findViewById(R.id.btnViewDetails);
        }
    }
}
