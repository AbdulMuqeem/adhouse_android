package com.wamztech.adhouse.ui.views;

import android.content.Context;
import android.util.AttributeSet;

import com.ctrlplusz.anytextview.Util;

import androidx.appcompat.widget.AppCompatButton;

public class AnyButton extends AppCompatButton {

    public AnyButton(Context context) {
        super(context);
    }

    public AnyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Util.setTypeface(attrs, this);
    }

    public AnyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Util.setTypeface(attrs, this);
    }
}
