package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.helpers.BasePreferenceHelper;
import com.wamztech.adhouse.models.Subscription;
import com.wamztech.adhouse.ui.views.AnyTextView;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;



public class SubscriptionBinder extends BaseView<Subscription> {

    private int TextSizeQAR;
    private int TextSizemonthNumber;
    private int TextSizemonth;
    boolean isScreenSmall;

    BasePreferenceHelper basePreferenceHelper;

    public SubscriptionBinder(boolean isScreenSmall) {
        super(R.layout.subscription_grid_item);
        this.isScreenSmall = isScreenSmall;
    }

    @Override
    protected BaseView.GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(final Subscription model, final int position, int grpPosition, View view, Context context) {


        basePreferenceHelper = new BasePreferenceHelper(context);
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        TextSizemonthNumber = context.getResources().getDimensionPixelSize(R.dimen.x20);
        TextSizemonth = context.getResources().getDimensionPixelSize(R.dimen.x14);
        TextSizeQAR = context.getResources().getDimensionPixelSize(R.dimen.x12);

        viewHolder.txtMonth.setText(model.get_package());
        viewHolder.txtInputFreeAds.setText(model.getFreeAds());
        viewHolder.txtInputAdsValidity.setText(model.getAdValidity() + " days");
        viewHolder.txtInputMaxAds.setText(model.getMaxAds());
        viewHolder.txtMonth.setText(model.getPrice());

        if (model.isPopular()) {
//            viewHolder.txtMonth.setTextColor(context.getResources().getColor(R.color.blue));
//            viewHolder.txtPopular.setVisibility(View.VISIBLE);
//            viewHolder.llContent.setBackgroundResource(R.drawable.rectangle_borders_blue);
//            viewHolder.llContent.setPadding(
//                    context.getResources().getDimensionPixelSize(R.dimen.x5),
//                    context.getResources().getDimensionPixelSize(R.dimen.x10),
//                    context.getResources().getDimensionPixelSize(R.dimen.x5),
//                    0
//            );

        } else {
            viewHolder.txtMonth.setTextColor(context.getResources().getColor(R.color.graish));
            viewHolder.llContent.setBackgroundResource(R.drawable.rectangle_borders);
            viewHolder.txtPopular.setVisibility(View.INVISIBLE);

            viewHolder.llContent.setPadding(
                    context.getResources().getDimensionPixelSize(R.dimen.x1),
                    context.getResources().getDimensionPixelSize(R.dimen.x10),
                    context.getResources().getDimensionPixelSize(R.dimen.x1),
                    0
            );
        }

        if(isScreenSmall)
        {
            viewHolder.txtInputFreeAds.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
            viewHolder.txtInputAdsValidity.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
            viewHolder.txtInputMaxAds.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
            viewHolder.txtLabelFreeAds.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
            viewHolder.txtLabelMaxAds.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
            viewHolder.txtLabelAdsValidity.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.x6));
        }

        String strMonthNumber = model.get_package();
        String strMonth = context.getString(R.string.months);
        String strQAR =  basePreferenceHelper.getUser().getCurrency_symbol()+" "+ model.getPrice() + context.getString(R.string.mth);

        SpannableString span1 = new SpannableString(strMonthNumber);

        span1.setSpan(new AbsoluteSizeSpan(TextSizemonthNumber), 0, strMonthNumber.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(strMonth);
        span2.setSpan(new AbsoluteSizeSpan(TextSizemonth), 0, strMonth.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span3 = new SpannableString(strQAR);
        span3.setSpan(new AbsoluteSizeSpan(TextSizeQAR), 0, strQAR.length(), SPAN_INCLUSIVE_INCLUSIVE);

        CharSequence finalText = TextUtils.concat(span1, "\n", span2, "\n", span3);

        viewHolder.txtMonth.setText(finalText);
    }

    public class ViewHolder extends BaseView.GenericViewHolder {
        AnyTextView txtMonth;
        AnyTextView txtInputFreeAds;
        AnyTextView txtInputAdsValidity;
        AnyTextView txtInputMaxAds;
        AnyTextView txtLabelAdsValidity;
        AnyTextView txtLabelFreeAds;
        AnyTextView txtLabelMaxAds;
        AnyTextView txtPopular;
        LinearLayout llContent;

        public ViewHolder(View view) {
            txtMonth = (AnyTextView) view.findViewById(R.id.txtMonth);
            txtLabelFreeAds = (AnyTextView) view.findViewById(R.id.txtLabelFreeAds);
            txtLabelMaxAds = (AnyTextView) view.findViewById(R.id.txtLabelMaxAds);
            txtLabelAdsValidity = (AnyTextView) view.findViewById(R.id.txtLabelAdsValidity);
            txtInputFreeAds = (AnyTextView) view.findViewById(R.id.txtInputFreeAds);
            txtInputAdsValidity = (AnyTextView) view.findViewById(R.id.txtInputAdsValidity);
            txtInputMaxAds = (AnyTextView) view.findViewById(R.id.txtInputMaxAds);
            txtPopular = (AnyTextView) view.findViewById(R.id.txtPopular);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
        }
    }
}
