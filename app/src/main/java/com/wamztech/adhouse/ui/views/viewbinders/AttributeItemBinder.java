package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.AdDetail;
import com.wamztech.adhouse.ui.views.AnyTextView;


public class AttributeItemBinder extends BaseView<AdDetail> {


    public AttributeItemBinder() {
        super(R.layout.item_product);
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(AdDetail model, int position, int grpPosition, View view, Context context) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtLabel.setText(model.getAttributeTitle());
        viewHolder.txtInput.setText(model.getAttributeValue());
    }


    public class ViewHolder extends GenericViewHolder {
        AnyTextView txtLabel;
        AnyTextView txtInput;

        public ViewHolder(View view) {
            txtLabel = (AnyTextView) view.findViewById(R.id.txtLabel);
            txtInput = (AnyTextView) view.findViewById(R.id.txtInput);
        }
    }
}
