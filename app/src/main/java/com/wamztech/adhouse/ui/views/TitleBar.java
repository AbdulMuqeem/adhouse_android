package com.wamztech.adhouse.ui.views;

import android.content.Context;
import android.graphics.Typeface;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wamztech.adhouse.R;

import com.wamztech.adhouse.activities.MainActivity;

import androidx.core.content.ContextCompat;


public class TitleBar extends LinearLayout {

    ImageView btnLeft;

    public ImageView getBtnRight() {
        return btnRight;
    }

    ImageView btnRight;
    ImageView btnRightTwo;
    TextView txtHeader;


    Context context;

    public TitleBar(Context context) {
        super(context);
        this.context = context;

        initViews();

    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        initViews();
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        initViews();
    }

    public void initViews() {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.titlebar, this);


        btnLeft = (ImageView) this.findViewById(R.id.btnLeft);
        txtHeader = (TextView) this.findViewById(R.id.txtHeader);
        btnRight = (ImageView) this.findViewById(R.id.btnRight);
        btnRightTwo = (ImageView) this.findViewById(R.id.btnRightTwo);


    }

    public void setHeading(String text) {
        txtHeader.setText(text);

    }

    public void restHeading(){

        Typeface regularTypeFace = Typeface.createFromAsset(getContext().getAssets(),"fonts/"+ getContext().getString(R.string.font_regular));
        txtHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.graish));
        txtHeader.setTypeface(regularTypeFace);
    }



    public void setTitleBarBoldAndRed() {
        Typeface regularTypeFace = Typeface.createFromAsset(getContext().getAssets(),"fonts/"+ getContext().getString(R.string.font_bold));
        txtHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
        txtHeader.setTypeface(regularTypeFace);
    }


    public void hideTitleBar() {
        this.setVisibility(View.GONE);
    }

    public void showBackButton(final MainActivity context) {
        btnLeft.setVisibility(VISIBLE);
        btnLeft.setImageResource(R.drawable.backbtn);
        btnLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                context.getSupportFragmentManager().popBackStack();
            }
        });


    }

    public void resetTitleBar() {

        this.setVisibility(View.VISIBLE);
        btnLeft.setVisibility(View.GONE);
        btnRight.setVisibility(View.GONE);
        btnRightTwo.setVisibility(View.GONE);


        restHeading();

    }

    public void showLeftButton() {
        btnLeft.setVisibility(View.VISIBLE);
    }

    public void setLeftButton(int drawable, OnClickListener listener) {
        btnLeft.setImageResource(drawable);
        btnLeft.setOnClickListener(listener);
        btnLeft.setVisibility(View.VISIBLE);
    }


    public void setRightButton(int drawable, OnClickListener listener) {
        btnRight.setImageResource(drawable);
        btnRight.setOnClickListener(listener);
        btnRight.setVisibility(View.VISIBLE);
    }

    public void setRightTwoButton(int drawable, OnClickListener listener) {
        btnRightTwo.setImageResource(drawable);
        btnRightTwo.setOnClickListener(listener);
        btnRightTwo.setVisibility(View.VISIBLE);
    }

}



