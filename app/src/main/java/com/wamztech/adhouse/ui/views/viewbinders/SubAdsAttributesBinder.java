package com.wamztech.adhouse.ui.views.viewbinders;

import android.app.Activity;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.helpers.UtilHelper;
import com.wamztech.adhouse.models.Attributes;
import com.wamztech.adhouse.models.Dropdownlist;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.SpinnerPlus;

import java.util.ArrayList;


public class SubAdsAttributesBinder extends BaseView<Attributes> {


    private final Activity activity;
    private final Boolean isFromUpdate;

    private final ArrayList<String> attValue;
    private String item;
    private ArrayList<Dropdownlist> arrSpAttr;
    private GenericArrayAdapter<Dropdownlist> adpSp;
    private boolean isSelected = false;

    public SubAdsAttributesBinder(Activity activity, Boolean isFromUpdate, ArrayList<String> attValue) {
        super(R.layout.item_submit_ads);
        this.activity = activity;
        this.isFromUpdate = isFromUpdate;
        this.attValue = attValue;
        arrSpAttr = new ArrayList<>();
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }


    @Override
    public void getView(final Attributes entity, final int position, int grpPosition, View view, final Context context) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (isFromUpdate) {
            if (entity.getAttributeType().equals("text")) {
                viewHolder.txtEdCategoryTitle.setText(entity.getAttributeName());
                viewHolder.llEdttxt.setVisibility(View.VISIBLE);
                viewHolder.llSp.setVisibility(View.GONE);
                if (entity.getKeyboardType().equals("numpad")) {
                    viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_NUMBER |
                            InputType.TYPE_CLASS_NUMBER);
                } else if (entity.getKeyboardType().equals("alphapad")) {
                    viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_CLASS_TEXT);
                }
            } else {
                viewHolder.llEdttxt.setVisibility(View.GONE);
                viewHolder.llSp.setVisibility(View.VISIBLE);
                viewHolder.txtSpinner.setText(attValue.get(position));
                setSpValue(viewHolder);
                viewHolder.txtSpCategoryTitle.setText(entity.getAttributeName());
                arrSpAttr.clear();
                arrSpAttr.addAll(entity.getDropdownlist());
                adpSp = new GenericArrayAdapter<>(activity, arrSpAttr, new DropDownBinder());
                viewHolder.spAttrCategory.setAdapter(adpSp);
                viewHolder.txtSpinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isSelected = true;
                        viewHolder.spAttrCategory.performClick();
                    }
                });

                viewHolder.spAttrCategory.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        item = arrSpAttr.get(position).getValue();
                        viewHolder.txtSpinner.setText(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        UtilHelper.showToast(context, "nothing");
                    }
                });
            }
        } else {
            if (entity.getAttributeType().equals("text")) {
                viewHolder.txtEdCategoryTitle.setText(entity.getAttributeName());
                viewHolder.edtCategory.setHint(entity.getAttributeName());
                viewHolder.llEdttxt.setVisibility(View.VISIBLE);
                viewHolder.llSp.setVisibility(View.GONE);
                if (entity.getKeyboardType().equals("numpad")) {
                    viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_NUMBER |
                            InputType.TYPE_CLASS_NUMBER);

                } else if (entity.getKeyboardType().equals("alphapad")) {
                    viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_CLASS_TEXT);

                }

            } else {
                viewHolder.llEdttxt.setVisibility(View.GONE);
                viewHolder.llSp.setVisibility(View.VISIBLE);
                viewHolder.txtSpCategoryTitle.setText(entity.getAttributeName());
                viewHolder.txtSpinner.setText(entity.getAttributeName());
                arrSpAttr.clear();
                arrSpAttr.addAll(entity.getDropdownlist());
                setSpValue(viewHolder);
                adpSp = new GenericArrayAdapter<>(activity, arrSpAttr, new DropDownBinder());
                viewHolder.spAttrCategory.setAdapter(adpSp);
                viewHolder.txtSpinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewHolder.spAttrCategory.performClick();

                    }
                });
            }
        }
    }

    private void setSpValue(final ViewHolder viewHolder) {

        viewHolder.spAttrCategory.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = arrSpAttr.get(position).getValue();
                viewHolder.txtSpinner.setText(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    public class ViewHolder extends GenericViewHolder {

        AnyTextView txtEdCategoryTitle;
        AnyEditTextView edtCategory;
        AnyTextView txtSpCategoryTitle;
        AnyTextView txtSpinner;
        SpinnerPlus spAttrCategory;
        LinearLayout llEdttxt;
        LinearLayout llSp;

        public ViewHolder(View view) {
            txtEdCategoryTitle = (AnyTextView) view.findViewById(R.id.txtEdCategoryTitle);
            txtSpCategoryTitle = (AnyTextView) view.findViewById(R.id.txtSpCategoryTitle);
            txtSpinner = (AnyTextView) view.findViewById(R.id.txtSpinnerCat);
            edtCategory = (AnyEditTextView) view.findViewById(R.id.edtCategory);
            spAttrCategory = (SpinnerPlus) view.findViewById(R.id.spAttrCategory);
            llEdttxt = (LinearLayout) view.findViewById(R.id.llEdttxt);
            llSp = (LinearLayout) view.findViewById(R.id.llSp);
        }
    }
}
