package com.wamztech.adhouse.ui.views;

import android.content.Context;

import android.util.AttributeSet;

import com.ctrlplusz.anytextview.Util;

import androidx.appcompat.widget.AppCompatTextView;

public class AnyTextView extends AppCompatTextView {



    public AnyTextView(Context context) {
        super(context);
    }

    public AnyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Util.setTypeface(attrs, this);
    }

    public AnyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Util.setTypeface(attrs, this);
    }



    public String getStringTrimmed(){
        return  getText().toString().trim() ;
    }
}
