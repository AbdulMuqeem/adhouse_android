package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.Notification;
import com.wamztech.adhouse.ui.views.AnyTextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class NotificationItemBinder extends BaseView<Notification> {

    public NotificationItemBinder() {
        super(R.layout.item_notification);
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(Notification model, int position, int grpPosition, View view, Context context) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtNotification.setText( model.getTitle());
        viewHolder.txtDetail.setText( model.getMessage() );

        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(model.getCreatedAt());
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat df2 = new SimpleDateFormat("hh:mm a");
            df2.setTimeZone(TimeZone.getDefault());
            viewHolder.txtTime.setText("" + df2.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class ViewHolder extends GenericViewHolder {
        AnyTextView txtNotification;
        AnyTextView txtTime;
        AnyTextView txtDetail;

        public ViewHolder(View view) {
            txtNotification = (AnyTextView) view.findViewById(R.id.txtNotification);
            txtTime = (AnyTextView) view.findViewById(R.id.txtTime);
            txtDetail = (AnyTextView) view.findViewById(R.id.txtDetail);
        }
    }
}
