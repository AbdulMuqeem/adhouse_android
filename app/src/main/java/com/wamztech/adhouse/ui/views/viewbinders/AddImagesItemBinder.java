package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.interfaces.OnItemDelete;


public class AddImagesItemBinder extends BaseView<String> {

    private OnItemDelete onItemDelete;

    public AddImagesItemBinder(OnItemDelete onItemDelete) {
        super(R.layout.item_addimage);
        this.onItemDelete = onItemDelete;
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(String model, final int position, int grpPosition, View view, Context context) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        ImageLoader.getInstance().displayImage(model, viewHolder.imgAddProduct);
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemDelete.itemDelect(position);
            }
        });
    }


    public class ViewHolder extends GenericViewHolder {
        ImageView imgAddProduct;
        ImageView imgDelete;

        public ViewHolder(View view) {
            imgAddProduct = (ImageView) view.findViewById(R.id.imgAddProduct);
            imgDelete = (ImageView) view.findViewById(R.id.imgDelete);
        }
    }
}
