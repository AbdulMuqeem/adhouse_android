package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.Category;
import com.wamztech.adhouse.ui.views.AnyTextView;


public class AutoCompleteBinder extends BaseView<Category> {

    public AutoCompleteBinder() {
        super(R.layout.item_text);
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }


    @Override
    public void getView(Category model, int position, int grpPosition, View view, Context context) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtSelectCity.setText(model.getName() + "in Category");
    }


    public class ViewHolder extends GenericViewHolder {
        AnyTextView txtSelectCity;

        public ViewHolder(View view) {
            txtSelectCity = (AnyTextView) view.findViewById(R.id.txtSelectCity);
        }
    }
}
