package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.ui.views.AnyTextView;

import java.text.NumberFormat;


public class MyAdsItemBinder extends BaseView<Ad> {

    private View.OnClickListener onClickListener;

    public MyAdsItemBinder(View.OnClickListener onClickListener) {
        super(R.layout.item_my_ads);
        this.onClickListener = onClickListener;
    }

    @Override
    protected BaseView.GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(final Ad model, final int position, int grpPosition, View view, Context context) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        if (model.getAdImages().size() > 0) {
            ImageLoader.getInstance().displayImage(model.getAdImages().get(0).getImageUrl(), viewHolder.imgResult);
        }

        viewHolder.txtName.setText(model.getTitle());
        viewHolder.txtPrice.setText(getFormattedNumber(Double.parseDouble(model.getPrice()), 0) + " "+ model.getCurrency_symbol());
        if (model.getIsVehicle() == 1) {
            viewHolder.txtKilometer.setText("km " + model.getKilometer());
            viewHolder.txtLocation.setVisibility(View.GONE);
            viewHolder.txtKilometer.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtLocation.setText(model.getArea());
            viewHolder.txtKilometer.setVisibility(View.GONE);
            viewHolder.txtLocation.setVisibility(View.VISIBLE);
        }

        viewHolder.btnEdit.setOnClickListener(onClickListener);
        viewHolder.btnEdit.setTag(position);
        viewHolder.btnDelete.setOnClickListener(onClickListener);
        viewHolder.btnDelete.setTag(position);

    }

    public String getFormattedNumber(double value, int factor) {
        NumberFormat format1 = NumberFormat.getInstance();
        format1.setMaximumFractionDigits(factor);
        format1.setMinimumFractionDigits(factor);
        return format1.format(value);
    }

    public class ViewHolder extends BaseView.GenericViewHolder {
        ImageView imgResult;
        AnyTextView txtName;
        AnyTextView txtPrice;
        AnyTextView txtKilometer;
        AnyTextView txtLocation;

        Button btnEdit;
        Button btnDelete;

        public ViewHolder(View view) {
            imgResult = (ImageView) view.findViewById(R.id.imgProduct);
            txtName = (AnyTextView) view.findViewById(R.id.txtName);
            txtPrice = (AnyTextView) view.findViewById(R.id.txtPrice);
            txtKilometer = (AnyTextView) view.findViewById(R.id.txtKilometer);
            txtLocation = (AnyTextView) view.findViewById(R.id.txtLocation);
            btnEdit = (Button) view.findViewById(R.id.btnEdit);
            btnDelete = (Button) view.findViewById(R.id.btnDelete);
        }
    }
}
