package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.helpers.Utils;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.models.User;
import com.wamztech.adhouse.ui.views.AnyTextView;


public class AdsListItemBinder extends BaseView<Ad> {

    private View.OnClickListener onClickListener;
    private User user;

    public AdsListItemBinder(View.OnClickListener onClickListener) {
        super(R.layout.product_item);
        this.onClickListener = onClickListener;
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(final Ad model, int position, int grpPosition, View view, Context context) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        if (model.getAdImages().size() > 0) {
            ImageLoader.getInstance().displayImage(model.getAdImages().get(0).getImageUrl(), viewHolder.imgResult);
        }
        viewHolder.txtName.setText(model.getTitle());
        viewHolder.txtPrice.setText(model.getCurrency_symbol()+ " " + Utils.formatNumber(model.getPrice()));


        switch (model.getType()) {
            case Ad.TYPE_VEHICLE: {
                viewHolder.txtKilometerandLocation.setText("km " + Utils.formatNumber(model.getKilometer()));
                viewHolder.txtKilometerandLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                viewHolder.txtCarType.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                viewHolder.txtCarMake.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                viewHolder.txtCarType.setVisibility(View.INVISIBLE);
                viewHolder.txtCarMake.setVisibility(View.INVISIBLE);

                for (int i = 0; i < model.getAdDetails().size(); i++) {
                    // Car Class
                    if (model.getAdDetails().get(i).getAttributeId() == 4) {
                        viewHolder.txtCarType.setText(model.getAdDetails().get(i).getAttributeValue());
                        viewHolder.txtCarType.setVisibility(View.VISIBLE);
                    }

                    // Car Make
                    if (model.getAdDetails().get(i).getAttributeId() == 3) {
                        viewHolder.txtCarMake.setText(model.getAdDetails().get(i).getAttributeValue());
                        viewHolder.txtCarMake.setVisibility(View.VISIBLE);
                    }
                }
            }
            break;

            case Ad.TYPE_PROPERTIES:
                if(model.getAdDetails().size() >= 1){
                    viewHolder.txtKilometerandLocation.setText(model.getAdDetails().get(model.getAdDetails().size() - 1).getAttributeValue());
                }else{
                    viewHolder.txtKilometerandLocation.setText("N/A");
                }
                //viewHolder.txtKilometerandLocation.setText(model.getArea());
                viewHolder.txtKilometerandLocation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location, 0, 0, 0);
                viewHolder.txtCarType.setVisibility(View.INVISIBLE);
                viewHolder.txtCarMake.setVisibility(View.INVISIBLE);
                break;
            case Ad.TYPE_CLASSIFIED: {
                viewHolder.txtKilometerandLocation.setText(model.getArea());
                viewHolder.txtKilometerandLocation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location, 0, 0, 0);
                viewHolder.txtCarType.setVisibility(View.INVISIBLE);
                viewHolder.txtCarMake.setVisibility(View.INVISIBLE);

            }
            break;

        }
        if (model.isFeatured()) {
            viewHolder.txtFeatured.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtFeatured.setVisibility(View.GONE);
        }

        viewHolder.imgFavourite.setOnClickListener(onClickListener);
        viewHolder.imgFavourite.setTag(position);


        if (model.isFavourite()) {
            viewHolder.imgFavourite.setImageResource(R.drawable.heartfilled);
        } else {
            viewHolder.imgFavourite.setImageResource(R.drawable.listheart);
        }

    }

    public class ViewHolder extends GenericViewHolder {
        ImageView imgResult;
        AnyTextView txtName;
        AnyTextView txtPrice;
        AnyTextView txtKilometerandLocation;
        AnyTextView txtCarMake;
        AnyTextView txtCarType;
        AnyTextView txtFeatured;
        ImageView imgFavourite;


        public ViewHolder(View view) {
            imgResult = (ImageView) view.findViewById(R.id.imgProduct);
            txtName = (AnyTextView) view.findViewById(R.id.txtName);
            txtPrice = (AnyTextView) view.findViewById(R.id.txtPrice);
            txtCarMake = (AnyTextView) view.findViewById(R.id.txtCarMake);
            txtCarType = (AnyTextView) view.findViewById(R.id.txtCarType);
            txtKilometerandLocation = (AnyTextView) view.findViewById(R.id.txtKilometerandLocation);
            txtFeatured = (AnyTextView) view.findViewById(R.id.txtFeture);
            imgFavourite = (ImageView) view.findViewById(R.id.imgFavourite);

        }
    }
}
