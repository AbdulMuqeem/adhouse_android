package com.wamztech.adhouse.ui.views.viewbinders;

import android.app.Activity;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;
import com.wamztech.adhouse.models.Attributes;
import com.wamztech.adhouse.models.Dropdownlist;
import com.wamztech.adhouse.ui.views.AnyEditTextView;
import com.wamztech.adhouse.ui.views.AnyTextView;
import com.wamztech.adhouse.ui.views.SpinnerPlus;

import java.util.ArrayList;
import java.util.HashMap;


public class SubAdsAttributesBinderV1 extends BaseView<Attributes> {


    private final Activity activity;
    //private ArrayList<Dropdownlist> arrSpAttr;
    ArrayList<ArrayList<Dropdownlist>> arrSpAttrParent;
    ArrayList<ArrayList<Dropdownlist>> arrAttrParentFull;
    NotifyDataSetChangedListener listenerNotifyDataSetChanged;
    private ArrayList<String> attValue;
    private String item;
    private int parentIndex = 0;
    private GenericArrayAdapter<Dropdownlist> adpSp;
    private HashMap<Integer, ViewHolder> hashmapViewHolder;
    private ArrayList<GenericArrayAdapter<Dropdownlist>> adpSpList;
    private ArrayList<Integer> selections;
    private boolean isSelected = false;

    public SubAdsAttributesBinderV1(Activity activity, ArrayList<String> attValue) {
        super(R.layout.item_submit_ads);
        this.activity = activity;
        this.attValue = attValue;
        arrSpAttrParent = new ArrayList<>();
        selections = new ArrayList<>();
        hashmapViewHolder = new HashMap<>();
        adpSpList = new ArrayList<>();
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    /**
     * @param positionMaster linean layout container
     **/
    @Override
    public void getView(final Attributes entity, final int positionMaster, int grpPosition, View view, final Context context) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        ArrayList<Dropdownlist> arrSpAttr = new ArrayList<>();
        hashmapViewHolder.put(positionMaster, viewHolder);

        if (entity.getAttributeType().equals("text")) {
            viewHolder.txtEdCategoryTitle.setText(entity.getAttributeName());

            if (attValue != null) {
                /** Updating Attribute Value **/
                viewHolder.edtCategory.setText(attValue.get(positionMaster));
            }

            viewHolder.edtCategory.setHint(entity.getAttributeName());
            viewHolder.llEdttxt.setVisibility(View.VISIBLE);
            viewHolder.llSp.setVisibility(View.GONE);

            /** Seting Keyboard Type **/
            if (entity.getKeyboardType().equals("numpad")) {
                viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_NUMBER);
            } else if (entity.getKeyboardType().equals("alphapad")) {
                viewHolder.edtCategory.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
            }

            //arrSpAttr.addAll(entity.getDropdownlist());
            // arrSpAttrParent.add(arrSpAttr);

            if (positionMaster == 0) {
                arrSpAttrParent.clear();
                parentIndex = 0;
            }
            arrSpAttr.addAll(entity.getDropdownlist());
            arrSpAttrParent.add(arrSpAttr);

            adpSp = new GenericArrayAdapter<>(activity, new ArrayList<Dropdownlist>(arrSpAttrParent.get(parentIndex)), new DropDownBinder());
            viewHolder.spAttrCategory.setTag(parentIndex);
            parentIndex++;
            viewHolder.spAttrCategory.setAdapter(adpSp);
            if (entity.getAdpSp() == null) {
                entity.setAdpSp(adpSp);
                adpSpList.add(entity.getAdpSp());
            }


        } else {
            viewHolder.llEdttxt.setVisibility(View.GONE);
            viewHolder.llSp.setVisibility(View.VISIBLE);
            viewHolder.txtSpCategoryTitle.setText(entity.getAttributeName());
            viewHolder.txtSpinner.setText(entity.getAttributeName());
            if (positionMaster == 0) {
                arrSpAttrParent.clear();
                parentIndex = 0;
            }
            arrSpAttr.addAll(entity.getDropdownlist());
            arrSpAttrParent.add(arrSpAttr);

            adpSp = new GenericArrayAdapter<>(activity, new ArrayList<Dropdownlist>(arrSpAttrParent.get(parentIndex)), new DropDownBinder());
            viewHolder.spAttrCategory.setTag(parentIndex);

            parentIndex++;

            viewHolder.spAttrCategory.setAdapter(adpSp);
            if (entity.getAdpSp() == null) {
                entity.setAdpSp(adpSp);
                adpSpList.add(entity.getAdpSp());
            }
            viewHolder.txtSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<ArrayList<Dropdownlist>> checkingArray = new ArrayList<>(arrSpAttrParent);
                    for (int i = 0; i < checkingArray.get(positionMaster).size(); i++) {
                        int idDropDownToSearch = checkingArray.get(positionMaster).get(i).getAttributeParentId();
                        int idDropDownValueToCheck = checkingArray.get(positionMaster).get(i).getAttributeParentValueId();

                        entity.getAdpSp().getList().clear();
                        entity.getAdpSp().getList().addAll(checkingArray.get(positionMaster));
                        entity.getAdpSp().notifyDataSetChanged();

                        if (idDropDownToSearch != 0 || idDropDownValueToCheck != 0) {
                            for (int j = 0; j < checkingArray.size(); j++) {
                                for (int k = 0; k < checkingArray.get(j).size(); k++) {
                                    if (checkingArray.get(j).get(k).getAttributeId() == idDropDownToSearch) {
                                        if (checkingArray.get(j).get(k).getId() != idDropDownValueToCheck) {
                                            if (!selections.contains(idDropDownValueToCheck)) {
                                                checkingArray.get(positionMaster).set(i, new Dropdownlist(-1));
                                                entity.getAdpSp().getList().set(i, new Dropdownlist(-1));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (Dropdownlist myDropDown : new ArrayList<>(entity.getAdpSp().getList())) {
                        if (myDropDown.getId() == -1) {
                            entity.getAdpSp().getList().remove(myDropDown);
                        }
                    }
                    viewHolder.spAttrCategory.setAdapter(entity.getAdpSp());
                    viewHolder.spAttrCategory.performClick();
                }
            });

            viewHolder.spAttrCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (selections.size() == 0) {
                        for (int i = 0; i < arrSpAttrParent.size(); i++) {
                            selections.add(0);
                        }
                    }
                    int index = Integer.parseInt(parent.getTag().toString());
                    if (entity.getAdpSp().getList().size() > 0) {
                        item = entity.getAdpSp().getList().get(position).getValue();
                        viewHolder.txtSpinner.setText(item);
                        viewHolder.txtSpinner.setTag(position);
                        try {
                            selections.set(index, entity.getAdpSp().getList().get(position).getId());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });


            if (attValue != null) {
                for (int i = 0; i < attValue.size(); i++) {
                    for (int j = 0; j < adpSp.getList().size(); j++) {
                        try {
                            if (adpSp.getList().get(j).getId().toString().equals(attValue.get(positionMaster))) {
                                viewHolder.spAttrCategory.setSelection(j);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                }

            }
        }
    }


    public NotifyDataSetChangedListener getListenerNotifyDataSetChanged() {
        return listenerNotifyDataSetChanged;
    }

    public void setListenerNotifyDataSetChanged(NotifyDataSetChangedListener listenerNotifyDataSetChanged) {
        this.listenerNotifyDataSetChanged = listenerNotifyDataSetChanged;
    }


    public interface NotifyDataSetChangedListener {
        void notifyDataSetChanged();
    }

    public class ViewHolder extends GenericViewHolder {

        AnyTextView txtEdCategoryTitle;
        AnyEditTextView edtCategory;
        AnyTextView txtSpCategoryTitle;
        AnyTextView txtSpinner;
        SpinnerPlus spAttrCategory;
        LinearLayout llEdttxt;
        LinearLayout llSp;
        int position;

        public ViewHolder(View view) {
            txtEdCategoryTitle = (AnyTextView) view.findViewById(R.id.txtEdCategoryTitle);
            txtSpCategoryTitle = (AnyTextView) view.findViewById(R.id.txtSpCategoryTitle);
            txtSpinner = (AnyTextView) view.findViewById(R.id.txtSpinnerCat);
            edtCategory = (AnyEditTextView) view.findViewById(R.id.edtCategory);
            spAttrCategory = (SpinnerPlus) view.findViewById(R.id.spAttrCategory);
            llEdttxt = (LinearLayout) view.findViewById(R.id.llEdttxt);
            llSp = (LinearLayout) view.findViewById(R.id.llSp);
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}