package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.Currency;
import com.wamztech.adhouse.ui.views.AnyTextView;

public class SpCurrencyBinder extends BaseView<Currency> {


    public SpCurrencyBinder() {
        super(R.layout.item_spinner);


    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(Currency model, int position, int grpPosition, View view, Context context) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtSpinner.setText(model.getName() + " - " + model.getSymbol());

    }


    public class ViewHolder extends GenericViewHolder {
        AnyTextView txtSpinner;

        public ViewHolder(View view) {

            txtSpinner = (AnyTextView) view.findViewById(R.id.txtSpinner);

        }
    }
}
