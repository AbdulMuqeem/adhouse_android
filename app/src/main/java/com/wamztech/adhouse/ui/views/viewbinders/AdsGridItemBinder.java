package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.helpers.Utils;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.ui.views.AnyTextView;

import static com.wamztech.adhouse.models.Ad.TYPE_PROPERTIES;


public class AdsGridItemBinder extends BaseView<Ad> {

    public AdsGridItemBinder() {
        super(R.layout.item_list_ads);
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void getView(Ad model, int position, int grpPosition, View view, Context context) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();


        if (model.getAdImages().size() > 0) {
            ImageLoader.getInstance().displayImage(model.getAdImages().get(0).getImageUrl(), viewHolder.imgDetail);
        }

        viewHolder.txtName.setText(model.getTitle());
        viewHolder.txtPrice.setText(model.getCurrency_symbol() + " " + Utils.formatNumber(model.getPrice()));
        viewHolder.txtPrice.setVisibility(View.VISIBLE);
        viewHolder.txtFeatured.setText("Featured");
        if (model.isFeatured()) {
            viewHolder.txtFeatured.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtFeatured.setVisibility(View.INVISIBLE);
        }


        //New Code Inject

        viewHolder.txtKilometerandLocation.setVisibility(View.INVISIBLE);
        if(!model.getKilometer().equalsIgnoreCase("0") ){
            viewHolder.txtKilometerandLocation.setText("KM: " + Utils.formatNumber(model.getKilometer()));
            viewHolder.txtKilometerandLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            viewHolder.txtKilometerandLocation.setVisibility(View.VISIBLE);
        }
        viewHolder.txtCarType.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        viewHolder.txtCarMake.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        viewHolder.txtCarType.setVisibility(View.INVISIBLE);
        viewHolder.txtCarMake.setVisibility(View.INVISIBLE);

        for (int i = 0; i < model.getAdDetails().size(); i++) {
            // Car Class
            if (model.getAdDetails().get(i).getAttributeId() == 4) {
                viewHolder.txtCarType.setText(model.getAdDetails().get(i).getAttributeValue());
                viewHolder.txtCarType.setVisibility(View.VISIBLE);
            }

            // Car Make
            if (model.getAdDetails().get(i).getAttributeId() == 3) {
                viewHolder.txtCarMake.setText(model.getAdDetails().get(i).getAttributeValue());
                viewHolder.txtCarMake.setVisibility(View.VISIBLE);
            }
        }

        // If Car Ads so sow KMS t o 0
        if(viewHolder.txtCarMake.getVisibility() == View.VISIBLE && viewHolder.txtCarType.getVisibility() == View.VISIBLE){
            if(model.getKilometer().equalsIgnoreCase("0")){
                viewHolder.txtKilometerandLocation.setText("KM: 0" );
                viewHolder.txtKilometerandLocation.setVisibility(View.VISIBLE);
                viewHolder.txtKilometerandLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }


        //New Code Inject
        // For City Name in Property
        if(model.getType().equals(TYPE_PROPERTIES)){
            viewHolder.txtCity.setVisibility(View.VISIBLE);
            if(model.getAdDetails().size() >= 1)
                viewHolder.txtCity.setText(model.getAdDetails().get(model.getAdDetails().size() - 1).getAttributeValue());
//                if(model.getAdDetails().get(model.getAdDetails().size() - 1).getAttributeValue().matches("[0-9]+"))
//                    viewHolder.txtCity.setText(model.getAdDetails().get(model.getAdDetails().size() - 1).getAttributeValue());
//                else
//                    viewHolder.txtCity.setText("N/A");
            else
                viewHolder.txtCity.setText("N/A");

        }else
            viewHolder.txtCity.setVisibility(View.GONE);


    }


    public class ViewHolder extends GenericViewHolder {
        ImageView imgDetail;
        AnyTextView txtName;
        AnyTextView txtPrice;
        AnyTextView txtKilometerandLocation;
        AnyTextView txtCarMake;
        AnyTextView txtCarType;
        AnyTextView txtFeatured;
        AnyTextView txtCity;

        public ViewHolder(View view) {
            imgDetail = (ImageView) view.findViewById(R.id.imgDetail);
            txtName = (AnyTextView) view.findViewById(R.id.txtName);
            txtPrice = (AnyTextView) view.findViewById(R.id.txtPrice);
            txtCarMake = (AnyTextView) view.findViewById(R.id.txtCarMake);
            txtCarType = (AnyTextView) view.findViewById(R.id.txtCarType);
            txtKilometerandLocation = (AnyTextView) view.findViewById(R.id.txtKilometerandLocation);
            txtFeatured = (AnyTextView) view.findViewById(R.id.txtFeture);
            txtCity = (AnyTextView) view.findViewById(R.id.txtCity);
        }
    }
}
