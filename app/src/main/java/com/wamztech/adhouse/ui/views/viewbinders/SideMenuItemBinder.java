package com.wamztech.adhouse.ui.views.viewbinders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.adapters.BaseView;
import com.wamztech.adhouse.models.SideMenu;


public class SideMenuItemBinder extends BaseView<SideMenu> {

    private Context context;
    public SideMenuItemBinder() {
        super(R.layout.item_side_menu);
    }

    @Override
    protected GenericViewHolder ViewHolder(View view) {
        return new SideMenuItemBinder.ViewHolder(view);
    }

    @Override
    public void getView(SideMenu model, final int position, int grpPosition, View view, Context context) {

        SideMenuItemBinder.ViewHolder viewHolder = (ViewHolder) view.getTag();


        switch (model.getLabel_id()) {

//            case SideMenu.SIDE_MENU_SUBSCRIPTION: {
//
//                String heading = context.getResources().getString(R.string.Subscription_Commercial);
//                String subHeading ;
//
//
//
//                if (getUserSubscriptionData() != null) {
//                    subHeading = context.getResources().getString(R.string.you_can_post) + getUserSubscriptionData().getRemainingAds() + context.getResources().getString(R.string.in) + getUserSubscriptionData().getRemainingDays() + context.getResources().getString(R.string.days)/*+ getUserSubscriptionData().getPackageId()*/;
//                }
//                else{
//
//                    subHeading = context.getResources().getString(R.string.guest_user);
//                }
//
//
//                SpannableString span1 = new SpannableString(heading);
//                span1.setSpan(new AbsoluteSizeSpan(context.getResources().getDimensionPixelSize(R.dimen.x14)), 0, heading.length(), SPAN_INCLUSIVE_INCLUSIVE);
//
//                SpannableString span2 = new SpannableString(subHeading);
//                span2.setSpan(new AbsoluteSizeSpan(context.getResources().getDimensionPixelSize(R.dimen.x12)), 0, subHeading.length(), SPAN_INCLUSIVE_INCLUSIVE);
//                span2.setSpan(new ForegroundColorSpan(Color.GRAY), 0, span2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                CharSequence finalText = TextUtils.concat(span1, "\n", span2);
//
//                viewHolder.txtTitle.setText(finalText);
//                viewHolder.imgIcon.setImageResource(R.drawable.ssm_icon_sub_com);
//            }
//            break;

            default: {
                viewHolder.imgIcon.setImageResource(model.getImgIcon());
                viewHolder.txtTitle.setText(model.getTxtTitle());
            }
            break;
        }

    }

//    public UserSubscription getUserSubscriptionData() {
//        return userSubscription;
//    }
//
//    public void setUserSubscriptionData(UserSubscription userSubscription) {
//        this.userSubscription = userSubscription;
//    }

    public class ViewHolder extends GenericViewHolder {
        ImageView imgIcon;
        TextView txtTitle;

        public ViewHolder(View view) {
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        }
    }

}
