package com.wamztech.adhouse.ui.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wamztech.adhouse.R;

import java.util.ArrayList;
import java.util.List;


public class SpinnerAdapter extends BaseAdapter
{
    private ArrayList<String> arrListCities = new ArrayList<String>();
    Context context;
//    ArrayList<String> arrSpiner;
    List<String> arrSpiner;
    LayoutInflater inflter;

    public SpinnerAdapter(Context applicationContext, List<String> arrSpiner) {

        this.context = applicationContext;
//        this.arrSpiner = arrSpiner;
        this.arrSpiner = arrSpiner;
        inflter = (LayoutInflater.from(applicationContext));
    }


    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return arrSpiner.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long)i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spinner, null);

//        City city =arrSpiner.get(i) ;
        TextView txtSpinner = (TextView) view.findViewById(R.id.txtSpinner);
        txtSpinner.setText("select city");
        txtSpinner.setText(arrSpiner.get(i));
//        txtSpinner.setText(city.getTitle());
        return view;
    }
}