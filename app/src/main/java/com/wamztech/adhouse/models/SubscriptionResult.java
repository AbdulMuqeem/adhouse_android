package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SubscriptionResult {

    @SerializedName("client_id")
    @Expose
    private String clientId = null;

    @SerializedName("conversion_rate")
    @Expose
    private double conversionRate = 1;

    @SerializedName("individual")
    @Expose
    private ArrayList<Subscription> individual = null;
    @SerializedName("commercial")
    @Expose
    private ArrayList<Subscription> commercial = null;
    @SerializedName("images")
    @Expose
    private ArrayList<SubscriptionImages> images = null;


    public ArrayList<Subscription> getIndividual() {
        return individual;
    }

    public void setIndividual(ArrayList<Subscription> individual) {
        this.individual = individual;
    }

    public ArrayList<Subscription> getCommercial() {
        return commercial;
    }

    public void setCommercial(ArrayList<Subscription> commercial) {
        this.commercial = commercial;
    }

    public ArrayList<SubscriptionImages> getImages() {
        return images;
    }

    public void setImages(ArrayList<SubscriptionImages> images) {
        this.images = images;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(double conversionRate) {
        this.conversionRate = conversionRate;
    }
}
