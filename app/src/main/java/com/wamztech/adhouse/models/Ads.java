package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Ads {

    @SerializedName("vehicles")
    @Expose
    private ArrayList<Ad> vehicles = null;
    @SerializedName("properties")
    @Expose
    private ArrayList<Ad> properties = null;

    @SerializedName("classified_category")
    @Expose
    private ArrayList<ClassifiedCategory> classifiedCategory = null;

    public ArrayList<ClassifiedCategory> getClassifiedCategory() {
        return classifiedCategory;
    }

    public void setClassifiedCategory(ArrayList<ClassifiedCategory> classifiedCategory) {
        this.classifiedCategory = classifiedCategory;
    }


    public ArrayList<Ad> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<Ad> vehicles) {
        this.vehicles = vehicles;
    }

    public ArrayList<Ad> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<Ad> properties) {
        this.properties = properties;
    }




}
