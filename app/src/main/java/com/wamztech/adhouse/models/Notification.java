package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Notification {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sender_id")
    @Expose
    private Integer senderId;
    @SerializedName("reciever_id")
    @Expose
    private Integer recieverId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("action_type")
    @Expose
    private String actionType;
    @SerializedName("action_id")
    @Expose
    private Integer actionId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(Integer recieverId) {
        this.recieverId = recieverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    //    String txtNotification;
//    String txtTime;
//    String txtDetail;
//
//    public Notification(String txtNotification, String txtTime, String txtDetail) {
//        this.txtNotification = txtNotification;
//        this.txtTime = txtTime;
//        this.txtDetail = txtDetail;
//    }
//
//    public String getTxtNotification() {
//        return txtNotification;
//    }
//
//    public void setTxtNotification(String txtNotification) {
//        this.txtNotification = txtNotification;
//    }
//
//    public String getTxtTime() {
//        return txtTime;
//    }
//
//    public void setTxtTime(String txtTime) {
//        this.txtTime = txtTime;
//    }
//
//    public String getTxtDetail() {
//        return txtDetail;
//    }
//
//    public void setTxtDetail(String txtDetail) {
//        this.txtDetail = txtDetail;
//    }
}
