package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subscription {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subscription_type")
    @Expose
    private Integer subscriptionType;
    @SerializedName("ad_validity")
    @Expose
    private String adValidity;
    @SerializedName("free_ads")
    @Expose
    private String freeAds;
    @SerializedName("max_ads")
    @Expose
    private String maxAds;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("is_popular")
    @Expose
    private Integer isPopular;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(Integer subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getAdValidity() {
        return adValidity;
    }

    public void setAdValidity(String adValidity) {
        this.adValidity = adValidity;
    }

    public String getFreeAds() {
        return freeAds;
    }

    public void setFreeAds(String freeAds) {
        this.freeAds = freeAds;
    }

    public String getMaxAds() {
        return maxAds;
    }

    public void setMaxAds(String maxAds) {
        this.maxAds = maxAds;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String get_package() {
        return _package;
    }

    public void set_package(String _package) {
        this._package = _package;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsPopular() {
        return isPopular;
    }

    public void setIsPopular(Integer isPopular) {
        this.isPopular = isPopular;
    }

    public boolean isPopular() {
        return getIsPopular() == 1;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    //    String txtMonth;
//    String txtFreeAds;
//    String txtPrice;
//    String txtAdsValidity;
//    String txtMaxAds;
//
//    boolean isPopular ;
//
//    public Subscription(String txtMonth, String txtFreeAds, String txtPrice, String txtAdsValidity, String txtMaxAds) {
//        this.txtMonth = txtMonth;
//        this.txtFreeAds = txtFreeAds;
//        this.txtPrice = txtPrice;
//        this.txtAdsValidity = txtAdsValidity;
//        this.txtMaxAds = txtMaxAds;
//    }
//
//    public String getTxtMonth() {
//        return txtMonth;
//    }
//
//    public void setTxtMonth(String txtMonth) {
//        this.txtMonth = txtMonth;
//    }
//
//    public String getTxtFreeAds() {
//        return txtFreeAds;
//    }
//
//    public void setTxtFreeAds(String txtFreeAds) {
//        this.txtFreeAds = txtFreeAds;
//    }
//
//    public String getTxtPrice() {
//        return txtPrice;
//    }
//
//    public void setTxtPrice(String txtPrice) {
//        this.txtPrice = txtPrice;
//    }
//
//    public String getTxtAdsValidity() {
//        return txtAdsValidity;
//    }
//
//    public void setTxtAdsValidity(String txtAdsValidity) {
//        this.txtAdsValidity = txtAdsValidity;
//    }
//
//    public String getTxtMaxAds() {
//        return txtMaxAds;
//    }
//
//    public void setTxtMaxAds(String txtMaxAds) {
//        this.txtMaxAds = txtMaxAds;
//    }
//
//    public boolean isPopular() {
//        return isPopular;
//    }
//
//    public void setPopular(boolean popular) {
//        isPopular = popular;
//    }
}

