package com.wamztech.adhouse.models;


public class DetailHorizontal {

    String imgDetail;
    String txtName;
    String txtDate;
    String txtFeatures;


    public DetailHorizontal(String imgDetail, String txtName, String txtFeatures) {
        this.imgDetail = imgDetail;
        this.txtName = txtName;
        this.txtFeatures = txtFeatures;

    }

    public String getImgDetail() {
        return imgDetail;
    }

    public void setImgDetail(String imgDetail) {
        this.imgDetail = imgDetail;
    }

    public String getTxtName() {
        return txtName;
    }

    public void setTxtName(String txtName) {
        this.txtName = txtName;
    }



    public String getTxtDate() {
        return txtDate;
    }

    public void setTxtDate(String txtDate) {
        this.txtDate = txtDate;
    }

    public String getTxtFeture() {
        return txtFeatures;
    }

    public void setTxtFeture(String txtFeatures) {
        this.txtFeatures = txtFeatures;
    }

}
