package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Dropdownlist {
    private final static long serialVersionUID = -2134869534642024538L;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_attribute_id")
    @Expose
    private Integer attributeParentId;
    @SerializedName("parent_attribute_value_id")
    @Expose
    private Integer attributeParentValueId;
    @SerializedName("attribute_id")
    @Expose
    private int attributeId;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("child_attribute_value_id")
    @Expose
    private String children;
    @SerializedName("child_attribute_id")
    @Expose
    private String parents;

    public Dropdownlist(Integer id) {
        this.id = id;
    }

    public Dropdownlist() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getAttributeParentId() {
        return attributeParentId;
    }

    public void setAttributeParentId(Integer attributeParentId) {
        this.attributeParentId = attributeParentId;
    }

    public Integer getAttributeParentValueId() {
        return attributeParentValueId;
    }

    public void setAttributeParentValueId(Integer attributeParentValueId) {
        this.attributeParentValueId = attributeParentValueId;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getParents() {
        return parents;
    }

    public void setParents(String parents) {
        this.parents = parents;
    }
}
