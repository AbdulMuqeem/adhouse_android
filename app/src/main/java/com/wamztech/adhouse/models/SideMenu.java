package com.wamztech.adhouse.models;


public class SideMenu {


    public static final int SIDE_MENU_HOME = 0;
    public static final int SIDE_MENU_NOTIFICATION = 1;
    public static final int SIDE_MENU_MY_FAVORITES = 2;
    public static final int SIDE_MENU_MY_ADS = 3;
    public static final int SIDE_MENU_MY_PROFILE = 4;
    public static final int SIDE_MENU_SETTINGS = 5;
    public static final int SIDE_MENU_SUBSCRIPTION = 6 ;
    public static final int SIDE_MENU_LOGOUT = 7;

    int label_id;
    int imgIcon;
    String txtTitle;

    public SideMenu(int label_id, int imgIcon, String txtTitle) {

        this.label_id = label_id;
        this.imgIcon = imgIcon;
        this.txtTitle = txtTitle;

    }

    public int getLabel_id() {
        return label_id;
    }

    public void setLabel_id(int label_id) {
        this.label_id = label_id;
    }

    public int getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(int imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getTxtTitle() {
        return txtTitle;
    }

    public void setTxtTitle(String txtTitle) {
        this.txtTitle = txtTitle;
    }
}
