package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionImages {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("subscription_image")
    @Expose
    private String subscriptionImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(String subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

}