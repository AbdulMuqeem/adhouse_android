package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.wamztech.adhouse.adapters.GenericArrayAdapter;

import java.util.List;


public class Attributes {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("attribute_name")
    @Expose
    private String attributeName;
    @SerializedName("attribute_type")
    @Expose
    private String attributeType;
    @SerializedName("keyboard_type")
    @Expose
    private String keyboardType;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("dropdownlist")
    @Expose
    private List<Dropdownlist> dropdownlist = null;
    private final static long serialVersionUID = 6881198816474684494L;

    private transient GenericArrayAdapter<Dropdownlist> adpSp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }

    public String getKeyboardType() {
        return keyboardType;
    }

    public void setKeyboardType(String keyboardType) {
        this.keyboardType = keyboardType;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Dropdownlist> getDropdownlist() {
        return dropdownlist;
    }

    public void setDropdownlist(List<Dropdownlist> dropdownlist) {
        this.dropdownlist = dropdownlist;
    }


    public GenericArrayAdapter<Dropdownlist> getAdpSp() {
        return adpSp;
    }

    public void setAdpSp(GenericArrayAdapter<Dropdownlist> adpSp) {
        this.adpSp = adpSp;
    }
}
