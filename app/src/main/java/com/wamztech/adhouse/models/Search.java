package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Search {

    @SerializedName("Ad")
    @Expose
    private ArrayList<Ad> ad = null;
    @SerializedName("count")
    @Expose
    private int count;


    public ArrayList<Ad> getAd() {
        return ad;
    }

    public void setAd(ArrayList<Ad> ad) {
        this.ad = ad;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}

