package com.wamztech.adhouse.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSubscription {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("payment_response")
    @Expose
    private String paymentResponse;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("subscription_ads")
    @Expose
    private Integer subscriptionAds;
    @SerializedName("free_ads")
    @Expose
    private Integer freeAds;
    @SerializedName("remaining_ads")
    @Expose
    private Integer remainingAds;
    @SerializedName("subscription_duration")
    @Expose
    private Integer subscriptionDuration;
    @SerializedName("remaining_days")
    @Expose
    private Integer remainingDays;
    @SerializedName("expired_time")
    @Expose
    private String expiredTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSubscriptionAds() {
        return subscriptionAds;
    }

    public void setSubscriptionAds(Integer subscriptionAds) {
        this.subscriptionAds = subscriptionAds;
    }

    public Integer getFreeAds() {
        return freeAds;
    }

    public void setFreeAds(Integer freeAds) {
        this.freeAds = freeAds;
    }

    public Integer getRemainingAds() {
        return remainingAds;
    }

    public void setRemainingAds(Integer remainingAds) {
        this.remainingAds = remainingAds;
    }

    public Integer getSubscriptionDuration() {
        return subscriptionDuration;
    }

    public void setSubscriptionDuration(Integer subscriptionDuration) {
        this.subscriptionDuration = subscriptionDuration;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }


}

