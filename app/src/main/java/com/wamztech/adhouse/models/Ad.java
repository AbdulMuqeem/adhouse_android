package com.wamztech.adhouse.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Ad implements Parcelable {

    public static final String TYPE_VEHICLE = "Vehicle";
    public static final String TYPE_PROPERTIES = "Properties";
    public static final String TYPE_CLASSIFIED = "Classified";
    public static final Creator<Ad> CREATOR = new Creator<Ad>() {
        @Override
        public Ad createFromParcel(Parcel in) {
            return new Ad(in);
        }

        @Override
        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    };
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("sub_category_id")
    @Expose
    private int subCategoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("is_property")
    @Expose
    private int isProperty;
    @SerializedName("is_vehicle")
    @Expose
    private int isVehicle;
    @SerializedName("is_classified")
    @Expose
    private int isClassified;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("featured")
    @Expose
    private int featured;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("is_sold")
    @Expose
    private int isSold;
    @SerializedName("is_verified")
    @Expose
    private int isVerified;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_like")
    @Expose
    private int isLike;
    @SerializedName("kilometer")
    @Expose
    private String kilometer;
    @SerializedName("ad_images")
    @Expose
    private ArrayList<AdImages> adImages = null;
    @SerializedName("user_details")
    @Expose
    private UserDetail userDetails;
    @SerializedName("ad_details")
    @Expose
    private ArrayList<AdDetail> adDetails = null;
    @SerializedName("total_views")
    @Expose
    private String total_views;
    @SerializedName("currency_symbol")
    @Expose
    private String currency_symbol;

    protected Ad(Parcel in) {
        title = in.readString();
        price = in.readString();
        phone = in.readString();
        description = in.readString();
        area = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        isLike = in.readInt();
        kilometer = in.readString();
        total_views = in.readString();
        currency_symbol = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsProperty() {
        return isProperty;
    }

    public void setIsProperty(int isProperty) {
        this.isProperty = isProperty;
    }

    public int getIsVehicle() {
        return isVehicle;
    }

    public void setIsVehicle(int isVehicle) {
        this.isVehicle = isVehicle;
    }

    public int getIsClassified() {
        return isClassified;
    }

    public void setIsClassified(int isClassified) {
        this.isClassified = isClassified;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFeatured() {
        return featured;
    }

    public boolean isFeatured() {
        return getFeatured() == 1;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getIsSold() {
        return isSold;
    }

    public void setIsSold(int isSold) {
        this.isSold = isSold;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIsLike() {
        return isLike;
    }


    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public boolean isFavourite() {
        return getIsLike() == 1;
    }
//
//    public void setFavorite(boolean favorite) {
//        isFavourite() = favorite;
//    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public ArrayList<AdImages> getAdImages() {
        return adImages;
    }

    public void setAdImages(ArrayList<AdImages> adImages) {
        this.adImages = adImages;
    }

    public UserDetail getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetail userDetails) {
        this.userDetails = userDetails;
    }

    public ArrayList<AdDetail> getAdDetails() {
        return adDetails;
    }

    public void setAdDetails(ArrayList<AdDetail> adDetails) {
        this.adDetails = adDetails;
    }

    public String getTotal_views() {
        return total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(price);
        dest.writeString(phone);
        dest.writeString(description);
        dest.writeString(area);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(total_views);
        dest.writeString(currency_symbol);
    }


    public String getType() {

        if( isClassified ==1 ){
            return  TYPE_CLASSIFIED ;
        }


          if( isProperty == 1 ){
            return  TYPE_PROPERTIES ;
        }



          if( isVehicle==1 ){
            return  TYPE_VEHICLE ;
        }


        return TYPE_VEHICLE ;
    }
}
