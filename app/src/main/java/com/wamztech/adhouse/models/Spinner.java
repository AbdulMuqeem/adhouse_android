package com.wamztech.adhouse.models;


public class Spinner {

    String txtSpinner;

    public Spinner(String txtSpinner) {
        this.txtSpinner = txtSpinner;
    }

    public String getTxtSpinner() {
        return txtSpinner;
    }

    public void setTxtSpinner(String txtSpinner) {
        this.txtSpinner = txtSpinner;
    }
}
