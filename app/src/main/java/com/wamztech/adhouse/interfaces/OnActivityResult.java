package com.wamztech.adhouse.interfaces;

import android.content.Intent;


public interface OnActivityResult {
    void onResult(int requestCode, int resultCode, Intent data);

 }
