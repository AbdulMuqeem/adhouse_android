package com.wamztech.adhouse.interfaces;

public interface PermissionEvents {
    void onPermissionGranted();
    void onPermissionDenied();
}
