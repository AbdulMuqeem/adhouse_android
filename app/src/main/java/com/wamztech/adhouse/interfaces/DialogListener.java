package com.wamztech.adhouse.interfaces;



public interface DialogListener {

    void onOkClicked();
    void onCancelClicked();

}
