package com.wamztech.adhouse;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.StrictMode;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.L;

import androidx.multidex.MultiDexApplication;


public class BaseApplication extends MultiDexApplication {
    private static Context mContext;
    public static String googleApiKey;


    public static void setGoogleApiKey(String googleApiKey) {
        BaseApplication.googleApiKey = googleApiKey;
    }

    public static String getGoogleApiKey() {
        return googleApiKey;
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initImageLoader();
        mContext = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
//        Mint.initAndStartSession(this, "656a96da");
    }

    public void initImageLoader() {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.color.colorAccent)
                .showImageOnFail(R.color.colorAccent).resetViewBeforeLoading(true)
                .cacheInMemory(true).cacheOnDisc(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).defaultDisplayImageOptions(options)
                .build();

        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config);
        L.disableLogging();
    }

}
