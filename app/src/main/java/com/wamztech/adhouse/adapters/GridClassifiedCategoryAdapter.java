package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.activities.MainActivity;
import com.wamztech.adhouse.fragments.ClassifiedListingFragment;
import com.wamztech.adhouse.models.ClassifiedCategory;
import com.wamztech.adhouse.ui.views.AnyTextView;

import java.util.ArrayList;


public class GridClassifiedCategoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ClassifiedCategory> arrGridCollection;
    private boolean isOptionsVisible;
    private View.OnClickListener onClickListener;

    public GridClassifiedCategoryAdapter(Context context, ArrayList<ClassifiedCategory> arrGridCollection, boolean isOptionsVisible, View.OnClickListener onClickListener) {
        this.context = context;
        this.arrGridCollection = arrGridCollection;
        this.isOptionsVisible = isOptionsVisible;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return arrGridCollection.size();
    }

    @Override
    public Object getItem(int position) {
        return arrGridCollection.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.horizontal_grid_items, parent, false);
            view.setFocusable(false);
            viewHolder = new ViewHolder();
            viewHolder.imgDetail = (ImageView) view.findViewById(R.id.imgDetail);
            viewHolder.txtName = (AnyTextView) view.findViewById(R.id.txtName);
            viewHolder.txtDate = (AnyTextView) view.findViewById(R.id.txtDate);
            viewHolder.imgEdit = (ImageView) view.findViewById(R.id.imgEdit);
            viewHolder.imgDel = (ImageView) view.findViewById(R.id.imgDel);
            viewHolder.txtFeatured = (AnyTextView) view.findViewById(R.id.txtFeture);
            viewHolder.llMyAds = (LinearLayout) view.findViewById(R.id.llMyAds);
            viewHolder.linearGrid = (LinearLayout) view.findViewById(R.id.linearGrid);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final ClassifiedCategory model = arrGridCollection.get(position);

        viewHolder.txtName.setText(model.getName());
        viewHolder.txtName.setGravity(View.TEXT_ALIGNMENT_CENTER);
        ImageLoader.getInstance().displayImage(model.getCategoryImage(), viewHolder.imgDetail);

        viewHolder.txtFeatured.setText("Featured");


        viewHolder.linearGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).addDockableFragment(ClassifiedListingFragment.newInstance(model.getId(), model.getName()));
            }
        });

        viewHolder.imgDel.setOnClickListener(onClickListener);
        viewHolder.imgDel.setTag(position);

        viewHolder.imgEdit.setOnClickListener(onClickListener);
        viewHolder.imgEdit.setTag(position);


        if (isOptionsVisible) {
            viewHolder.llMyAds.setVisibility(View.VISIBLE);
        } else {
            viewHolder.llMyAds.setVisibility(View.GONE);
        }
        return view;
    }

    public void remove(int position) {
        arrGridCollection.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.arrGridCollection.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList obj) {

        this.arrGridCollection.addAll(obj);
        notifyDataSetChanged();
    }

    public class ViewHolder {
        AnyTextView txtName;
        ImageView imgDetail;
        AnyTextView txtDate;
        ImageView imgEdit;
        ImageView imgDel;
        LinearLayout linearGrid;
        LinearLayout llMyAds;
        AnyTextView txtFeatured;
    }
}
