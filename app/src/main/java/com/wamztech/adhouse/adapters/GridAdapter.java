package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.models.Ad;
import com.wamztech.adhouse.ui.views.AnyTextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Ad> arrGridCollection;
    private boolean isOptionsVisible;
    private View.OnClickListener onClickListener;

    public GridAdapter(Context context, ArrayList<Ad> arrGridCollection, boolean isOptionsVisible, View.OnClickListener onClickListener) {
        this.context = context;
        this.arrGridCollection = arrGridCollection;
        this.isOptionsVisible = isOptionsVisible;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return arrGridCollection.size();
    }

    @Override
    public Object getItem(int position) {
        return arrGridCollection.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.horizontal_grid_items, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imgDetail = (ImageView) view.findViewById(R.id.imgDetail);
            viewHolder.txtName = (AnyTextView) view.findViewById(R.id.txtName);
            viewHolder.txtDate = (AnyTextView) view.findViewById(R.id.txtDate);
            viewHolder.imgEdit = (ImageView) view.findViewById(R.id.imgEdit);
            viewHolder.imgDel = (ImageView) view.findViewById(R.id.imgDel);
            viewHolder.txtFeatured = (AnyTextView) view.findViewById(R.id.txtFeture);
            viewHolder.llMyAds = (LinearLayout) view.findViewById(R.id.llMyAds);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Ad model = arrGridCollection.get(position);

        viewHolder.txtName.setText(model.getTitle());

        if (model.getAdImages() != null)
            ImageLoader.getInstance().displayImage(model.getAdImages().get(0).getImageUrl(), viewHolder.imgDetail);

        viewHolder.txtFeatured.setText("Featured");

        viewHolder.txtDate.setText(model.getPrice());

        viewHolder.imgDel.setOnClickListener(onClickListener);
        viewHolder.imgDel.setTag(position);

        viewHolder.imgEdit.setOnClickListener(onClickListener);
        viewHolder.imgEdit.setTag(position);


        if (isOptionsVisible) {
            viewHolder.llMyAds.setVisibility(View.VISIBLE);
        } else {
            viewHolder.llMyAds.setVisibility(View.GONE);
        }
        if (model.isFeatured()) {
            viewHolder.txtFeatured.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtFeatured.setVisibility(View.GONE);
        }
        return view;
    }

    public void remove(int position) {
        arrGridCollection.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.arrGridCollection.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList obj) {

        this.arrGridCollection.addAll(obj);
        notifyDataSetChanged();
    }

    public class ViewHolder {
        AnyTextView txtName;
        ImageView imgDetail;
        AnyTextView txtDate;
        ImageView imgEdit;
        ImageView imgDel;
        LinearLayout llMyAds;
        AnyTextView txtFeatured;
    }

}
