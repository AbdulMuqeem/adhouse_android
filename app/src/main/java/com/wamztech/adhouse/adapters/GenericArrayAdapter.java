package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;


public class GenericArrayAdapter<T> extends BaseAdapter implements Filterable {

    protected List<T> arrayListFiltered;
    protected List<T> arrayList;
    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            T filter = (T) resultValue;
            return filter.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                arrayListFiltered.clear();
                for (T people : arrayList) {
                    if (people.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        arrayListFiltered.add(people);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arrayListFiltered;
                filterResults.count = arrayListFiltered.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<T> c = (ArrayList<T>) results.values;
            if (results != null && results.count > 0) {
                arrayListFiltered.clear();
                for (T cust : c) {
                    arrayListFiltered.add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };
    private ArrayList<T> arrCollection;
    private Context context;
    private BaseView baseView;

    public GenericArrayAdapter(Context context, ArrayList<T> arrCollection, BaseView baseView) {
        this.arrCollection = arrCollection;
        this.context = context;
        this.baseView = baseView;
    }

    @Override
    public int getCount() {
        return arrCollection.size();
    }

    @Override
    public Object getItem(int position) {
        return arrCollection.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = baseView.ViewCreate(context);
        }

        baseView.getView(arrCollection.get(position), position, 0, convertView, context);

        return convertView;
    }

    public void add(T obj) {

        this.arrCollection.add(obj);
        notifyDataSetChanged();
    }

    public void add(int position, T obj) {

        this.arrCollection.add(position, obj);
        notifyDataSetChanged();
    }

    public void addAll(ArrayList obj) {

        this.arrCollection.addAll(obj);
        notifyDataSetChanged();
    }

    public void remove(int position) {

        this.arrCollection.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.arrCollection.clear();
        notifyDataSetChanged();
    }

    public ArrayList<T> getList() {
        return this.arrCollection;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }


}
