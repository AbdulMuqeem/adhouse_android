package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wamztech.adhouse.R;

import java.util.ArrayList;


public class SpSubmitAdsAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> arrSpAds;
    LayoutInflater inflter;

    public SpSubmitAdsAdapter(Context applicationContext, ArrayList<String> arrSpAds) {

        this.context = applicationContext;
        this.arrSpAds = arrSpAds;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return arrSpAds.size();
    }

    @Override
    public Object getItem(int i) {
        return arrSpAds.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_sp_submit_ads, null);
        TextView txtSpSubmitAds = (TextView) view.findViewById(R.id.txtSpSubmitAds);
        txtSpSubmitAds.setText("select city");
        txtSpSubmitAds.setText(arrSpAds.get(i));
        return view;
    }
}