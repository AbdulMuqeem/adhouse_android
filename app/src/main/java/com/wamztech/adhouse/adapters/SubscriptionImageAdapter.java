package com.wamztech.adhouse.adapters;

import android.content.Context;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.models.SubscriptionImages;

import java.util.ArrayList;

import androidx.viewpager.widget.PagerAdapter;


public class SubscriptionImageAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    private View.OnClickListener onClickListener;
    private ArrayList<SubscriptionImages> arrimages;


    public SubscriptionImageAdapter(Context context, ArrayList<SubscriptionImages> arrimages, View.OnClickListener onClickListener) {
        this.context = context;
        this.arrimages = arrimages;
        this.onClickListener = onClickListener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrimages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = null;
        ViewHolder viewHolder;
        if (itemView == null) {
            itemView = layoutInflater.inflate(R.layout.item_viewpager, container, false);
            viewHolder = new ViewHolder();
            viewHolder.imgPager = (ImageView) itemView.findViewById(R.id.imgPager);
            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        SubscriptionImages entity = arrimages.get(position);
        ImageLoader.getInstance().displayImage(entity.getSubscriptionImage(), viewHolder.imgPager);
        container.addView(itemView);

        viewHolder.imgPager.setOnClickListener(onClickListener);
        viewHolder.imgPager.setTag(position);
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void addAll(ArrayList<SubscriptionImages> arrayList) {
        arrimages.addAll(arrayList);
        notifyDataSetChanged();

    }

    public Object getItem(int position) {
        return arrimages.get(position);
    }

    public void clear() {
        arrimages.clear();
        notifyDataSetChanged();

    }

    public ArrayList<SubscriptionImages> getList() {
        return arrimages;
    }

    public class ViewHolder {
        ImageView imgPager;
    }


}
