package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.wamztech.adhouse.R;
import com.wamztech.adhouse.helpers.BasePreferenceHelper;
import com.wamztech.adhouse.models.Subscription;
import com.wamztech.adhouse.ui.views.AnyTextView;

import java.util.ArrayList;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;


public class GridSubscriptionAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Subscription> arrsubscription;
    private int TextSizeQAR;
    private int TextSizemonthNumber;
    private int TextSizemonth;

    BasePreferenceHelper basePreferenceHelper;

    public GridSubscriptionAdapter(Context context, ArrayList<Subscription> arrsubscription) {
        this.context = context;
        this.arrsubscription = arrsubscription;
        TextSizemonthNumber = context.getResources().getDimensionPixelSize(R.dimen.x20);
        TextSizemonth = context.getResources().getDimensionPixelSize(R.dimen.x14);
        TextSizeQAR = context.getResources().getDimensionPixelSize(R.dimen.x12);
        basePreferenceHelper = new BasePreferenceHelper(context);
    }

    @Override
    public int getCount() {
        return arrsubscription.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.subscription_grid_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtMonth = (AnyTextView) view.findViewById(R.id.txtMonth);
            viewHolder.txtInputFreeAds = (AnyTextView) view.findViewById(R.id.txtInputFreeAds);
            viewHolder.txtInputAdsValidity = (AnyTextView) view.findViewById(R.id.txtInputAdsValidity);
            viewHolder.txtInputMaxAds = (AnyTextView) view.findViewById(R.id.txtInputMaxAds);
            viewHolder.txtPopular = (AnyTextView) view.findViewById(R.id.txtPopular);
            viewHolder.llContent = (LinearLayout) view.findViewById(R.id.llContent);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Subscription entity = arrsubscription.get(position);

        viewHolder.txtMonth.setText(entity.get_package());
        viewHolder.txtInputFreeAds.setText(entity.getFreeAds());
        viewHolder.txtInputAdsValidity.setText(entity.getAdValidity() + "days");
        viewHolder.txtInputMaxAds.setText(entity.getMaxAds());
        viewHolder.txtMonth.setText(entity.getPrice());

        if (entity.isPopular()) {
            viewHolder.llContent.setBackgroundResource(R.drawable.rectangle_borders_blue);
            viewHolder.txtPopular.setVisibility(View.VISIBLE);
            viewHolder.txtMonth.setTextColor(context.getResources().getColor(R.color.blue));
        } else {
            viewHolder.llContent.setBackgroundResource(R.drawable.rectangle_borders);
            viewHolder.txtPopular.setVisibility(View.INVISIBLE);
            viewHolder.txtMonth.setTextColor(context.getResources().getColor(R.color.graish));
        }

        if (entity.get_package().equalsIgnoreCase(arrsubscription.get(position).get_package())) {

            String strMonthNumber = entity.get_package();
            String strMonth = context.getString(R.string.months);
            String strQAR = basePreferenceHelper.getUser().getCurrency_symbol() + entity.getPrice() + context.getString(R.string.mth);
            SpannableString span1 = new SpannableString(strMonthNumber);
            span1.setSpan(new AbsoluteSizeSpan(TextSizemonthNumber), 0, strMonthNumber.length(), SPAN_INCLUSIVE_INCLUSIVE);
            SpannableString span2 = new SpannableString(strMonth);
            span2.setSpan(new AbsoluteSizeSpan(TextSizemonth), 0, strMonth.length(), SPAN_INCLUSIVE_INCLUSIVE);
            SpannableString span3 = new SpannableString(strQAR);
            span3.setSpan(new AbsoluteSizeSpan(TextSizeQAR), 0, strQAR.length(), SPAN_INCLUSIVE_INCLUSIVE);
            CharSequence finalText = TextUtils.concat(span1, "\n", span2, "\n", span3);
            viewHolder.txtMonth.setText(finalText);
        }


        return view;
    }

    public void remove(int position) {
        arrsubscription.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.arrsubscription.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList obj) {
        this.arrsubscription.addAll(obj);
        notifyDataSetChanged();
    }

    public class ViewHolder {
        AnyTextView txtMonth;
        AnyTextView txtInputFreeAds;
        AnyTextView txtInputAdsValidity;
        AnyTextView txtInputMaxAds;
        AnyTextView txtPopular;
        LinearLayout llContent;

    }
}
