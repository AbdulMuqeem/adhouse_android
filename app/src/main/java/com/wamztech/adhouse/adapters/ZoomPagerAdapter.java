package com.wamztech.adhouse.adapters;

import android.content.Context;
 ;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wamztech.adhouse.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wamztech.adhouse.models.AdImages;

import java.util.ArrayList;

import androidx.viewpager.widget.PagerAdapter;

public class ZoomPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    private View.OnClickListener onClickListener;
    private ArrayList<AdImages> arrimages;

    public ZoomPagerAdapter(Context context, ArrayList<AdImages> arrimages) {
        this.context = context;
        this.arrimages = arrimages;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrimages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = null;
        ViewHolder viewHolder;
        if (itemView == null) {
            itemView = layoutInflater.inflate(R.layout.item_zoom_image, container, false);
            viewHolder = new ViewHolder();
            viewHolder.imgFullImage = (ImageView) itemView.findViewById(R.id.imgFullImage);
            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        AdImages entity = arrimages.get(position);
        ImageLoader.getInstance().displayImage(entity.getImageUrl(), viewHolder.imgFullImage);
        container.addView(itemView);
        viewHolder.imgFullImage.setTag(position);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void addAll(ArrayList<AdImages> arrayList, int position) {
        arrimages.addAll(arrayList);
        arrayList.get(position);
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<AdImages> arrayList) {
        arrimages.addAll(arrayList);
        notifyDataSetChanged();
    }

    public void clear() {
        arrimages.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder {
        ImageView imgFullImage;
    }

}