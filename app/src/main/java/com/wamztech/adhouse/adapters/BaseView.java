package com.wamztech.adhouse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;


public abstract class BaseView<T> {

    private int mLayoutResId;
    private Context context;

    public BaseView(int mLayoutResId) {
        this.mLayoutResId = mLayoutResId;

    }

    public View ViewCreate(Context context) {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(mLayoutResId, null);

        view.setTag(ViewHolder(view));
        return view;

    }

    protected abstract GenericViewHolder ViewHolder(View view);

    public abstract void getView(T entity, int position, int grpPosition, View view, Context context);

    protected class GenericViewHolder {
    }

}
