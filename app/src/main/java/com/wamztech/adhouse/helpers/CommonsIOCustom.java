package com.wamztech.adhouse.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class CommonsIOCustom {
    private static final int BUF_SIZE = 0x1000; // 4K

    public static long copy(InputStream from, OutputStream to)
        throws IOException {
              checkNotNull(from);
              checkNotNull(to);
              byte[] buf = new byte[BUF_SIZE];
              long total = 0;
              while (true) {
                    int r = from.read(buf);
                    if (r == -1) {
                          break;
                        }
                    to.write(buf, 0, r);
                    total += r;
                  }
              return total;
            }
}
