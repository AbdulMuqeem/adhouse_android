package com.wamztech.adhouse.helpers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.wamztech.adhouse.interfaces.PermissionEvents;
import com.wamztech.adhouse.R;

import androidx.core.app.ActivityCompat;

public class CurrentLocationHelper {


    private final Context mContext;
    private final FusedLocationProviderClient mFusedLocationClient;


    private Location lastKnownLocation;
    private onLocationHelper onLocationHelperListener;

    public CurrentLocationHelper(Context context) {
        mContext = context;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    public  void getLastLocationAddress() {

        /*if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/
        PermissionHelper.isPermissionsGranted(mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new PermissionEvents() {
            @Override
            public void onPermissionGranted() {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                lastKnownLocation = location;

                                // In some rare cases the location returned can be null
                                if (lastKnownLocation == null) {
                                    return;
                                }

                                if (!Geocoder.isPresent()) {
                                    Toast.makeText(mContext,
                                            R.string.no_geocoder_available,
                                            Toast.LENGTH_LONG).show();
                                    return;
                                }
                                onLocationHelperListener.onLastKnownLocation(lastKnownLocation);
                            }
                        });
            }

            @Override
            public void onPermissionDenied() {

            }
        });
    }


    public void setOnLocationHelperListener(onLocationHelper onLocationHelper) {
        onLocationHelperListener = onLocationHelper;
    }

    public void unregisterCurrentLocationListener() {
        if (onLocationHelperListener != null)
            onLocationHelperListener = null;
    }

    public interface onLocationHelper {
        void onLastKnownLocation(Location location);
    }
}
