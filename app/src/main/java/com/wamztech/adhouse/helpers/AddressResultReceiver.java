package com.wamztech.adhouse.helpers;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

import com.wamztech.adhouse.Constants;

public class AddressResultReceiver extends ResultReceiver {
    private IAddressFetch listener;

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public AddressResultReceiver(Handler handler) {
        super(handler);
    }


    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        try {
            if (resultData == null) {
                return;
            }

            // Display the address string
            // or an error message sent from the intent service.
            String LocationAddress = resultData.getString(Constants.RESULT_DATA_KEY);
            Location location = resultData.getParcelable(FetchAddressIntentService.RESULT_LATLNG);

            if (LocationAddress == null) {
                LocationAddress = "";
            }

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                //Listener call to the Main Fragment
                Log.d("Location", LocationAddress);
                if (listener != null)
                    listener.onAddressFetch(LocationAddress,location.getLongitude(),location.getLatitude());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListener(IAddressFetch addressFetch) {
        listener = addressFetch;
    }

    public interface IAddressFetch {
        void onAddressFetch(String address, double longitude, double latitude);
    }
}
