package com.wamztech.adhouse.helpers;

import android.content.Context;

import com.google.gson.Gson;
import com.wamztech.adhouse.models.User;


public class BasePreferenceHelper extends PreferencesHelper {

    private static final String KEY_FILENAME = "file_app_1243";

    private static final String KEY_USER = "keydata_user";
    private static final String FILENAME_LANG = "en";
    private static final String KEY_DEFAULT_LANG = "aaa";


    private Context context;

    public BasePreferenceHelper(Context context) {
        this.context = context;
    }


    public void putUser(User entity) {
        putStringPreference(context, KEY_FILENAME, KEY_USER, new Gson().toJson(entity));
    }

    public User getUser() {
        String keyData = getStringPreference(context, KEY_FILENAME, KEY_USER);
        return new Gson().fromJson(keyData, User.class);
    }


    public void putLang(String lang) {
        putStringPreference(context, FILENAME_LANG, KEY_DEFAULT_LANG,lang);
    }

    public String getLang() {
        String lang = getStringPreference(context, FILENAME_LANG, KEY_DEFAULT_LANG);
        return lang ;
    }


    public void removeUser() {
        removePreference(context, KEY_FILENAME, KEY_USER);
    }


}

