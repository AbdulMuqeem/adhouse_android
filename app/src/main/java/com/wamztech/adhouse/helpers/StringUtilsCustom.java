package com.wamztech.adhouse.helpers;

public class StringUtilsCustom {
    public   static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(cs.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }


//    public static boolean isAnyBlank(final CharSequence... css) {
//        if (isEmpty(css)) {
//            return true;
//        }
//        for (final CharSequence cs : css){
//            if (isBlank(cs)) {
//                return true;
//            }
//        }
//        return false;
//    }


}
