package com.wamztech.adhouse.helpers;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.wamztech.adhouse.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;


public class CameraGalleryActionDialog extends DialogFragment implements
        View.OnClickListener  {

    private static final int REQUEST_CAMERA = 60005;
    private static final int PICK_FROM_CAMERA = 60001;
    private static final int PICK_FROM_FILE = 60002;
    private static final int RC_CAMERA_STORAGE_PERM = 123;
    CamGalleryActionListener mListener;
    private ImageButton btnClose;
    private Button btnCamCoder;
    private Button btnGalleryImg;
    private Uri mImageCaptureUri;
    private String fileImagePath;
    private File imagePath;

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.DIM_AMOUNT_CHANGED);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.argb(95, 0, 0, 0)));

//		 setStyle( STYLE_NO_FRAME, android.R.style.Theme_Holo_Dialog);


//		getActivity().setTheme(
//				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen );


        View view = inflater.inflate(R.layout.dialog_camera_gallery_option,
                null);

        btnCamCoder = (Button) view.findViewById(R.id.btnCamCoder);
        btnGalleryImg = (Button) view.findViewById(R.id.btnGalleryImg);
        btnClose = (ImageButton) view.findViewById(R.id.btnClose);

        btnCamCoder.setOnClickListener(this);
        btnGalleryImg.setOnClickListener(this);

        btnClose.setOnClickListener(this);

        return view;
    }

    public void setOnCamGalleryActionListener(CamGalleryActionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCamCoder:

//                // Check if the Camera permission is already available.
//                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    // Camera permission has not been granted.
//
//                    requestCameraPermission();
//
//                } else {
//
//                    // Camera permissions is already available, show the camera preview.
//                    setCameraIntent();
//                }

                if ((ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        || (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ){
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, RC_CAMERA_STORAGE_PERM);
                    return;
                }
                else {
                    setCameraIntent();
                }

                break;

            case R.id.btnGalleryImg:
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_CAMERA_STORAGE_PERM);
                    return;
                }
                else {
                    browsePic();
                }
                break;

            case R.id.btnClose:
                this.mListener.closeClick();
                dismiss();
                break;
        }

    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestCameraPermission() {


        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.


            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);

//            Snackbar.make(getView(), "R.string.permission_camera_rationale", Snackbar.LENGTH_INDEFINITE)
//                    .setAction(R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
//                        }
//                    })
//                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);

        }
        dismiss();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {

            // Received permission result for camera permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed

//                Snackbar.make(mLayout, R.string.permision_available_camera,Snackbar.LENGTH_SHORT).show();

            } else {

//                Snackbar.make(mLayout, R.string.permissions_not_granted, Snackbar.LENGTH_SHORT).show();


            }
        }
    }

    @Override
    public void onDetach() {
        if (getActivity() != null) {
            getActivity().setTheme(R.style.AppTheme);
        }

        super.onDetach();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getActivity() != null) {
            getActivity().setTheme(R.style.AppTheme);
        }

        super.onDismiss(dialog);
    }

    public void setCameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputPhotoFile();


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.mImageCaptureUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        } else {
            this.mImageCaptureUri = Uri.fromFile(file);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, this.mImageCaptureUri);

        startActivityForResult(intent, PICK_FROM_CAMERA);

    }

    private void browsePic() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");

        startActivityForResult(intent, PICK_FROM_FILE);

//		startActivityForResult(Intent.createChooser( intent, "Complete action using" ),ApplicationLevelConstants.PICK_FROM_FILE );
    }

    private File getOutputPhotoFile() {

        File directory = new FileHelper(getActivity()).getAvailableCache();

        String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss", Locale.UK)
                .format(new Date());

        File filPath = new File(directory.getPath() + File.separator + "IMG_"
                + timeStamp + ".png");

        return filPath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)

        {
            switch (requestCode) {
                case PICK_FROM_FILE:

                    if(this.mListener!=null)
                    {
                        this.mListener.browsePic("", null);
                    }
                    else   dismiss();

                    break;

                case PICK_FROM_CAMERA:

                 if(this.mListener!=null)
                    {
                        this.mListener.setCameraOpen(null);
                    } else   dismiss();


                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        switch (requestCode) {

            case PICK_FROM_FILE:

                mImageCaptureUri = data.getData();
                fileImagePath = OSHelper.resolvedMediaPath(mImageCaptureUri, getActivity());

                if (data == null || data.getData() == null || fileImagePath == null || fileImagePath.startsWith("http")) {
                    this.mListener.browsePic(null, null);
                    dismiss();
                    return;
                }


//				File imageFilePick = new File(mImageCaptureUri.getPath());
//				this.mListener.browsePic( fileImagePath , imageFilePick );


                new BitmapRotationAsyncTask(mImageCaptureUri, false).execute("RUN");


                dismiss();

                break;

            case PICK_FROM_CAMERA:


                if (mImageCaptureUri == null) {
                    this.mListener.setCameraOpen(null);
                    dismiss();
                    return;
                }


                File imageFile = new File(mImageCaptureUri.getPath());

                if (imageFile.exists()) {

//					imagePath = null;
//					imagePath = new File( this.mImageCaptureUri.getPath() );
//					this.mListener.setCameraOpen( imagePath );


                    new BitmapRotationAsyncTask(mImageCaptureUri, true).execute("RUN");


                }

                dismiss();
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Create a file Uri for saving an image or video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    public interface CamGalleryActionListener {

        void setCameraOpen(File filPath);

        void browsePic(String strFilePath, File file);

        void closeClick();
    }

    private class BitmapRotationAsyncTask extends AsyncTask<String, Void, File> {

        private ProgressDialog progressDialog;
        private boolean isFromCamera;
        private Uri mUriBitmap;

        public BitmapRotationAsyncTask(Uri uri, boolean isFromCamera) {

            this.mUriBitmap = uri;
            this.isFromCamera = isFromCamera;

        }

        @Override
        protected File doInBackground(String... params) {

            File imageCPath;
            try {
                // imagePath = null;


                String origPath = isFromCamera ? this.mUriBitmap.getPath() : "" + OSHelper.resolvedMediaPath(mImageCaptureUri, getActivity());

                imageCPath = new File(origPath);

                Options o2 = new Options();
                o2.inSampleSize = 1;
                Bitmap bmp = BitmapFactory.decodeFile(origPath);
                final int maxSize = 1920;

                int outWidth;
                int outHeight;

                int inWidth = bmp.getWidth();
                int inHeight = bmp.getHeight();


                if (inWidth > inHeight) {
                    outWidth = maxSize;
                    outHeight = (inHeight * maxSize) / inWidth;
                } else {
                    outHeight = maxSize;
                    outWidth = (inWidth * maxSize) / inHeight;
                }

                Bitmap resizedBitmap = Bitmap.createScaledBitmap(bmp, outWidth, outHeight, false);
                String path = BitmapHelper.replace(getActivity(), resizedBitmap, imageCPath.getAbsolutePath());


                bmp.recycle();
                bmp = null;
                imageCPath = new File(path);

                return imageCPath;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(File result) {

            progressDialog.dismiss();

            if (isFromCamera) {

                mListener.setCameraOpen(result);

            } else {
                // mListener.setCameraOpen( result );
                if (result != null) {
                    mListener.browsePic(result.getPath() + "", result);
                } else {
                    mListener.browsePic(null, null);
                }
            }

            //dismiss();

        }

        @Override
        protected void onPreExecute() {

            try {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    progressDialog = new ProgressDialog(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog));
                } else {
                    progressDialog = new ProgressDialog(getActivity());
                }

                progressDialog.setMessage("Processing...");
                progressDialog.setCancelable(false);

                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }

}
