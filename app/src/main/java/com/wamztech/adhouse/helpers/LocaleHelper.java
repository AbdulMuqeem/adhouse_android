package com.wamztech.adhouse.helpers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 * <p/>
 * Created by gunhansancar on 07/10/15.
 */
public class LocaleHelper {

    private static final String SELECTED_LANGUAGE = "en";


    public static Context onAttach(Context context, String defaultLanguage) {
        String lang = getPersistedData(context, defaultLanguage);

        return setLocale(context, lang);
    }


    public static Context setLocale(Context context, String language) {
        persist(context, language);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        if (Build.VERSION.SDK_INT > 25) {
            return updateResources(context, language);

        } else {
            return updateResourcesLegacy(context, language);
        }
//		return updateResourcesV1(context,language);

    }

    private static String getPersistedData(Context context, String defaultLanguage) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);

        editor.apply();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(new Locale("en"));

        return context.createConfigurationContext(configuration);


    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//			configuration.setLayoutDirection(locale);
            configuration.setLayoutDirection(new Locale("en"));
//        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }

    private static Context updateResourcesV1(Context context, String language) {

        Configuration configuration = context.getResources().getConfiguration();
        Resources resources = context.getResources();
        if (Build.VERSION.SDK_INT > 25) {

            configuration.setLocale(new Locale(language));
            configuration.setLayoutDirection(new Locale("en"));
            context = context.createConfigurationContext(configuration);
            return context;

        } else if (Build.VERSION.SDK_INT > 17) {
            configuration.setLocale(new Locale(language));
//				configuration.setLayoutDirection(new Locale("en"));
            context = context.createConfigurationContext(configuration);
            return context;
        } else {
            configuration.locale = new Locale(language);
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());

            return context;
        }
    }
}