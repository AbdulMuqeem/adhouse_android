package com.wamztech.adhouse.helpers;

import android.content.Context;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.wamztech.adhouse.interfaces.PermissionEvents;

import java.util.List;

public class PermissionHelper {



    public static void isPermissionsGranted(Context context, String[] PERMISSIONS, final PermissionEvents permissionEvents){

        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                permissionEvents.onPermissionGranted();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                permissionEvents.onPermissionDenied();
            }
        };

        TedPermission.with(context)
                .setPermissionListener(permissionListener)
                .setPermissions(PERMISSIONS).check();
    }


}
